/* $Id: casu_mask.h,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */


/* Includes */

#ifndef CASU_MASK_H
#define CASU_MASK_H

#include <cpl.h>
#include "catalogue/casu_fits.h"

/* Definitions of mask types */

#define MASK_NONE 0
#define MASK_BPM  1
#define MASK_CPM  2
#define MASK_OPM  3

typedef struct {
    cpl_frame      *master_mask;
    casu_fits      *mask_image;
    int            masktype;
    int            nx;
    int            ny;
    unsigned char  *mdata;
} casu_mask;

/* CASU_MASK method prototypes */

extern casu_mask *casu_mask_define(cpl_frameset *framelist, cpl_size *labels,
                                   cpl_size nlab, const char *conftag,
                                   const char *bpmtag);
extern casu_mask *casu_objmask_define(cpl_frame *frame); 
extern int casu_mask_load(casu_mask *m, int nexten, int nx, int ny);
extern void casu_mask_delete(casu_mask *m);
extern casu_mask *casu_mask_wrap_bpm(unsigned char *inbpm, int nx, int ny);
extern void casu_mask_clear(casu_mask *m);
extern void casu_mask_force(casu_mask *m, int nx, int ny);
extern casu_fits *casu_mask_get_fits(casu_mask *m);
extern const char *casu_mask_get_filename(casu_mask *m);
extern unsigned char *casu_mask_get_data(casu_mask *m);
extern int casu_mask_get_size_x(casu_mask *m);
extern int casu_mask_get_size_y(casu_mask *m);
extern int casu_mask_get_type(casu_mask *m);

#endif

/*

$Log: casu_mask.h,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.3  2015/01/29 11:51:56  jim
modified comments

Revision 1.2  2013/11/21 09:38:14  jim
detabbed

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/

                                  
