/* $Id: casu_inpaint.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

/* Includes */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <cpl.h>
#include <string.h>

#include "casu_mods.h"
#include "catalogue/casu_utils.h"
#include "casu_filt.h"
#include "catalogue/casu_fits.h"
#include "casu_mask.h"
#include "casu_stats.h"

/**@{*/

/*---------------------------------------------------------------------------*/
/**
    \ingroup casu_modules
    \brief Inpaint pixels or patches in a map
  
    \par Name:
        casu_inpaint
    \par Purpose:
        Inpoint pixels or patches in a map
    \par Description:
        An image and its bad pixel mask are given. The background of the
        image is modelled by using robust medians of cells. This
        background map is smoothed. Any bad pixels in the input map are
        replaced by the value in the background map.
    \par Language:
        C
    \param in
        The input data image (overwritten by result).
    \param nbsize
        The size of the cells for the smoothing boxes
    \param status 
        An input/output status that is the same as the returned values below.
    \retval CASU_OK 
        if everything is ok
    \retval CASU_WARN 
        if the output property list is NULL.
    \retval CASU_FATAL 
        if image data arrays don't have matching dimensions
    \par QC headers:
        None
    \par DRS headers:
        None
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern int casu_inpaint(casu_fits *in, int nbsize, int *status) {
    int i,nx,ny;
    float *data,*skymap,avback;
    cpl_binary *bpm;
    cpl_image *im;
    
    /* Inherited status */

    if (*status != CASU_OK)
        return(*status);

    /* Set the data arrays */

    im = casu_fits_get_image(in);
    data = cpl_image_get_data_float(im);
    bpm = cpl_mask_get_data(cpl_image_get_bpm(im));
    nx = (int)cpl_image_get_size_x(im);
    ny = (int)cpl_image_get_size_y(im);

    /* Model out the background */

    casu_backmap(data,bpm,nx,ny,nbsize,&avback,&skymap,status);

    /* Now inpaint the bad bits */

    for (i = 0; i < nx*ny; i++) 
        if (bpm[i]) 
            data[i] = skymap[i];
    freespace(skymap);

    /* Get out of here */

    GOOD_STATUS
}


/**@}*/

/*

$Log: casu_inpaint.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.6  2015/03/12 09:16:51  jim
Modified to remove some compiler moans

Revision 1.5  2015/01/29 11:51:56  jim
modified comments

Revision 1.4  2015/01/09 12:13:15  jim
*** empty log message ***

Revision 1.3  2014/03/26 15:49:55  jim
Modified to use casu_backmap routine

Revision 1.2  2013/11/21 09:38:14  jim
detabbed

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/
