/* $Id: casu_mods.h,v 1.4 2015/11/25 13:50:03 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/11/25 13:50:03 $
 * $Revision: 1.4 $
 * $Name:  $
 */

#ifndef CASU_MODS_H
#define CASU_MODS_H

#include <cpl.h>
#include "catalogue/casu_fits.h"
#include "casu_mask.h"
#include "catalogue/casu_tfits.h"

/* Function prototypes */

extern int casu_backmap(float *map, cpl_binary *bpm, int nx, int ny, 
                        int nbsize, float *avback, float **skymap, int *status);
extern int casu_crwtmap(casu_fits *infile, casu_fits *initconf, int cellsize,
                        float readnoise, float gain, casu_fits **wtmap, 
                        int *status);
extern int casu_darkcor(casu_fits *infile, casu_fits *darksrc, float darkscl,
                        int *status);
extern int casu_defringe(casu_fits **infiles, int nimages, casu_fits **fringes,
                         int nfringes, casu_mask *mask, int nbsize,
                         int *status);
extern int casu_flatcor(casu_fits *infile, casu_fits *flatsrc, int *status);
extern int casu_gaincor(casu_fits *infile, float gainscl, int *status);
extern int casu_genbpm(casu_fits **flatlist, int nflatlist, cpl_image *master,
                       float lthr, float hthr, const char *expkey, 
                       cpl_array **bpm_array, int *nbad, float *badfrac, 
                       int *status);
extern int casu_getstds(cpl_propertylist *plist, int cache, char *path,
                        char *catname, int cdssearch, char *cacheloc,
                        cpl_table **stds, int *status);
extern int casu_getstds_cdslist(int cdschoice, char **cdscatname,
                                char **cdscatid, int *status);
extern int casu_get_cdschoice(const char* cdscatname);
extern int casu_imcombine(casu_fits **fset, casu_fits **fsetv, int nfits, 
                          int combtype, int scaletype, int xrej, float thresh, 
                          const char *expkey, cpl_image **outimage, 
                          cpl_image **outvimage, unsigned char **rejmask, 
                          unsigned char **rejplus, cpl_propertylist **drs, 
                          int *status);
extern int casu_imcore(casu_fits *infile, casu_fits *conf, int ipix,
                       float threshold, int icrowd, float rcore, int nbsize,
                       int cattyp, float filtfwhm, casu_tfits **outtab, 
                       float gain, int *status);
extern int casu_imdither(casu_fits **inf, casu_fits **inconf, int nimages, 
                         int nconfs, float lthr, float hthr,
                         cpl_propertylist **p, const char *expkey,
                         cpl_image **out, cpl_image **outc, int *status);
extern int casu_imstack(casu_fits **inf, casu_fits **inconf, casu_fits **invar,
                        casu_tfits **cats, int nimages, int nconfs, float lthr,
                        float hthr, int method, int seeing, int fast, int unmap,
                        const char *expkey, casu_fits **out, casu_fits **outc, 
                        casu_fits **outv, int *status);
extern int casu_inpaint(casu_fits *in, int nbsize, int *status);
extern int casu_matchstds(cpl_table *objtab, cpl_table *stdstab, float srad,
                          cpl_table **outtab, int *status);
extern int casu_matchxy(cpl_table *progtab, cpl_table *tmplate, float srad,
                        float *xoffset, float *yoffset, int *nm, 
                        cpl_table **outtab, int *status);
extern int casu_mkconf(cpl_image *flat, char *flatfile, casu_mask *bpm, 
                       cpl_image **outconf, cpl_propertylist **drs, 
                       int *status);
extern int casu_mosaic(cpl_frameset *infiles, cpl_frameset *inconf,
                       int interp, int skyflag, float skywish_in, 
                       char *expkey, int conflim, casu_fits **out,
                       casu_fits **outc, int *status);
extern int casu_nditcor(casu_fits *infile, int ndit, const char *expkey,
                        int *status);
extern int casu_nebuliser(casu_fits *infile, casu_fits *inconf, int medfilt,
                          int linfilt, int niter, int axis, int twod,
                          int takeout_sky, int norm, int wantback, 
                          float signeg, float sigpos, casu_fits **backmap,
                          int *status);
extern int casu_opm(casu_fits *infile, casu_fits *conf, int ipix,
                    float threshold, int nbsize, float filtfwhm, 
                    int niter, int *status);
extern int casu_photcal_extinct(casu_fits **images, casu_tfits **mstds, 
                                casu_tfits **cats, int nimages, 
                                char *filt, cpl_table *phottab, int minstars,
                                cpl_frame *schlf_n, cpl_frame *schlf_s,
                                const char *expkey, const char *amkey,
                                float magerrcut, int *status);

extern int casu_remove_mag_outside_range(casu_tfits **mstds, const int nimages,
                                         char *filt, cpl_table *phottab,
                                         const float low_mag, const float high_mag);
extern float
casu_calculate_abmag_lim(const float zeropoint, const float skynoise,
                         const float r_core, const float eff_expt, 
                         const float apcor3, const float extinction);
extern float
casu_calculate_abmag_sat(const float zeropoint, const float satlev,
                         const float mean_sky, const float psf_fwhm, 
                         const float pixel_scale, const float exptime);
extern int casu_platesol(cpl_propertylist *plist, cpl_propertylist *tlist,
                         cpl_table *matchedstds, int nconst, int shiftan, 
                         int *status);
extern int casu_platexy(cpl_table *matchedxy, int nconst, cpl_array **coefs, 
                        int *status);
#endif
