/* $Id: casu_sky.h,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */


/* Includes */

#ifndef CASU_SKY_H
#define CASU_SKY_H

#include <cpl.h>
#include "catalogue/casu_fits.h"
#include "casu_mask.h"

/* Sky subroutine prototypes */

extern int casu_pawsky_mask(casu_fits **inlist, casu_fits **invar, int nfiles, 
                            casu_fits *conf, casu_mask *mask, 
                            casu_fits **skyout, casu_fits **skyvar, int niter, 
                            int ipix, float thresh, int nbsize, float smkern, 
                            int *status);
extern int casu_pawsky_mask_pre(casu_fits **inlist, casu_fits **invar, 
                                int nfiles, casu_mask *mask, 
                                casu_fits *objmask, int nbsize, 
                                casu_fits **skyout, casu_fits **skyvar, 
                                int *status);
extern int casu_simplesky_mask(casu_fits **inlist, casu_fits **invar, 
                               int nfiles, casu_fits *conf, casu_mask *mask, 
                               casu_fits **skyout, casu_fits **skyvar, 
                               int niter, int ipix, float thresh, int nbsize, 
                               float smkern, int *status);
extern int casu_pawsky_minus(casu_fits **infiles, casu_fits **invar, 
                             casu_fits *conf, casu_fits *mask, int nfiles, 
                             casu_fits **skyout, casu_fits **skyvar,
                             int *status);

#endif

/*

$Log: casu_sky.h,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.8  2015/03/12 09:16:51  jim
Modified to remove some compiler moans

Revision 1.7  2015/01/29 11:56:27  jim
modified comments

Revision 1.6  2014/12/11 12:23:33  jim
new version

Revision 1.5  2014/04/09 11:08:21  jim
Get rid of a couple of compiler moans

Revision 1.4  2013/11/27 10:35:57  jim
Removed extraneous declaration

Revision 1.3  2013/11/21 09:38:14  jim
detabbed

Revision 1.2  2013-09-30 18:12:24  jim
Added casu_simplesky_mask

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/
