/* $Id: casu_darkcor.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

/* Includes */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <cpl.h>
#include "casu_mods.h"
#include "catalogue/casu_utils.h"
#include "catalogue/casu_fits.h"

/**@{*/

/*---------------------------------------------------------------------------*/
/**
    \ingroup casu_modules
    \brief Correct input data for dark current
  
    \par Name:
        casu_darkcor
    \par Purpose:
        Correct input data for dark current
    \par Description:
        Two images are given -- one is the data for an observation, the 
        other is for a mean dark frame. The latter is subtracted from 
        the former. The result overwrites the input data array for the object 
        image. The data for the dark array can be scaled by a factor before 
        subtraction if a scaling factor other than 1 is specified.
    \par Language:
        C
    \param infile 
        The input data image (overwritten by result).
    \param darksrc 
        The input dark image
    \param darkscl 
        The dark scaling factor
    \param status 
        An input/output status that is the same as the returned values below.
    \retval CASU_OK 
        if everything is ok
    \retval CASU_WARN 
        if the output property list is NULL.
    \retval CASU_FATAL 
        if image data fails to load, the images have different dimensionality or
        if the image subtraction in CPL fails
    \par QC headers:
        None
    \par DRS headers:
        The following DRS keywords are written to the infile extension header
        - \b DARKCOR
            The name of the originating FITS file for the dark image
        - \b DARKSCL: 
            The scale factor applied to the dark frame before subtraction.
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern int casu_darkcor(casu_fits *infile, casu_fits *darksrc, float darkscl,
                        int *status) {
    long n,i;
    float *idata,*ddata;
    cpl_image *im,*dm;
    cpl_propertylist *oplist;
    const char *fctid = "casu_darkcor";

    /* Inherited status */

    if (*status != CASU_OK)
        return(*status);

    /* See if this file has already been done */

    oplist = casu_fits_get_ehu(infile);
    if (cpl_propertylist_has(oplist,"ESO DRS DARKCOR"))
        return(*status);

    /* Get the images and check the dimension to make sure they match */
    
    im = casu_fits_get_image(infile);
    dm = casu_fits_get_image(darksrc);
    if (casu_compare_dims(im,dm) != CASU_OK) {
        cpl_msg_error(fctid,"Object and dark data array dimensions don't match");
        FATAL_ERROR
    }

    /* If the scale factor is 1, then just use the cpl image routine to do
       the arithmetic */

    if (darkscl == 1.0) {
        if (cpl_image_subtract(im,dm) != CPL_ERROR_NONE)
            FATAL_ERROR

    /* Otherwise, do it by hand */

    } else {
        idata = cpl_image_get_data_float(im);
        ddata = cpl_image_get_data_float(dm);
        if (idata == NULL || ddata == NULL) 
            FATAL_ERROR;
        n = (long)cpl_image_get_size_x(im)*(long)cpl_image_get_size_y(im);
        for (i = 0; i < n; i++) 
            idata[i] -= darkscl*ddata[i];
    }

    /* Now put some stuff in the DRS extension... */

    oplist = casu_fits_get_ehu(infile);
    if (oplist != NULL) {
        if (casu_fits_get_fullname(darksrc) != NULL) 
            cpl_propertylist_update_string(oplist,"ESO DRS DARKCOR",
                                           casu_fits_get_fullname(darksrc));
        else
            cpl_propertylist_update_string(oplist,"ESO DRS DARKCOR",
                                           "Memory File");
        cpl_propertylist_set_comment(oplist,"ESO DRS DARKCOR",
                                     "Image used for dark correction");
        cpl_propertylist_update_float(oplist,"ESO DRS DARKSCL",darkscl);
        cpl_propertylist_set_comment(oplist,"ESO DRS DARKSCL",
                                     "Scaling factor used in dark correction");
    } else
        WARN_RETURN

    /* Get out of here */

    GOOD_STATUS
}


/**@}*/

/*

$Log: casu_darkcor.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.3  2015/01/29 11:46:18  jim
modified comments

Revision 1.2  2013/11/21 09:38:13  jim
detabbed

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/
