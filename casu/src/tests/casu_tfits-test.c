/* $Id: casu_tfits-test.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_tfits.h>
#include <casu_utils.h>
#include <casu_mods.h>

int main(void) {
    const char *testfile = "casutfitstest.fit";
    casu_tfits *ftest,**ftests,*ftest2;
    int i,ival;
    float fval;
    cpl_frame *fr;
    cpl_table *tab;
    cpl_propertylist *phu,*ehu;
    char str[BUFSIZ];

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Check correct response to NULL file or a file that doesn't exist */

    ftest = casu_tfits_load(NULL,1);
    cpl_test_null(ftest);
    fr = cpl_frame_new();
    cpl_frame_set_filename(fr,testfile);
    ftest = casu_tfits_load(fr,1);
    cpl_test_null(ftest);
    ftests = casu_tfits_load_list(NULL,1);
    cpl_test_null(ftests);

    /* Create a dummy FITS file and save it */

    if (access(testfile,F_OK) == 0)
        remove(testfile);
    tab = cpl_table_new(10);
    cpl_table_new_column(tab,"X",CPL_TYPE_FLOAT);
    cpl_table_new_column(tab,"Y",CPL_TYPE_FLOAT);
    phu = cpl_propertylist_new();
    cpl_propertylist_update_float(phu,"EXPTIME",1000.0);
    ehu = cpl_propertylist_new();
    cpl_propertylist_update_int(ehu,"NDIT",5);
    cpl_propertylist_update_string(ehu,"EXTNAME","TESTTAB");
    cpl_image_save(NULL,testfile,CPL_TYPE_UCHAR,phu,CPL_IO_DEFAULT);
    cpl_table_save(tab,NULL,ehu,testfile,CPL_IO_EXTEND);
    cpl_propertylist_delete(ehu);
    cpl_propertylist_delete(phu);
    cpl_table_delete(tab);

    /* Check that we can load this */

    ftest = casu_tfits_load(fr,1);
    cpl_test_nonnull(ftest);

    /* Check we can access the two headers */
    
    phu = casu_tfits_get_phu(NULL);
    cpl_test_null(phu);
    phu = casu_tfits_get_phu(ftest);
    cpl_test_nonnull(phu);
    fval = cpl_propertylist_get_float(phu,"EXPTIME");
    cpl_test_eq(fval,1000.0);
    ehu = casu_tfits_get_ehu(NULL);
    cpl_test_null(ehu);
    ehu = casu_tfits_get_ehu(ftest);
    cpl_test_nonnull(ehu);
    ival = cpl_propertylist_get_int(ehu,"NDIT");
    cpl_test_eq(ival,5);

    /* Check out various accessors */

    tab = casu_tfits_get_table(NULL);
    cpl_test_null(tab);
    tab = casu_tfits_get_table(ftest);
    cpl_test_nonnull(tab);
    i = casu_tfits_get_nexten(NULL);
    cpl_test_eq(i,-1);
    i = casu_tfits_get_nexten(ftest);
    cpl_test_eq(i,1);
    cpl_test_null(casu_tfits_get_filename(NULL));
    cpl_test_eq_string(casu_tfits_get_filename(ftest),testfile);
    cpl_test_null(casu_tfits_get_fullname(NULL));
    (void)sprintf(str,"%s[%s]",testfile,"TESTTAB");
    cpl_test_eq_string(casu_tfits_get_fullname(ftest),str);
    cpl_test_eq(casu_tfits_get_status(NULL),CASU_FATAL);
    cpl_test_eq(casu_tfits_get_status(ftest),CASU_OK);
    cpl_test_eq(casu_tfits_set_error(NULL,CASU_FATAL),0);
    cpl_test_eq(casu_tfits_set_error(ftest,CASU_FATAL),1);
    casu_tfits_set_status(ftest,CASU_OK);
    casu_tfits_set_filename(ftest,NULL);
    cpl_test_eq_string(casu_tfits_get_filename(ftest),testfile);
    ftest2 = casu_tfits_wrap(NULL,ftest,phu,ehu);
    cpl_test_null(ftest2);
    
    /* Tidy and exit */

    casu_tfits_delete(ftest);
    remove(testfile);
    cpl_frame_delete(fr);
    return(cpl_test_end(0));
}

/*

$Log: casu_tfits-test.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.1  2015/01/09 11:39:55  jim
new entry


*/
