/* $Id: casu_pawsky_minus-test.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_fits.h>
#include <casu_utils.h>
#include <casu_sky.h>

int main(void) {
    int status,retval;
    casu_fits *skyout,*skyvar;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Check inherited status only. This is too complicated and needs
       a proper regression test */

    status = CASU_FATAL;
    retval = casu_pawsky_minus(NULL,NULL,NULL,NULL,10,&skyout,&skyvar,
                               &status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);
    cpl_test_null(skyout);
    cpl_test_null(skyvar);


    return(cpl_test_end(0));
}

/*

$Log: casu_pawsky_minus-test.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.2  2015/03/12 09:16:51  jim
Modified to remove some compiler moans

Revision 1.1  2015/01/09 11:39:55  jim
new entry


*/
