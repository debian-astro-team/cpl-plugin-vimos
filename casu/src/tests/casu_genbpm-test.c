/* $Id: casu_genbpm-test.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_fits.h>
#include <casu_utils.h>
#include <casu_mods.h>

int main(void) {
    int status,retval,nbad,i,ilim,ind,ii,jj,isnull;
    cpl_array *bpm_array;
    float badfrac,lim,ave,*idata,off;
    casu_fits *fitslist[10];
    cpl_image *im,*master = NULL;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Check inherited status */

    status = CASU_FATAL;
    retval = casu_genbpm(NULL,0,NULL,5.0,5.0,"EXPTIME",&bpm_array,&nbad,
                         &badfrac,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);
    status = CASU_OK;

    /* Create some images */

    for (i = 0; i <= 10; i++) {
        ave = 100.0*(float)(i+1);
        im = cpl_image_new(100,100,CPL_TYPE_FLOAT);
        lim = 10.0*(float)(i+1);
        ilim = 20*(i+1);
        idata = cpl_image_get_data_float(im);
        ind = 0;
        for (ii = 0; ii < 100; ii++) {
            for (jj = 0; jj < 100; jj++) {
                off = lim - (float)(rand() % ilim);
                idata[ind++] = ave + off;
            }
        }
        if (i != 0) {
            idata[5500] = 100*ave;
            fitslist[i-1] = casu_fits_wrap(im,NULL,NULL,NULL);
        } else {
            master = im;
        }
    }

    /* Run the routine and check the output values */

    retval = casu_genbpm(fitslist,10,master,3.0,3.0,"EXPTIME",&bpm_array,
                         &nbad,&badfrac,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(status,retval);
    cpl_test_eq(nbad,1);
    cpl_test_rel(badfrac,1.0e-4,1.0e-6);
    cpl_test_eq(cpl_array_get_int(bpm_array,5500,&isnull),1);
    
    /* Tidy and exit */
    
    cpl_image_delete(master);
    for (i = 0; i < 10; i++) {
        im = casu_fits_get_image(fitslist[i]);
        casu_fits_unwrap(fitslist[i]);
        cpl_image_delete(im);
    }
    cpl_array_delete(bpm_array);
    

    return(cpl_test_end(0));
}

/*

$Log: casu_genbpm-test.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.1  2015/01/09 11:39:55  jim
new entry


*/
