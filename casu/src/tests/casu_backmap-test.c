/* $Id: casu_backmap-test.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_utils.h>
#include <casu_mods.h>

int main(void) {
    int status,retval,nx,ny,nbsize,npts,j;
    float *map,*skyout,avback;
    cpl_binary *bpm;
    cpl_image *maptest,*test;
    cpl_mask *bpmtest;
    double val;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Start with input values that should cause problems. Then do
       the input testing by removing one problem at a time until they
       are all sensible. Start by checking the inherited status.
       It should return with the input value and the output sky map
       should always be NULL in an error condition */

    status = CASU_FATAL;
    nx = 1000;
    ny = 1000;
    npts = nx*ny;
    maptest = cpl_image_new((cpl_size)nx,(cpl_size)ny,CPL_TYPE_FLOAT);
    bpmtest = cpl_image_get_bpm(maptest);
    map = cpl_image_get_data_float(maptest);
    bpm = cpl_mask_get_data(bpmtest);
    nbsize = 100;
    retval = casu_backmap(map,bpm,nx,ny,nbsize,&avback,&skyout,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);
    cpl_test_null(skyout);

    /* Reset status now. It should now fail because the input arrays 
       are null */

    status = CASU_OK;
    map = NULL;
    bpm = NULL;
    retval = casu_backmap(map,bpm,nx,ny,nbsize,&avback,&skyout,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);
    cpl_test_null(skyout);

    /* Now test image size errors */

    status = CASU_OK;
    map = cpl_image_get_data_float(maptest);
    bpm = cpl_mask_get_data(bpmtest);
    nx = -1;
    retval = casu_backmap(map,bpm,nx,ny,nbsize,&avback,&skyout,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);
    cpl_test_null(skyout);

    /* Now the condition where every pixel is flagged as bad */

    status = CASU_OK;
    nx = 1000;
    for (j = 0; j < npts; j++)
        bpm[j] = 1;
    retval = casu_backmap(map,bpm,nx,ny,nbsize,&avback,&skyout,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);
    cpl_test_null(skyout);

    /* Finally the condition where nbsize is stupid */

    status = CASU_OK;
    memset(bpm,0,npts*sizeof(cpl_binary));
    nbsize = -1;
    retval = casu_backmap(map,bpm,nx,ny,nbsize,&avback,&skyout,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);
    cpl_test_null(skyout);
    nbsize = 100;

    /* Do a background test now. Start by assuming all the pixels are good.
       Give the map a background level of 2000 with some noise. See if it
       comes back as 2000... */

    status = CASU_OK;
    cpl_image_fill_noise_uniform(maptest,1990.0,2010.0);
    retval = casu_backmap(map,bpm,nx,ny,nbsize,&avback,&skyout,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(status,retval);
    cpl_test_nonnull(skyout);
    cpl_test_rel(avback,2000.0,0.1);
    test = cpl_image_wrap_float((cpl_size)nx,(cpl_size)ny,skyout);
    val = cpl_image_get_mean_window(test,480,520,480,520);
    cpl_test_rel(val,2000.0,0.1);
    cpl_image_unwrap(test);
    cpl_free(skyout);

    /* Now put some different values into a small area, but mask this
       area out and see if we still get 2000 */

    status = CASU_OK;
    cpl_image_fill_window(maptest,500,500,520,520,3000.0);
    cpl_mask_threshold_image(bpmtest,(const cpl_image *)maptest,
                             2999.0,3001.0,CPL_BINARY_1);    
    retval = casu_backmap(map,bpm,nx,ny,nbsize,&avback,&skyout,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(status,retval);
    cpl_test_nonnull(skyout);
    cpl_test_rel(avback,2000.0,0.1);
    test = cpl_image_wrap_float((cpl_size)nx,(cpl_size)ny,skyout);
    val = cpl_image_get_mean_window(test,480,520,480,520);
    cpl_image_unwrap(test);
    cpl_free(skyout);

    /* Tidy and exit */

    cpl_image_delete(maptest);
    return(cpl_test_end(0));
}

/*

$Log: casu_backmap-test.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.2  2015/03/12 09:16:51  jim
Modified to remove some compiler moans

Revision 1.1  2015/01/09 11:39:55  jim
new entry


*/
