/* $Id: casu_getstds-test.c,v 1.3 2015/11/25 15:42:34 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/11/25 15:42:34 $
 * $Revision: 1.3 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_mods.h>
#include <casu_utils.h>

int main(void) {
    int status,retval;
    cpl_propertylist *plist;
    cpl_table *tabout;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Check inherited status and situation where the propertylist is NULL */
    
    status = CASU_OK;
    plist = NULL;
    retval = casu_getstds(plist,0,"","",1,".",&tabout,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);
    cpl_test_null(tabout);
    plist = cpl_propertylist_new();
    status = CASU_FATAL;
    retval = casu_getstds(plist,0,"","",1,".",&tabout,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);
    cpl_test_null(tabout);
    status = CASU_OK;

    /* Fill the propertylist with some values */

    cpl_propertylist_update_string(plist,"CTYPE1","RA---ZPN");
    cpl_propertylist_update_string(plist,"CTYPE2","DEC--ZPN");
    cpl_propertylist_update_double(plist,"CRVAL1",81.0578935624986);
    cpl_propertylist_update_double(plist,"CRVAL2",-24.5408992630551);
    cpl_propertylist_update_double(plist,"CRPIX1",2159.65203390075);
    cpl_propertylist_update_double(plist,"CRPIX2",2187.28057518062);
    cpl_propertylist_update_double(plist,"CD1_1",-2.96100210581431E-05);
    cpl_propertylist_update_double(plist,"CD2_1",-3.54397484069851E-08);
    cpl_propertylist_update_double(plist,"CD1_2",-3.93573000440439E-08);
    cpl_propertylist_update_double(plist,"CD2_2",2.96102853649076E-05);
    cpl_propertylist_update_double(plist,"PV2_1",1.0);
    cpl_propertylist_update_double(plist,"PV2_3",400.0);
    cpl_propertylist_update_int(plist,"NAXIS",2);
    cpl_propertylist_update_int(plist,"NAXIS1",2048);
    cpl_propertylist_update_int(plist,"NAXIS2",2048);

    /* Now run it with these properties and see if you get a table out
       with the right number of sources (2MASS PSC) */

/*
 * For the time deactivating this test as it needs internet connection and not
 * all computer may have it at installation time
 *
    retval = casu_getstds(plist,0,"","",1,".",&tabout,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(status,retval);
    cpl_test_nonnull(tabout);
    cpl_test_eq(cpl_table_get_nrow(tabout),34);
*/
    
    /* Tidy and exit */

    cpl_table_delete(tabout);
    cpl_propertylist_delete(plist);
    return(cpl_test_end(0));
}

/*

$Log: casu_getstds-test.c,v $
Revision 1.3  2015/11/25 15:42:34  jim
added cacheloc parameter to casu_getstds

Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.1  2015/01/09 11:39:55  jim
new entry


*/
