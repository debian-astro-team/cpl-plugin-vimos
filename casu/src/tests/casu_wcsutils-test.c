/* $Id: casu_wcsutils-test.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_fits.h>
#include <casu_utils.h>
#include <casu_mods.h>
#include <casu_wcsutils.h>

int main(void) {
    cpl_propertylist *plist,*plist2;
    cpl_wcs *wcs,*wcs2;
    double ra,dec,x,y,xi,eta,ra2,dec2,x2,y2;
    int status;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Create the propertylist and get the wcs descriptor */

    plist = cpl_propertylist_new();
    cpl_propertylist_update_string(plist,"CTYPE1","RA---ZPN");
    cpl_propertylist_update_string(plist,"CTYPE2","DEC--ZPN");
    cpl_propertylist_update_double(plist,"CRVAL1",81.0578935624986);
    cpl_propertylist_update_double(plist,"CRVAL2",-24.5408992630551);
    cpl_propertylist_update_double(plist,"CRPIX1",2159.65203390075);
    cpl_propertylist_update_double(plist,"CRPIX2",2187.28057518062);
    cpl_propertylist_update_double(plist,"CD1_1",-2.96100210581431E-05);
    cpl_propertylist_update_double(plist,"CD2_1",-3.54397484069851E-08);
    cpl_propertylist_update_double(plist,"CD1_2",-3.93573000440439E-08);
    cpl_propertylist_update_double(plist,"CD2_2",2.96102853649076E-05);
    cpl_propertylist_update_double(plist,"PV2_1",1.0);
    cpl_propertylist_update_double(plist,"PV2_3",400.0);
    cpl_propertylist_update_int(plist,"NAXIS",2);
    cpl_propertylist_update_int(plist,"NAXIS1",2048);
    cpl_propertylist_update_int(plist,"NAXIS2",2048);
    wcs = cpl_wcs_new_from_propertylist(plist);

    /* Do some tests now */

    x = 1000.0;
    y = 1000.0;
    casu_xytoradec(wcs,x,y,&ra,&dec);
    cpl_test_rel(ra,81.09569,0.001);
    cpl_test_rel(dec,-24.57600,0.001);
    casu_radectoxy(wcs,ra,dec,&x,&y);
    cpl_test_rel(x,1000.0,0.00001);
    cpl_test_rel(y,1000.0,0.00001);
    casu_radectoxieta(wcs,ra,dec,&xi,&eta);
    cpl_test_rel(xi,0.000600115,0.000001);
    cpl_test_rel(eta,-0.000612866,0.00001);
    status = CASU_OK;
    casu_coverage(plist,0,&ra,&ra2,&dec,&dec2,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_rel(ra,81.06153,0.001);
    cpl_test_rel(ra2,81.12822,0.001);
    cpl_test_rel(dec,-24.60560,0.001);
    cpl_test_rel(dec2,-24.54493,0.001);
    plist2 = cpl_propertylist_duplicate(plist);
    cpl_propertylist_update_double(plist2,"CRVAL1",81.1578935624986);
    cpl_propertylist_update_double(plist2,"CRVAL2",-24.6408992630551);
    wcs2 = cpl_wcs_new_from_propertylist(plist2);
    casu_xytoxy_list(wcs,wcs2,1,&x,&y,&x2,&y2);
    cpl_test_rel(x2,4068.86290,0.001);
    cpl_test_rel(y2,4382.93236,0.001);

    /* Tidy and exit */

    cpl_wcs_delete(wcs);
    cpl_wcs_delete(wcs2);
    cpl_propertylist_delete(plist);
    cpl_propertylist_delete(plist2);
    return(cpl_test_end(0));
}

/*

$Log: casu_wcsutils-test.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.1  2015/01/09 11:39:55  jim
new entry


*/
