/* $Id: casu_stats-test.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_fits.h>
#include <casu_utils.h>
#include <casu_mods.h>
#include <casu_stats.h>

int main(void) {
    float *data,val,mad;
    double *ddata,dval;
    unsigned char *bpm;
    int i;
    long npts;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Get some workspace for arrays */

    data = cpl_malloc(21*sizeof(float));
    ddata = cpl_malloc(21*sizeof(double));
    bpm = cpl_calloc(21,sizeof(unsigned char));
    for (i = 0; i < 21; i++) {
        data[20-i] = (float)i - 10.0;
        ddata[20-i] = (double)i - 10.0;
    }
    npts = 21;

    /* Test median routines */
    
    val = casu_med(data,bpm,npts);
    cpl_test_rel(val,0.0,0.001);
    dval = casu_dmed(ddata,bpm,npts);
    cpl_test_rel(dval,0.0,0.001);
    bpm[0] = 1;
    bpm[1] = 1;
    val = casu_med(data,bpm,npts);
    cpl_test_rel(val,-1.0,0.001);
    dval = casu_dmed(ddata,bpm,npts);
    cpl_test_rel(dval,-1.0,0.001);
    bpm[0] = 0;
    bpm[1] = 0;

    /* Test casu_medmad */

    casu_medmad(data,bpm,npts,&val,&mad);
    cpl_test_rel(val,0.0,0.001);
    cpl_test_rel(mad,5.0,0.001);

    /* Test casu_medmadcut */

    casu_medmadcut(data,bpm,npts,-11.0,9,&val,&mad);
    cpl_test_rel(val,-0.5,0.001);
    cpl_test_rel(mad,5.0,0.001);

    /* Test mean routines */
    
    val = casu_mean(data,bpm,npts);
    cpl_test_rel(val,0.0,0.001);
    dval = casu_dmean(ddata,bpm,npts);
    cpl_test_rel(dval,0.0,0.001);
    bpm[0] = 1;
    bpm[1] = 1;
    val = casu_mean(data,bpm,npts);
    cpl_test_rel(val,-1.0,0.01);
    dval = casu_dmean(ddata,bpm,npts);
    cpl_test_rel(dval,-1.0,0.01);
    bpm[0] = 0;
    bpm[1] = 0;

    /* Test casu_meansig and casu_medsig */

    casu_meansig(data,bpm,npts,&val,&mad);
    cpl_test_rel(val,0.0,0.01);
    cpl_test_rel(mad,6.0553,0.01);
    casu_medsig(data,bpm,npts,&val,&mad);
    cpl_test_rel(val,0.0,0.01);
    cpl_test_rel(mad,6.0553,0.01);

    /* Finally test casu_sumbpm */

    casu_sumbpm(bpm,npts,&i);
    cpl_test_eq(i,0);

    /* Tidy and exit */

    cpl_free(data);
    cpl_free(ddata);
    cpl_free(bpm);
    return(cpl_test_end(0));
}

/*

$Log: casu_stats-test.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.2  2015/01/29 11:56:18  jim
Added medmadcut test

Revision 1.1  2015/01/09 11:39:55  jim
new entry


*/
