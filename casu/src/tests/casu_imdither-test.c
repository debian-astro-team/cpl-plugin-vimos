/* $Id: casu_imdither-test.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_fits.h>
#include <casu_utils.h>
#include <casu_mods.h>

int main(void) {
    int status,retval,i;
    cpl_propertylist *prop,*phu,*ehu;
    cpl_image *out,*outc,*im;
    casu_fits *infits[2],*inconfs[2];
    double ave,val1,val2;
    float off;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Check inherited status and the situation where you say there are
       no input frames */

    status = CASU_FATAL;
    retval = casu_imdither(NULL,NULL,10,10,100.0,100.0,&prop,"EXPTIME",
                           &out,&outc,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);
    cpl_test_null(prop);
    cpl_test_null(out);
    cpl_test_null(outc);
    status = CASU_OK;
    retval = casu_imdither(NULL,NULL,0,10,100.0,100.0,&prop,"EXPTIME",
                           &out,&outc,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);
    cpl_test_null(prop);
    cpl_test_null(out);
    cpl_test_null(outc);

    /* Make a couple of frames and some confidence maps */

    for (i = 0; i < 2; i++) {
        ave = 100.0*(float)(i+1);
        val1 = ave - 5.0;
        val2 = ave + 5.0;
        im = cpl_image_new(100,100,CPL_TYPE_FLOAT);
        cpl_image_fill_noise_uniform(im,val1,val2);
        phu = cpl_propertylist_new();
        ehu = cpl_propertylist_new();
        off = (i == 0 ? 0.0 : 50.0);
        cpl_propertylist_update_float(ehu,"ESO DRS XOFFDITHER",off);
        cpl_propertylist_update_float(ehu,"ESO DRS YOFFDITHER",0.0);
        infits[i] = casu_fits_wrap(im,NULL,phu,ehu);
        cpl_propertylist_delete(phu);
        cpl_propertylist_delete(ehu);

        val1 = 99.0;
        val2 = 101.0;
        im = cpl_image_new(100,100,CPL_TYPE_INT);
        cpl_image_fill_noise_uniform(im,val1,val2);
        phu = cpl_propertylist_new();
        ehu = cpl_propertylist_new();
        inconfs[i] = casu_fits_wrap(im,NULL,phu,ehu);
        cpl_propertylist_delete(phu);
        cpl_propertylist_delete(ehu);
    }

    /* Now test to see if we get a reasonable dither */

    status = CASU_OK;
    retval = casu_imdither(infits,inconfs,2,2,100.0,100.0,&prop,"EXPTIME",
                           &out,&outc,&status);    
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(status,retval);
    cpl_test_nonnull(prop);
    cpl_test_nonnull(out);
    cpl_test_nonnull(outc);
    cpl_test_eq(150,cpl_image_get_size_x(out));
    cpl_test_eq(100,cpl_image_get_size_y(out));
    cpl_test_rel(100.0,cpl_image_get_mean(out),0.001);
    
    /* Tidy and exit */

    cpl_propertylist_delete(prop);
    cpl_image_delete(out);
    cpl_image_delete(outc);
    for (i = 0; i < 2; i++) {
        casu_fits_delete(infits[i]);
        casu_fits_delete(inconfs[i]);
    }
    return(cpl_test_end(0));
}

/*

$Log: casu_imdither-test.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.1  2015/01/09 11:39:55  jim
new entry


*/
