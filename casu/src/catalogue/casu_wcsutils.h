/* $Id: casu_wcsutils.h,v 1.3 2015/09/22 15:09:31 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/09/22 15:09:31 $
 * $Revision: 1.3 $
 * $Name:  $
 */

/* Includes */

#ifndef CASU_WCSUTILS_H
#define CASU_WCSUTILS_H

#include <cpl.h>

/* Useful constants */

#define DEGRAD 57.2957795130823229

extern void casu_xytoradec(cpl_wcs *wcs, double x, double y, double *ra,
                           double *dec);
extern void casu_radectoxy(cpl_wcs *wcs, double ra, double dec, double *x,
                           double *y);
extern void casu_xytoxy_list(cpl_wcs *wcs1, cpl_wcs *wcs2, int nc, double *x1,
                             double *y1, double *x2, double *y2);
extern void casu_radectoxieta(cpl_wcs *wcs, double ra, double dec, 
                              double *xi, double *eta);
extern int casu_coverage(cpl_propertylist *plist, int fudge, double *ra1, 
                         double *ra2, double *dec1, double *dec2, 
                         int *status);
extern int casu_crpixshift(cpl_propertylist *p, double scalefac, double sh[]);
extern int casu_rescalecd(cpl_propertylist *p, double scalefac);
extern int casu_diffxywcs(cpl_wcs *wcs, cpl_wcs *wcsref, float *xoff,
                          float *yoff, int *status);
extern int casu_removewcs(cpl_propertylist *p, int *status);
extern int casu_tabwcs(cpl_propertylist *p, int xcol, int ycol, int *status);

#endif

/*

$Log: casu_wcsutils.h,v $
Revision 1.3  2015/09/22 15:09:31  jim
Fixed guard

Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.3  2015/01/29 11:56:27  jim
modified comments

Revision 1.2  2013/11/21 09:38:14  jim
detabbed

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/
