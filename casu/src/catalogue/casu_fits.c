/* $Id: casu_fits.c,v 1.4 2015/09/14 18:45:51 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/09/14 18:45:51 $
 * $Revision: 1.4 $
 * $Name:  $
 */

/* Includes */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <math.h>
#include <string.h>

#include <cpl.h>
#include "casu_utils.h"
#include "casu_fits.h"

/**
    \defgroup casu_fits casu_fits
    \ingroup casu_routines

    \brief
    These are methods for manipulating the casu_fits object

    \author
    Jim Lewis, CASU
*/

/**@{*/

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_load
    \par Purpose:
        Load an input image into a casu_fits object
    \par Description:
        An image from an input cpl_frame is loaded into a casu_fits object.
        Only the image for a given extension number is loaded. The rest
        of the object properties are initialised
    \par Language:
        C
    \param frame
        The input cpl_frame object
    \param type
        The data type for the loaded image
    \param nexten
        The image extension that you want to load.
    \return
        The output casu_fits object or NULL if there was a problem
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern casu_fits *casu_fits_load(cpl_frame *frame, cpl_type type, int nexten) {
    casu_fits *p;
    cpl_image *im,*im2;
    cpl_propertylist *ph,*eh;
    int nf;
    char extname[81] = "";
    const char *fctid = "casu_fits_load";

    /* Check for nonsense input */

    if (frame == NULL)
        return(NULL);

    /* See if you can load the image */

    im = cpl_image_load(cpl_frame_get_filename(frame),type,0,(cpl_size)nexten);
    if (im == NULL) {
        cpl_msg_error(fctid,"Unable to load %s[%" CPL_SIZE_FORMAT "] -- %s",
                      cpl_frame_get_filename(frame),(cpl_size)nexten,
                      cpl_error_get_message());
        cpl_error_reset();
        return(NULL);
    }

    /* If this is an unspecified type, the force it to be float */

    if (type == CPL_TYPE_UNSPECIFIED && 
        cpl_image_get_type(im) != CPL_TYPE_FLOAT) {
        im2 = cpl_image_cast(im,CPL_TYPE_FLOAT);
        cpl_image_delete(im);
        im = im2;
    }   

    /* Get the casu_fits structure */

    p = cpl_malloc(sizeof(casu_fits));

    /* Load stuff in */

    p->image = im;
    p->nexten = nexten;
    p->phu = NULL;
    p->ehu = NULL;
    p->fname = cpl_strdup(cpl_frame_get_filename(frame));
    p->type = type;
    p->status = CASU_OK;

    /* What type of file do we have? Also extract the extension name if
       it exists. */

    p->extname = NULL;
    if (cpl_frame_get_nextensions(frame) == 0) {
        p->casufitstype = CASU_FITS_SIMPLE;
        p->extname = cpl_strdup("0");
    } else { 
        ph = cpl_propertylist_load(p->fname,0);
        eh = cpl_propertylist_load(p->fname,nexten);
        if (cpl_propertylist_get_int(ph,"NAXIS") != 0) {
            p->casufitstype = CASU_FITS_MEF_NOPHU;
            if (cpl_propertylist_has(eh,"EXTNAME")) {
                strcpy(extname,cpl_propertylist_get_string(eh,"EXTNAME"));
                if (strcmp(extname,"COMPRESSED_IMAGE")) {
                    p->extname = cpl_strdup(extname);
                } else {
                    (void)sprintf(extname,"%d",nexten);
                    p->extname = cpl_strdup(extname);
                }
            }
        } else if (cpl_propertylist_has(eh,"ZSIMPLE")) {
            p->casufitstype = CASU_FITS_SIMPLE_CMP;
            p->extname = cpl_strdup("0");
        } else {
            p->casufitstype = CASU_FITS_MEF;
            if (cpl_propertylist_has(eh,"EXTNAME")) {
                strcpy(extname,cpl_propertylist_get_string(eh,"EXTNAME"));
                if (strcmp(extname,"COMPRESSED_IMAGE")) {
                    p->extname = cpl_strdup(extname);
                } else {
                    (void)sprintf(extname,"%d",nexten);
                    p->extname = cpl_strdup(extname);
                }
            }
        }
        cpl_propertylist_delete(eh);
        cpl_propertylist_delete(ph);
    }
    if (p->extname == NULL) {
        (void)sprintf(extname,"%d",nexten);
        p->extname = cpl_strdup(extname);
    }
        
    /* Create full name string */

    nf = strlen(p->extname) + strlen(p->fname) + 3;
    p->fullname = cpl_malloc(nf);
    (void)snprintf(p->fullname,nf,"%s[%s]",p->fname,p->extname);
   
    /* Get out of here */

    return(p);
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_unload_im
    \par Purpose:
        Unload the image from a casu_fits structure
    \par Description:
        The image is unloaded from a casu_fits structure. This might be
        done as a means of saving memory on large images.
    \par Language:
        C
    \param in
        The input casu_fits object
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern void casu_fits_unload_im(casu_fits *in) {

    freeimage(in->image);
}


/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_duplicate
    \par Purpose:
        Copy a casu_fits structure into another one.
    \par Description:
        An input casu_fits structure is duplcated and returned
    \par Language:
        C
    \param in
        The input casu_fits object
    \return
        The output casu_fits object.
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern casu_fits *casu_fits_duplicate(casu_fits *in) {
    casu_fits *p;

    /* Check for nonsense input */

    if (in == NULL)
        return(NULL);

    /* Get the casu_fits structure */

    p = cpl_malloc(sizeof(casu_fits));

    /* Now copy everything over */

    if (in->image != NULL)
        p->image = cpl_image_duplicate(in->image);
    else
        p->image = NULL;
    p->phu = cpl_propertylist_duplicate(casu_fits_get_phu(in));
    p->ehu = cpl_propertylist_duplicate(casu_fits_get_ehu(in));
    p->fname = cpl_strdup(in->fname);
    p->extname = cpl_strdup(in->extname);
    p->fullname = cpl_strdup(in->fullname);
    p->nexten = in->nexten;
    p->status = in->status;
    p->casufitstype = in->casufitstype;
    p->type = in->type;
   
    /* Get out of here */

    return(p);
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_replace_image
    \par Purpose:
        Replace the image in a casu_fits structure with a new one
    \par Description:
        The image in an input casu_fits structure is deleted and replaced
        with a new one. This should be used with caution, but can be useful
        for subsetting and image as when we trim the overscan regions off.
    \par Language:
        C
    \param in
        The input casu_fits object
    \param image
        The new image
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern void casu_fits_replace_image(casu_fits *in, cpl_image *image) {

    /* Check for nonsense input */

    if (in == NULL || image == NULL)
        return;

    /* Delete the old image and replace it with the new one */

    if (in->image != NULL)
        cpl_image_delete(in->image);
    in->image = image;
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_load_list
    \par Purpose:
        Load a list of input images into a casu_fits object array
    \par Description:
        Images from an input cpl_frameset are loaded into a list of 
        casu_fits objects. Only the images for a given extension number are
        loaded. The rest of the object properties are initialised
    \par Language:
        C
    \param f
        The input cpl_frameset object
    \param type
        The data type for the loaded images
    \param exten
        The image extension that you want to load.
    \return
        The output casu_fits object list or NULL if there was a problem
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern casu_fits **casu_fits_load_list(cpl_frameset *f, cpl_type type, 
                                       int exten) {
    int i;
    casu_fits **p;

    /* Check for nonsense input */

    if (f == NULL)
        return(NULL);

    /* Get some workspace */

    p = cpl_malloc(cpl_frameset_get_size(f)*sizeof(casu_fits *));
    
    /* Now load each of the frames... */

    for (i = 0; i < cpl_frameset_get_size(f); i++) {
        p[i] = casu_fits_load(cpl_frameset_get_position(f,i),type,exten);
        if (p[i] == NULL) {
            casu_fits_delete_list(p,i-1);
            return(NULL);
        }
    }

    /* Now return the array */

    return(p);
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_delete
    \par Purpose:
        Free all the workspace associated with a casu_fits object
    \par Description:
        Free all the workspace associated with a casu_fits object
    \par Language:
        C
    \param p
        The input casu_fits object
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern void casu_fits_delete(casu_fits *p) {

    /* Check for nonsense input */

    if (p == NULL)
        return;

    /* Free up workspace if it's been used */

    freeimage(p->image);
    freepropertylist(p->phu);
    freepropertylist(p->ehu);
    freespace(p->fname);
    freespace(p->extname);
    freespace(p->fullname);
    cpl_free(p);
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_delete_list
    \par Purpose:
        Free all the workspace associated with a list of casu_fits objects
    \par Description:
        Free all the workspace associated with a list of casu_fits objects
    \par Language:
        C
    \param p
        The input list of casu_fits objects
    \param n
        The number of casu_fits objects in the above array
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern void casu_fits_delete_list(casu_fits **p, int n) {
    int i;

    /* Check for nonsense input */

    if (p == NULL)
        return;

    /* Free up workspace if it's been used */

    for (i = 0; i < n; i++)
        casu_fits_delete(p[i]);
    cpl_free(p);
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_get_image
    \par Purpose:
        Get the CPL image from the casu_fits object
    \par Description:
        Return the CPL image from the input casu_fits object. This image is
        suitable for use in all cpl_image routines.
    \par Language:
        C
    \param p
        The input casu_fits object
    \return 
        The cpl_image object. NULL if there was an error.
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern cpl_image *casu_fits_get_image(casu_fits *p) {
    const char *fctid = "casu_fits_get_image";
    cpl_image *im2;
    
    /* Check for nonsense input */

    if (p == NULL)
        return(NULL);
    
    /* If the image is already loaded, then return it now */

    if (p->image != NULL)
        return(p->image);

    /* If the image hasn't been loaded, then do that now */
    
    if (p->image == NULL) 
        p->image = cpl_image_load(p->fname,p->type,0,(cpl_size)(p->nexten));
    if (p->image == NULL) {
        cpl_msg_error(fctid,"Unable to load %s[%" CPL_SIZE_FORMAT "] -- %s\n",
                      p->fname,(cpl_size)(p->nexten),
                      cpl_error_get_message());
        cpl_error_reset();
        return(NULL);
    } 

    /* If this is an unspecified type, the force it to be float */

    if (p->type == CPL_TYPE_UNSPECIFIED && 
        cpl_image_get_type(p->image) != CPL_TYPE_FLOAT) {
        im2 = cpl_image_cast(p->image,CPL_TYPE_FLOAT);
        cpl_image_delete(p->image);
        p->image = im2;
    }

    /* Return it */

    return(p->image);
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_get_nexten
    \par Purpose:
        Get the FITS extension number for the current image in a casu_fits 
        object
    \par Description:
        Get the FITS extension number for the current image in a casu_fits 
        object
    \par Language:
        C
    \param p
        The input casu_fits object
    \return 
        The extension number (-1 in case of error)
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern int casu_fits_get_nexten(casu_fits *p) {
    
    /* Check for nonsense input */

    if (p == NULL)
        return(-1);

    /* Return it */

    return(p->nexten);
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_get_phu
    \par Purpose:
        Get the propertylist for the primary header for a given casu_fits image.
    \par Description:
        Get the propertylist for the primary header for a given casu_fits image.
        This should only need to be read once and then can be used to add
        things to the primary header.
    \par Language:
        C
    \param p
        The input casu_fits object
    \return 
        The propertylist representing the primary header of the input image
        (NULL if there is an error).
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern cpl_propertylist *casu_fits_get_phu(casu_fits *p) {

    /* Check for nonsense input */

    if (p == NULL)
        return(NULL);

    /* If the propertylist hasn't already been loaded, then do it now */

    if (p->phu == NULL) {
        if (p->casufitstype == CASU_FITS_MEF) 
            p->phu = cpl_propertylist_load(p->fname,0);
        else
            p->phu = cpl_propertylist_load(p->fname,p->nexten);
    }
    
    /* Return it */

    return(p->phu);
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_get_ehu
    \par Purpose:
        Get the propertylist for the extension header for a given casu_fits 
        image.
    \par Description:
        Get the propertylist for the extension header for a given casu_fits 
        image. This is the extension that is relevant of the image.
        This should only need to be read once and then can be used to add
        things to the primary header.
    \par Language:
        C
    \param p
        The input casu_fits object
    \return 
        The propertylist representing the extension header of the input image
        (NULL if there is an error).
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern cpl_propertylist *casu_fits_get_ehu(casu_fits *p) {

    /* Check for nonsense input */

    if (p == NULL)
        return(NULL);

    /* If the propertylist hasn't already been loaded, then do it now */

    if (p->ehu == NULL) 
        p->ehu = cpl_propertylist_load(p->fname,(cpl_size)(p->nexten));
    
    /* Return it */

    return(p->ehu);
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_get_extname
    \par Purpose:
        Get the extension name for a give casu_fits object
    \par Description:
        Get the extension name for a given casu_fits object.
    \par Language:
        C
    \param p
        The input casu_fits object
    \return 
        The pointer to the extension name in the casu_fits structure -- not a 
        copy! (NULL if there is an error).
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern char *casu_fits_get_extname(casu_fits *p) {

    /* Check for nonsense input */

    if (p == NULL)
        return(NULL);

    /* Return it */

    return(p->extname);
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_get_filename
    \par Purpose:
        Get the filename from which the current casu_fits object originated
    \par Description:
        Get the filename from which the current casu_fits object originated. If
        this is null, then the image didn't originate in an FITS file.
    \par Language:
        C
    \param p
        The input casu_fits object
    \return 
        The name of the file from which this image originated. This is
        not a copy of the name, so be careful not to modify the string.
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern char *casu_fits_get_filename(casu_fits *p) {

    /* Check for nonsense input */

    if (p == NULL)
        return(NULL);

    /* Return it */

    return(p->fname);
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_get_fullname
    \par Purpose:
        Get the fullname of the FITS extension from which the current 
        casu_fits object originated
    \par Description:
        Get the fullname of the FITS extension  from which the current 
        casu_fits object originated. If this is null, then the image didn't 
        originate in an FITS file.
    \par Language:
        C
    \param p
        The input casu_fits object
    \return 
        The fullname name of the file from which this image originated
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern char *casu_fits_get_fullname(casu_fits *p) {

    /* Check for nonsense input */

    if (p == NULL)
        return(NULL);

    /* Return it */

    return(p->fullname);
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_get_status
    \par Purpose:
        Get the error status of the current object.
    \par Description:
        Get the error status of the current object.
    \par Language:
        C
    \param p
        The input casu_fits object
    \return 
        The error status
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern int casu_fits_get_status(casu_fits *p) {
    
    /* Check for nonsense input */

    if (p == NULL)
        return(CASU_FATAL);

    /* Return it */
   
    return(p->status);
}
  
/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_set_error
    \par Purpose:
        Set the error status and message for an object
    \par Description:
        The input status is checked to see if there has been a problem
        with the current object. If there has been, then the status is
        stored away and any error message from the cpl_error system is
        copied down.
    \par Language:
        C
    \param p
        The input casu_fits object
    \param status
        The input error status
    \return 
        A flag to say whether the input status was fatal.
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern int casu_fits_set_error(casu_fits *p, int status) {

    /* Check for nonsense input */

    if (p == NULL)
        return(0);

    /* Get out of here if the status is OK */

    if (status == CASU_OK)
        return(0);

    /* Set the error status if there was an error */

    p->status = status;

    /* If there was a cpl error then spew that out now */

    if (cpl_error_get_code() != CPL_ERROR_NONE) {
        cpl_msg_error("","%s",cpl_error_get_message());
        cpl_error_reset();
    }
    
    /* Get out of here */

    if (status == CASU_FATAL)
        return(1);
    else
        return(0);
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_set_status
    \par Purpose:
        Set the error status
    \par Description:
        The input status is checked to see if there has been a problem
        with the current object. If there has been, then the status is
        stored.
    \par Language:
        C
    \param p
        The input casu_fits object
    \param status
        The input error status
    \return 
        None
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern void casu_fits_set_status(casu_fits *p, int status) {

    /* Check for nonsense input */

    if (p == NULL)
        return;

    /* Update the status */

    p->status = status;

}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_set_filename
    \par Purpose:
        Set the file name associated with a casu_fits structure
    \par Description:
        Set the file name associated with a casu_fits structure. This is
        useful if you have wrapped a cpl_image in a structure and you don't
        have a physical file to refer to. The name can be used to define
        an output file name if you ultimately decide to write this out.
        If a name already exists, it will be overwritten 
    \par Language:
        C
    \param p
        The input casu_fits object
    \param fname
        The input file name.
    \return 
        Nothing
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern void casu_fits_set_filename(casu_fits *p, char *fname) {

    /* Check for nonsense input */

    if (p == NULL || fname == NULL)
        return;

    /* Set the name up and get out of here */

    freespace(p->fname);
    p->fname = cpl_strdup(fname);   
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_wrap
    \par Purpose:
        Wrap an image in a casu_fits wrapper
    \par Description:
        The input image is inserted into a casu_fits wrapper. A model
        casu_fits object may be provided to give the new object
        headers. If the phu and ehu parameters are not null then they will
        be used as the propertylists for the new object. If not, then
        an attempt will be made to copy the propertylists from the model.
        If the model and the propertylists are both NULL, then empty
        propertylists are given
    \par Language:
        C
    \param im
        The input cpl_image
    \param model
        The input casu_fits model object
    \param phu
        The input propertylist for the extension header for the new object.
    \param ehu
        The input propertylist for the extension header for the new object.
    \return 
        The new casu_fits structure.
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern casu_fits *casu_fits_wrap(cpl_image *im, casu_fits *model, 
                                 cpl_propertylist *phu, cpl_propertylist *ehu) {
    casu_fits *p;

    /* Check for nonsense input */

    if (im == NULL)
        return(NULL);

    /* Get the casu_fits structure */

    p = cpl_malloc(sizeof(casu_fits));

    /* Load stuff in */

    p->image = im;
    p->nexten = -1;
    if (phu != NULL) 
        p->phu = cpl_propertylist_duplicate(phu);
    else if (model != NULL) 
        p->phu = cpl_propertylist_duplicate(casu_fits_get_phu(model));
    else
        p->phu = cpl_propertylist_new();
    if (ehu != NULL)
        p->ehu = cpl_propertylist_duplicate(ehu);
    else if (model != NULL) 
        p->ehu = cpl_propertylist_duplicate(casu_fits_get_ehu(model));
    else 
        p->ehu = cpl_propertylist_new();
    p->fname = NULL;
    p->status = CASU_OK;
    p->extname = NULL;
    p->fullname = NULL;
    if (model != NULL) 
        p->casufitstype = model->casufitstype;
    else 
        p->casufitstype = CASU_FITS_MEF;
    p->type = cpl_image_get_type(im);
   
    /* Get out of here */

    return(p);
}
/*---------------------------------------------------------------------------*/
/**
    \par Name:
        casu_fits_unwrap
    \par Purpose:
        Get rid of the casu_fits structure but leave the image
    \par Description:
        The input casu_fits structure is deallocated leaving the image behind
        Make sure you have a reference to the original image when using
        this routine so that it can be deleted explicitly later
    \par Language:
        C
    \param p
        The input casu_fits
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern void casu_fits_unwrap(casu_fits *p) {

    /* Check for nonsense input */

    if (p == NULL)
        return;

    /* Free up workspace if it's been used */

    freepropertylist(p->phu);
    freepropertylist(p->ehu);
    freespace(p->fname);
    freespace(p->extname);
    freespace(p->fullname);
    cpl_free(p);
}
    
/**@}*/

/*

$Log: casu_fits.c,v $
Revision 1.4  2015/09/14 18:45:51  jim
replaced a free by cpl_free

Revision 1.3  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.2  2015/08/06 05:34:02  jim
Fixes to get rid of compiler moans

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.10  2015/01/29 11:48:15  jim
modified comments

Revision 1.9  2015/01/09 12:13:15  jim
*** empty log message ***

Revision 1.8  2014/12/11 12:23:33  jim
new version

Revision 1.7  2014/04/09 11:08:21  jim
Get rid of a couple of compiler moans

Revision 1.6  2014/04/09 09:09:51  jim
Detabbed

Revision 1.5  2014/03/26 15:38:01  jim
Modified so that using type CPL_TYPE_UNSPECIFIED will force a floating
point image. Also removed calls to deprecated cpl routine

Revision 1.4  2013/11/21 09:38:13  jim
detabbed

Revision 1.3  2013/11/08 06:51:39  jim
added casu_fits_unload_im

Revision 1.2  2013-10-24 09:18:37  jim
Now tracks the type of fits file using casufitstype

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/
