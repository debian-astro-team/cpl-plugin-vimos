/* $Id: casu_fits.h,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */


/* Includes */

#ifndef CASU_FITS_H
#define CASU_FITS_H

#include <cpl.h>

/* Macros to define the type of FITS file we might have here */

#define CASU_FITS_MEF        0
#define CASU_FITS_SIMPLE     1
#define CASU_FITS_SIMPLE_CMP 2
#define CASU_FITS_MEF_NOPHU  3

/* CASU FITS structure. */

typedef struct {
    cpl_image        *image;
    cpl_propertylist *phu;
    cpl_propertylist *ehu;
    char             *fname;
    char             *extname;
    char             *fullname;
    int              nexten;
    int              status;
    int              casufitstype;
    cpl_type         type;
} casu_fits;

/* CASU_FITS method prototypes */

extern casu_fits *casu_fits_load(cpl_frame *frame, cpl_type type, int nexten);
extern void casu_fits_unload_im(casu_fits *in);
extern casu_fits *casu_fits_duplicate(casu_fits *in);
extern void casu_fits_replace_image(casu_fits *in, cpl_image *image);
extern casu_fits **casu_fits_load_list(cpl_frameset *f, cpl_type type, 
                                       int exten);
extern void casu_fits_delete(casu_fits *p);
extern void casu_fits_delete_list(casu_fits **p, int n);
extern cpl_image *casu_fits_get_image(casu_fits *p);
extern int casu_fits_get_nexten(casu_fits *p);
extern char *casu_fits_get_filename(casu_fits *p);
extern cpl_propertylist *casu_fits_get_phu(casu_fits *p);
extern cpl_propertylist *casu_fits_get_ehu(casu_fits *p);
extern char *casu_fits_get_extname(casu_fits *p);
extern char *casu_fits_get_fullname(casu_fits *p);
extern int casu_fits_get_status(casu_fits *p);
extern void casu_fits_set_filename(casu_fits *p, char *fname);
extern int casu_fits_set_error(casu_fits *p, int status);
extern void casu_fits_set_status(casu_fits *p, int status);
extern casu_fits *casu_fits_wrap(cpl_image *im, casu_fits *model, 
                                 cpl_propertylist *phu, cpl_propertylist *ehu);
extern void casu_fits_unwrap(casu_fits *p);

#endif

/*

$Log: casu_fits.h,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.5  2015/01/29 11:48:15  jim
modified comments

Revision 1.4  2013/11/21 09:38:13  jim
detabbed

Revision 1.3  2013/11/08 06:51:53  jim
Added casu_fits_unload_im

Revision 1.2  2013-10-24 09:18:37  jim
Now tracks the type of fits file using casufitstype

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/
