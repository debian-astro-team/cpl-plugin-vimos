/* $Id: create_table_4.c,v 1.3 2015/08/12 11:16:55 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/12 11:16:55 $
 * $Revision: 1.3 $
 * $Name:  $
 */

#include <stdio.h>
#include <math.h>
#include "imcore.h"
#include "util.h"
#include "casu_fits.h"

/**@{*/

/*---------------------------------------------------------------------------*/
/**
    \ingroup cataloguemodules
    \brief Initialise type 4 catalogue (object mask)
  
    \par Name:
        imcore_tabinit_4
    \par Purpose:
        Initialise type 4 catalogue (object mask)
    \par Description:
        Type 4 catalogue is initialised. This is in fact an object mask.
    \par Language:
        C
    \param ap
        The current ap structure
    \returns
        Nothing
    \par QC headers:
        None
    \par DRS headers:
        None
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern void imcore_tabinit_4(ap_t *ap, int *imcore_xcol, int *imcore_ycol,
                             cpl_table **tab) {
    long npts,i;
    cpl_binary *opm;

    /* The output table doesn't exist */

    *tab = NULL;
    npts = (ap->lsiz)*(ap->csiz);
    ap->opmask = cpl_mask_new(ap->lsiz,ap->csiz);
    opm = cpl_mask_get_data(ap->opmask);
    for (i = 0; i < npts; i++) 
        opm[i] = 0;

    /* Undefined RA and Dec columns */

    *imcore_xcol = -1;
    *imcore_ycol = -1;
    
}

/*---------------------------------------------------------------------------*/
/**
    \ingroup cataloguemodules
    \brief Dummy seeing routine for type 4 catalogue (object mask)
  
    \par Name:
        imcore_do_seeing_4
    \par Purpose:
        Do seeing estimate for type 4 catalogue (object mask)
    \par Description:
        This is a dummy routine
    \par Language:
        C
    \param ap
        The current ap structure
    \retval CASU_OK
        If all is well. Currently this is the only return value
    \par QC headers:
        None
    \par DRS headers:
        None
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern int imcore_do_seeing_4(ap_t *ap) {

    /* Get out of here */

    ap->fwhm = 0.0;
    return(CASU_OK);
}
        
/*---------------------------------------------------------------------------*/
/**
    \ingroup cataloguemodules
    \brief Process results for type 4 catalogue (object mask)
  
    \par Name:
        imcore_process_results_4
    \par Purpose:
        Create the results for objects in a type 4 catalogue (object mask)
    \par Description:
        The positions for pixels identified as being part of a detected
        object are flagged in the object pixel mask.
    \par Language:
        C
    \param ap
        The current ap structure
    \retval CASU_OK
        If all is well. This is currently the only value
    \par QC headers:
        None
    \par DRS headers:
        None
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/
        
extern int imcore_process_results_4(ap_t *ap) {
    int i,j,np;
    long nx;
    plstruct *plarray;
    cpl_binary *opm;

    /* Loop for each object in the array */

    opm = cpl_mask_get_data(ap->opmask);
    plarray = ap->plarray;
    np = ap->npl_pix;
    nx = ap->lsiz;
    for (i = 0; i < np; i++) {
        j = nx*(plarray[i].y - 1) + plarray[i].x - 1;
        opm[j] = 2;
    }

    /* Get outta here */

    return(CASU_OK);
}

        
/*---------------------------------------------------------------------------*/
/**
    \ingroup cataloguemodules
    \brief Close object mask
  
    \par Name:
        imcore_tabclose_4
    \par Purpose:
        Close the object mask
    \par Description:
        The object mask array in the ap structure is freed and the internal
        mask in the input image is marked with the object locations.
    \par Language:
        C
    \param ap
        The current ap structure
    \retval CASU_OK
        If all is well. This is currently the only value
    \par QC headers:
        None
    \par DRS headers:
        None
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern int imcore_tabclose_4(ap_t *ap) {

    cpl_image_reject_from_mask(ap->inframe,ap->opmask);
    cpl_mask_delete(ap->opmask);
    return(CASU_OK);
}

/**@}*/

/*

$Log: create_table_4.c,v $
Revision 1.3  2015/08/12 11:16:55  jim
Modified procedure names to protect namespace

Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.4  2015/01/09 11:42:36  jim
Fixed routines to remove globals

Revision 1.3  2014/12/11 12:23:34  jim
new version

Revision 1.2  2014/04/09 09:09:51  jim
Detabbed

Revision 1.1.1.1  2013/08/27 12:07:48  jim
Imported


*/
