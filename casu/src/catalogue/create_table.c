/* $Id: create_table.c,v 1.3 2015/08/12 11:16:55 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/12 11:16:55 $
 * $Revision: 1.3 $
 * $Name:  $
 */

#include <stdio.h>
#include <string.h>
#include "imcore.h"


/**@{*/

/*---------------------------------------------------------------------------*/
/**
    \ingroup cataloguemodules
    \brief Initialise catalogues
  
    \par Name:
        imcore_tabinit
    \par Purpose:
        Initialise the output table.
    \par Description:
        Wrapper routine to call the relevant initialisation routine for
        each of the allowed types of catalogues.
    \par Language:
        C
    \param ap
        The current ap structure
    \returns
        Nothing
    \par QC headers:
        None
    \par DRS headers:
        None
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern void imcore_tabinit(ap_t *ap, int *xcol, int *ycol, int cattype, 
                           cpl_table **tab) {

    switch (cattype) {
    case CAT_INTWFC:
        imcore_tabinit_1(xcol,ycol,tab);
        break;
    case CAT_WFCAM:
        imcore_tabinit_2(xcol,ycol,tab);
        break;
    case CAT_BASIC:
        imcore_tabinit_3(xcol,ycol,tab);
        break;
    case CAT_OBJMASK:
        imcore_tabinit_4(ap,xcol,ycol,tab);
        break;
    case CAT_VIRCAM:
        imcore_tabinit_6(xcol,ycol,tab);
        break;
    default:
        cpl_msg_error("imcore_tabinit","Option %" CPL_SIZE_FORMAT " does not exist",
                      (cpl_size)cattype);
        *tab = NULL;
        break;
    }
}

/*---------------------------------------------------------------------------*/
/**
    \ingroup cataloguemodules
    \brief Do seeing estimate
  
    \par Name:
        imcore_do_seeing
    \par Purpose:
        Do the seeing estimate 
    \par Description:
        Wrapper routine to call the relevant routine to work out the seeing
        for each of the allowed types of catalogues
    \par Language:
        C
    \param ap
        The current ap structure
    \retval CASU_OK
        If all went well
    \retval CASU_FATAL
        If catalogue type is unrecognised
    \par QC headers:
        None
    \par DRS headers:
        None
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern int imcore_do_seeing(ap_t *ap, int cattype, int nobjects, 
                            cpl_table *tab) {
    int status;

    switch (cattype) {
    case CAT_INTWFC:
        status = imcore_do_seeing_1(ap,nobjects,tab);
        break;
    case CAT_WFCAM:
        status = imcore_do_seeing_2(ap,nobjects,tab);
        break;
    case CAT_BASIC:
        status = imcore_do_seeing_3(ap,nobjects,tab);
        break;
    case CAT_OBJMASK:
        status = imcore_do_seeing_4(ap);
        break;
    case CAT_VIRCAM:
        status = imcore_do_seeing_6(ap,nobjects,tab);
        break;
    default:
        status = CASU_FATAL;
        cpl_msg_error("imcore_do_seeing","Option %" CPL_SIZE_FORMAT " does not exist",
                      (cpl_size)cattype);
        break;
    }
    return(status);
}

/*---------------------------------------------------------------------------*/
/**
    \ingroup cataloguemodules
    \brief Process results
  
    \par Name:
        imcore_process_results
    \par Purpose:
        Process the results for each object and store them in the table
    \par Description:
        Wrapper routine to call the relevant routine to work out the results
        for each of the allowed types of catalogues
    \par Language:
        C
    \param ap
        The current ap structure
    \retval CASU_OK
        If all went well
    \retval CASU_FATAL
        If catalogue type is unrecognised
    \par QC headers:
        None
    \par DRS headers:
        None
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern int imcore_process_results(ap_t *ap, int cattype, float gain, 
                                  int *nobjects, cpl_table *tab) {
    int status;
    
    switch (cattype) {
    case CAT_INTWFC:
        status = imcore_process_results_1(ap,nobjects,tab);
        break;
    case CAT_WFCAM:
        status = imcore_process_results_2(ap,gain,nobjects,tab);
        break;
    case CAT_BASIC:
        status = imcore_process_results_3(ap,nobjects,tab);
        break;
    case CAT_OBJMASK:
        status = imcore_process_results_4(ap);
        break;
    case CAT_VIRCAM:
        status = imcore_process_results_6(ap,gain,nobjects,tab);
        break;
    default:
        status = CASU_FATAL;
        cpl_msg_error("imcore_process_result","Option %" CPL_SIZE_FORMAT " does not exist",
                      (cpl_size)cattype);
        break;
    }
    return(status);
}

/*---------------------------------------------------------------------------*/
/**
    \ingroup cataloguemodules
    \brief Close the table structure
  
    \par Name:
        imcore_tabclose
    \par Purpose:
        Close the table structure
    \par Description:
        Wrapper routine to call the relevant routine to close the table
        for each of the allowed types of catalogues
    \par Language:
        C
    \param ap
        The current ap structure
    \retval CASU_OK
        If all went well
    \retval CASU_FATAL
        If catalogue type is unrecognised
    \par QC headers:
        None
    \par DRS headers:
        None
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern int imcore_tabclose(ap_t *ap, int cattype) {
    int status;

    switch (cattype) {
    case CAT_OBJMASK:
        status = imcore_tabclose_4(ap);
        break;
    default:
        status = CASU_OK;
        break;
    }
    return(status);
}

/*---------------------------------------------------------------------------*/
/**
    \ingroup cataloguemodules
    \brief Initialise tables (generic)
  
    \par Name:
        imcore_tabinit_gen
    \par Purpose:
        Initialise tables (generic)
    \par Description:
        Generic routine to create FITS tables for the output catalogues
    \par Language:
        C
    \param ncols
        The number of columns in the table
    \param ttype
        Array of column names for FITS table
    \param tunit
        Array of units for each of the columns
    \param tform
        Array of formats for each of the columns as defined in the FITS
        standard
    \returns
        Nothing
    \par QC headers:
        None
    \par DRS headers:
        None
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern void imcore_tabinit_gen(int ncols, const char *ttype[], 
                               const char *tunit[], cpl_type tform[], 
                               cpl_table **tab) {
    int i;
    const char *fctid = "imcore_tabinit_gen";
    
    /* First, create the table with a default number of rows. */

    if ((*tab = cpl_table_new(0)) == NULL) {
        cpl_msg_error(fctid,"Unable to open cpl table!");
        return;
    }

    /* Now define all of the columns */

    for (i = 0; i < ncols; i++) {
        cpl_table_new_column(*tab,ttype[i],tform[i]);
        cpl_table_set_column_unit(*tab,ttype[i],tunit[i]);
    }

}

/*---------------------------------------------------------------------------*/
/**
    \ingroup cataloguemodules
    \brief Do seeing estimate (generic)
  
    \par Name:
        imcore_do_seeing_gen
    \par Purpose:
        Do seeing estimate (generic)
    \par Description:
        Wrapper routine for doing the seeing estimate
    \par Language:
        C
    \param ap
        The current ap structure
    \param col_ellipt
        The name of the column for ellipticity
    \param col_pkht
        The name of the column for the peak height
    \param col_areals
        The array of names of the areal profile columns
    \retval CASU_OK
        If all is ok. This is currently the only value.
    \par QC headers:
        None
    \par DRS headers:
        None
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern int imcore_do_seeing_gen(ap_t *ap, const char *col_ellipt, 
                         const char *col_pkht, char *col_areals[NAREAL],
                         int nobjects, cpl_table *tab) {
    int i;
    float fwhm,*areal[NAREAL],*ellipt,*pkht,*work;

    /* Get some space and read the relevant columns */
    
    work = NULL;
    if (nobjects >= 3) {
        ellipt = cpl_table_get_data_float(tab,col_ellipt);
        pkht = cpl_table_get_data_float(tab,col_pkht);
        work = cpl_malloc(nobjects*sizeof(*work));
        for (i = 0; i < NAREAL; i++)
            areal[i] = cpl_table_get_data_float(tab,col_areals[i]);

        /* Do the seeing calculation */

        imcore_seeing(ap,nobjects,ellipt,pkht,areal,work,&fwhm);
    } else 
        fwhm = 0.0;
    ap->fwhm = fwhm;

    /* Get out of here */

    freespace(work);
    return(CASU_OK);
}

/**@}*/

/*

$Log: create_table.c,v $
Revision 1.3  2015/08/12 11:16:55  jim
Modified procedure names to protect namespace

Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.3  2015/01/09 11:42:36  jim
Fixed routines to remove globals

Revision 1.2  2014/04/09 09:09:51  jim
Detabbed

Revision 1.1.1.1  2013/08/27 12:07:48  jim
Imported


*/
