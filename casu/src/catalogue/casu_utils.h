/* $Id: casu_utils.h,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utiliies
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */


/* Includes */

#ifndef CASU_UTILS_H
#define CASU_UTILS_H

#include <cpl.h>
#include "casu_fits.h"
#include "casu_tfits.h"

/* Useful Macros */

#define casu_nint(_x) ((int)(_x + ((_x) < 0.0 ? -0.5 : 0.5)))
#ifndef __cplusplus
#define min(_x,_y) (_x < _y ? _x : _y)
#define max(_x,_y) (_x > _y ? _x : _y)
#endif

/* Macros for freeing up space */

#define freespace(_p) if (_p != NULL) {cpl_free(_p); _p = NULL;}
#define freespace2(_p,_n) if (_p != NULL) {int _nfs2; for (_nfs2 = 0; _nfs2 < _n; _nfs2++) {freespace(_p[_nfs2])}}; freespace(_p);
#define freeframe(_p) if (_p != NULL) {cpl_frame_delete(_p); _p = NULL;}
#define freeimage(_p) if (_p != NULL) {cpl_image_delete(_p); _p = NULL;}
#define freeframeset(_p) if (_p != NULL) {cpl_frameset_delete(_p); _p = NULL;}
#define freeplist(_p) if (_p != NULL) {cpl_propertylist_delete(_p); _p = NULL;}
#define freetable(_p) if (_p != NULL) {cpl_table_delete(_p); _p = NULL;}
#define freepropertylist(_p) if (_p != NULL) {cpl_propertylist_delete(_p); _p = NULL;}
#define freeimagelist(_p) if (_p != NULL) {cpl_imagelist_delete(_p); _p = NULL;}
#define freearray(_p) if (_p != NULL) {cpl_array_delete(_p); _p = NULL;}
#define freewcs(_p) if (_p != NULL) {cpl_wcs_delete(_p); _p = NULL;}
#define closefile(_p) if (_p != NULL) {fclose(_p); _p = NULL;}
#define freefits(_p) if (_p != NULL) {casu_fits_delete(_p); _p = NULL;}
#define freefitslist(_p,_n) if (_p != NULL) {casu_fits_delete_list(_p,_n); _p = NULL;}
#define freetfitslist(_p,_n) if (_p != NULL) {casu_tfits_delete_list(_p,_n); _p = NULL;}
#define freetfits(_p) if (_p != NULL) {casu_tfits_delete(_p); _p = NULL;}
#define freemask(_p) if (_p != NULL) {casu_mask_delete(_p); _p = NULL;}

/* Define error return values */

#define CASU_OK 0
#define CASU_WARN 1
#define CASU_FATAL 2

/* Useful error handling macros for routines with inherited status */

#define FATAL_ERROR {*status = CASU_FATAL; return(*status);}
#define WARN_RETURN {*status = CASU_WARN; return(*status);}
#define WARN_CONTINUE {*status = CASU_WARN;}
#define GOOD_STATUS {*status = CASU_OK; return(*status);}

/* Define size for recipe description character variables */

#define SZ_ALLDESC 4096

/* Function prototypes */

extern int casu_compare_tags(const cpl_frame *frame1, const cpl_frame *frame2);
extern cpl_frameset *casu_frameset_subgroup(cpl_frameset *frameset, 
                                            cpl_size *labels, cpl_size nlab, 
                                            const char *tag);
extern cpl_frame *casu_frameset_subgroup_1(cpl_frameset *frameset, 
                                           cpl_size *labels, cpl_size nlab, 
                                           const char *tag);
extern long casu_getnpts(cpl_image *in);
extern void casu_prov(cpl_propertylist *p, casu_fits **inlist, int n, 
                      int isextn);
extern void casu_sort(float **a, int n, int m);
extern void casu_merge_propertylists(cpl_propertylist *p1, 
                                     cpl_propertylist *p2);
extern void casu_dummy_property(cpl_propertylist *p);
extern void casu_rename_property(cpl_propertylist *p, const char *oldname,
                                 char *newname);
extern cpl_image *casu_dummy_image(casu_fits *model);
extern cpl_table *casu_dummy_catalogue(int type);
extern int casu_is_dummy(cpl_propertylist *p);
extern void casu_overexp(casu_fits **fitslist, int *n, int ndit, float lthr, 
                         float hthr, int ditch, float *minv, float *maxv,
                         float *avev);
extern int casu_compare_dims(cpl_image *im1, cpl_image *im2);
extern int casu_gaincor_calc(cpl_frame *frame, int *n, float **cors,
                             int *status);
extern void casu_timestamp(char *out, int n);
extern int casu_catpars(cpl_frame *indx, char **catpath, char **catname);
extern int casu_fndmatch(float x, float y, float *xlist, float *ylist, 
                         int nlist, float err);
extern int casu_findcol(cpl_propertylist *p, const char *col);
extern int casu_night_from_dateobs(char *dateobs);
extern void casu_propertylist_update_float(cpl_propertylist *plist, 
                                           const char *name, float val);
extern void casu_propertylist_update_double(cpl_propertylist *plist, 
                                            const char *name, double val);
#endif

/*

$Log: casu_utils.h,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.10  2015/06/09 18:34:54  jim
Added casu_tfits.h

Revision 1.9  2015/05/13 11:47:34  jim
Added casu_propertylist_update_double

Revision 1.8  2015/03/12 09:16:51  jim
Modified to remove some compiler moans

Revision 1.7  2015/02/14 12:34:15  jim
Added casu_propertylist_update_float

Revision 1.6  2015/01/29 11:56:27  jim
modified comments

Revision 1.5  2014/12/11 12:23:33  jim
new version

Revision 1.4  2013/11/21 09:38:14  jim
detabbed

Revision 1.3  2013-10-24 09:27:55  jim
Added casu_night_from_dateobs

Revision 1.2  2013-09-30 18:13:19  jim
Added freetfitslist macro. Added casu_dummy_catalogue prototype

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/
