#!/usr/bin/env python3

#
# Simple pipeline test by loading all recipes and printing some
# basic information from them.
#

import cpl
recipes = cpl.Recipe.list()
if len(recipes) == 0:
    print("No pipeline recipes found")
    exit(1)

for rname, versions in recipes:
    print("Recipe {0}:".format(rname))
    for v in versions:
        print("    version {0}".format(v))
    recipe = cpl.Recipe(rname)
    print("  Parameters: {0}".format(str(recipe.param)))
    print("  Docstring: {0}".format(recipe.__doc__.strip()))
    print("  Author: {0} <{1}>".format(recipe.__author__.strip(),
                                       recipe.__email__.strip()))
    print("\n")
    del recipe._recipe
