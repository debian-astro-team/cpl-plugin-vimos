# VIMOS_SET_VERSION_INFO(VERSION, [CURRENT], [REVISION], [AGE])
#--------------------------------------------------------------
# Setup various version information, especially the libtool versioning
AC_DEFUN([VIMOS_SET_VERSION_INFO],
[
    vimos_version=`echo "$1" | sed -e 's/[[a-z,A-Z]].*$//'`

    vimos_major_version=`echo "$vimos_version" | \
        sed 's/\([[0-9]]*\).\(.*\)/\1/'`
    vimos_minor_version=`echo "$vimos_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\)\(.*\)/\2/'`
    vimos_micro_version=`echo "$vimos_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`

    if test -z "$vimos_major_version"; then
        vimos_major_version=0
    fi

    if test -z "$vimos_minor_version"; then
        vimos_minor_version=0
    fi

    if test -z "$vimos_micro_version"; then
        vimos_micro_version=0
    fi

    VIMOS_VERSION="$vimos_version"
    VIMOS_MAJOR_VERSION=$vimos_major_version
    VIMOS_MINOR_VERSION=$vimos_minor_version
    VIMOS_MICRO_VERSION=$vimos_micro_version

    if test -z "$4"; then
        VIMOS_INTERFACE_AGE=0
    else
        VIMOS_INTERFACE_AGE="$4"
    fi

    VIMOS_BINARY_AGE=`expr 100 '*' $VIMOS_MINOR_VERSION + $VIMOS_MICRO_VERSION`
    VIMOS_BINARY_VERSION=`expr 10000 '*' $VIMOS_MAJOR_VERSION + \
                          $VIMOS_BINARY_AGE`

    AC_SUBST(VIMOS_VERSION)
    AC_SUBST(VIMOS_MAJOR_VERSION)
    AC_SUBST(VIMOS_MINOR_VERSION)
    AC_SUBST(VIMOS_MICRO_VERSION)
    AC_SUBST(VIMOS_INTERFACE_AGE)
    AC_SUBST(VIMOS_BINARY_VERSION)
    AC_SUBST(VIMOS_BINARY_AGE)

    AC_DEFINE_UNQUOTED(VIMOS_MAJOR_VERSION, $VIMOS_MAJOR_VERSION,
                       [VIMOS major version number])
    AC_DEFINE_UNQUOTED(VIMOS_MINOR_VERSION, $VIMOS_MINOR_VERSION,
                       [VIMOS minor version number])
    AC_DEFINE_UNQUOTED(VIMOS_MICRO_VERSION, $VIMOS_MICRO_VERSION,
                       [VIMOS micro version number])
    AC_DEFINE_UNQUOTED(VIMOS_INTERFACE_AGE, $VIMOS_INTERFACE_AGE,
                       [VIMOS interface age])
    AC_DEFINE_UNQUOTED(VIMOS_BINARY_VERSION, $VIMOS_BINARY_VERSION,
                       [VIMOS binary version number])
    AC_DEFINE_UNQUOTED(VIMOS_BINARY_AGE, $VIMOS_BINARY_AGE,
                       [VIMOS binary age])

    ESO_SET_LIBRARY_VERSION([$2], [$3], [$4])
])


# VIMOS_SET_PATHS
#----------------
# Define auxiliary directories of the installed directory tree.
AC_DEFUN([VIMOS_SET_PATHS],
[

    if test -z "$plugindir"; then
        plugindir='${libdir}/esopipes-plugins/${PACKAGE}-${VERSION}'
    fi

    if test -z "$privatelibdir"; then
        privatelibdir='${libdir}/${PACKAGE}-${VERSION}'
    fi

    if test -z "$pipedocsdir"; then
        pipedocsdir='${datadir}/doc/esopipes/${PACKAGE}-${VERSION}/'
    fi

    htmldir='${pipedocsdir}/html'

    if test -z "$apidocdir"; then
        apidocdir='${pipedocsdir}/html'
    fi

    if test -z "$wkfextradir"; then
        wkfextradir='${datadir}/esopipes/${PACKAGE}-${VERSION}/reflex'
    fi

    if test -z "$wkfcopydir"; then
        wkfcopydir='${datadir}/reflex/workflows/${PACKAGE}-${VERSION}'
    fi

    AC_SUBST(plugindir)
    AC_SUBST(privatelibdir)
    AC_SUBST(pipedocsdir)
    AC_SUBST(apidocdir)
    AC_SUBST(configdir)
    AC_SUBST(wkfextradir)
    AC_SUBST(wkfcopydir)


    # Define a preprocesor symbol for the plugin search paths

    AC_DEFINE_UNQUOTED(VIMOS_PLUGIN_DIR, "esopipes-plugins",
                       [Plugin directory tree prefix])

    eval plugin_dir="$plugindir"
    plugin_path=`eval echo $plugin_dir | \
                sed -e "s/\/${PACKAGE}-${VERSION}.*$//"`

    AC_DEFINE_UNQUOTED(VIMOS_PLUGIN_PATH, "$plugin_path",
                       [Absolute path to the plugin directory tree])

])


# VIMOS_CREATE_SYMBOLS
#---------------------
# Define include and library related makefile symbols
AC_DEFUN([VIMOS_CREATE_SYMBOLS],
[

    # Symbols for package include file and library search paths

    VIMOS_INCLUDES='-I$(top_srcdir)/vimos'
    VIMOS_LDFLAGS='-L$(top_builddir)/vimos'
    IRPLIB_INCLUDES='-I$(top_srcdir)/irplib'
    MOSCA_INCLUDES='-I$(top_srcdir)/mosca/libmosca'
    CASU_INCLUDES='-I$(top_srcdir)/casu/src -I$(top_srcdir)/casu/src/catalogue'
    CASU_LDFLAGS='-L$(top_srcdir)/casu/src -L$(top_srcdir)/casu/src/catalogue'
    # No -L for IRPLIB which is statically linked

    LIBVIMOSWCS_INCLUDES='-I$(top_srcdir)/external/libwcs'
    LIBVIMOSWCS_LDFLAGS='-L$(top_builddir)/external/libwcs'

    LIBPIL_INCLUDES='-I$(top_srcdir)/libpil/pil -I$(top_srcdir)/libpil/kazlib'
    LIBPIL_LDFLAGS='-L$(top_builddir)/libpil/pil -L$(top_builddir)/libpil/kazlib'

    # Library aliases

    LIBVIMOS='$(top_builddir)/vimos/libvimos.la'
    LIBPIL='$(top_builddir)/libpil/pil/libpil.la'
    LIBVIMOSWCS='$(top_builddir)/external/libwcs/libvimoswcs.la'
    LIBIRPLIB='$(top_builddir)/irplib/libirplib.la'
    LIBMOSCA='$(top_builddir)/mosca/libmosca/libmosca.la'
    LIBCASU='$(top_builddir)/casu/src/libcasu.la'
    LIBCASUCAT='$(top_builddir)/casu/src/catalogue/libcasu_catalogue.la'


    # Substitute the defined symbols

    AC_SUBST(LIBVIMOSWCS_INCLUDES)
    AC_SUBST(LIBVIMOSWCS_LDFLAGS)

    AC_SUBST(LIBPIL_INCLUDES)
    AC_SUBST(LIBPIL_LDFLAGS)

    AC_SUBST(VIMOS_INCLUDES)
    AC_SUBST(VIMOS_LDFLAGS)

    AC_SUBST(LIBVIMOS)
    AC_SUBST(LIBPIL)
    AC_SUBST(LIBVIMOSWCS)

    AC_SUBST(IRPLIB_INCLUDES)
    AC_SUBST(LIBIRPLIB)

    AC_SUBST(MOSCA_INCLUDES)
    AC_SUBST(LIBMOSCA)

    AC_SUBST(CASU_INCLUDES)
    AC_SUBST(CASU_LDFLAGS)
    
    AC_SUBST(LIBCASU)
    AC_SUBST(LIBCASUCAT)

    AC_SUBST(CFITSIO_INCLUDES)
    AC_SUBST(CFITSIO_LDFLAGS)
    AC_SUBST(LIBCFITSIO)



    # Check for CPL and user defined libraries
    AC_REQUIRE([CPL_CHECK_LIBS])
    AC_REQUIRE([ESO_CHECK_EXTRA_LIBS])

    all_includes='$(VIMOS_INCLUDES) $(CASU_INCLUDES) $(MOSCA_INCLUDES) $(LIBPIL_INCLUDES) $(LIBVIMOSWCS_INCLUDES) $(IRPLIB_INCLUDES) $(CPL_INCLUDES) $(CX_INCLUDES) $(CFITSIO_INCLUDES) $(EXTRA_INCLUDES)'
    all_ldflags='$(VIMOS_LDFLAGS) $(CASU_LDFLAGS) $(LIBPIL_LDFLAGS) $(LIBVIMOSWCS_LDFLAGS) $(CPL_LDFLAGS) $(CX_LDFLAGS)  $(CFITSIO_LDFLAGS) $(EXTRA_LDFLAGS)'

    AC_SUBST(all_includes)
    AC_SUBST(all_ldflags)

])


# VIMOS_ENABLE_ONLINE
#--------------------
# Enable the building of extra tools for PSO.
AC_DEFUN([VIMOS_ENABLE_ONLINE],
[

    AH_TEMPLATE([ONLINE_MODE],
                [Define if online support tools should be built])

    AC_ARG_ENABLE(online,
                  AC_HELP_STRING([--enable-online],
                                 [enable online support for PSO [[default=yes]]]),
                  vimos_enable_online=$enableval, vimos_enable_online=no)

    AC_CACHE_CHECK([whether an online support should be enabled],
                   vimos_cv_enable_online,
                   vimos_cv_enable_online=$vimos_enable_online)

    AM_CONDITIONAL([ONLINE_MODE], [test x$vimos_cv_enable_online = xyes])

])
