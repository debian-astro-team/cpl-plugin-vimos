/* $Id: vimos_var.h,v 1.3 2015/08/07 13:07:15 jim Exp $
 *
 * This file is part of the VIMOS Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:07:15 $
 * $Revision: 1.3 $
 * $Name:  $
 */


/* Includes */

#ifndef VIMOS_VAR_H
#define VIMOS_VAR_H

#include <casu_fits.h>
#include <casu_mask.h>

/* Function prototypes */

casu_fits *vimos_var_create(casu_fits *in, casu_mask *mask, 
                            float readnoise, float gain);
void vimos_var_add(casu_fits *in1, casu_fits *in2);
void vimos_var_div_im(casu_fits *in1, casu_fits *in2);
void vimos_var_divk(casu_fits *in, float kscl);
void vimos_var_scaledsubt(casu_fits *in1, casu_fits *in2, float kscl);

#endif

/* 

$Log: vimos_var.h,v $
Revision 1.3  2015/08/07 13:07:15  jim
Fixed copyright to ESO

Revision 1.2  2015/04/30 12:14:29  jim
Added scaled subtract method

Revision 1.1  2015/01/30 12:46:10  jim
new entry


*/

