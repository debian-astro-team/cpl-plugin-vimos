/* $Id: vimos_dfs.h,v 1.15 2015/08/07 13:07:15 jim Exp $
 *
 * This file is part of the VIMOS Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:07:15 $
 * $Revision: 1.15 $
 * $Name:  $
 */


/* Includes */

#ifndef VIMOS_DFS_H
#define VIMOS_DFS_H

#include <cpl.h>

/*********************************************************/
/*       PRO.CATG keywords for main data products        */
/*********************************************************/

/* Products from vimos_ima_bias */

#define VIMOS_PRO_BIAS                "MASTER_BIAS"
#define VIMOS_PRO_DIFFIMG_BIAS        "DIFFIMG_BIAS"
#define VIMOS_PRO_DIFFIMG_BIAS_STATS  "DIFFIMG_STATS_BIAS"

/* Products from vimos_ima_dark */

#define VIMOS_PRO_DARK                "MASTER_DARK"
#define VIMOS_PRO_DIFFIMG_DARK        "DIFFIMG_DARK"
#define VIMOS_PRO_DIFFIMG_DARK_STATS  "DIFFIMG_STATS_DARK"

/* Products from vimos_ima_science */

#define VIMOS_PRO_SIMPLE_SCI          "BASIC_CALIBRATED_SCI"
#define VIMOS_PRO_VAR_SCI             "BASIC_VAR_MAP_SCI"
#define VIMOS_PRO_JITTERED_SCI        "JITTERED_IMAGE_SCI"
#define VIMOS_PRO_CONF_SCI            "CONFIDENCE_MAP_SCI"
#define VIMOS_PRO_JITTERED_VAR        "JITTERED_VAR_IMAGE"
#define VIMOS_PRO_OBJCAT_SCI          "OBJECT_CATALOGUE_SCI"
#define VIMOS_PRO_OBJCAT_SCI_JITTERED "OBJECT_CATALOGUE_SCI_JITTERED"

/* Products from vimos_ima_standard */

#define VIMOS_PRO_SIMPLE_STD          "BASIC_CALIBRATED_STD"
#define VIMOS_PRO_VAR_STD             "BASIC_VAR_MAP_STD"
#define VIMOS_PRO_OBJCAT_STD          "OBJECT_CATALOGUE_STD"

/* Common products from science and standard processing recipes */

#define VIMOS_PRO_MATCHSTD_ASTROM     "MATCHSTD_ASTROM"
#define VIMOS_PRO_MATCHSTD_PHOTOM     "MATCHSTD_PHOTOM"

/* Products from vimos_ima_twilight_flat */

#define VIMOS_PRO_TWILIGHT_FLAT                "MASTER_TWILIGHT_FLAT"
#define VIMOS_PRO_CONF                         "MASTER_CONF"
#define VIMOS_PRO_BPM                          "MASTER_BPM"
#define VIMOS_PRO_RATIOIMG_TWILIGHT_FLAT       "RATIOIMG_TWILIGHT_FLAT"
#define VIMOS_PRO_RATIOIMG_TWILIGHT_FLAT_STATS "RATIOIMG_STATS_TWILIGHT_FLAT"

/* Products from vimos_ima_det_noise */

#define VIMOS_PRO_READGAIN            "MASTER_READGAIN" 

/* Products from vimos_ima_fringe */

#define VIMOS_PRO_FRINGE              "MASTER_FRINGE"
#define VIMOS_PRO_FRINGE_VAR          "MASTER_FRINGE_VAR"

/*********************************************************/
/*         DO.CATG keywords for raw data frames          */
/*********************************************************/

/* Raw input for vimos_ima_bias */

#define VIMOS_BIAS_RAW                "BIAS"

/* Raw input for vimos_ima_dark */

#define VIMOS_DARK_RAW                "DARK"

/* Raw input for vimos_jitter_microstep_process */

#define VIMOS_SCI_OBJECT_RAW          "OBJECT"

/* Raw input for vimos_ima_standard */

#define VIMOS_STD_OBJECT_RAW          "STD"

/* Raw input for vimos_ima_twilight_flat */

#define VIMOS_TWI_RAW                 "FLAT_TWILIGHT"

/*********************************************************/
/*     DO.CATG keywords for reference data frames        */
/*     Used for comparison of new master calibrations    */
/*********************************************************/

#define VIMOS_REF_BIAS                "REFERENCE_BIAS"
#define VIMOS_REF_DARK                "REFERENCE_DARK"
#define VIMOS_REF_TWILIGHT_FLAT       "REFERENCE_TWILIGHT_FLAT"
#define VIMOS_REF_BPM                 "REFERENCE_BPM"
#define VIMOS_REF_CONF                "REFERENCE_CONF"

/*********************************************************/
/*   DO.CATG keywords for master calibration frames      */
/*********************************************************/

#define VIMOS_CAL_BIAS                "MASTER_BIAS"
#define VIMOS_CAL_DARK                "MASTER_DARK"
#define VIMOS_CAL_TWILIGHT_FLAT       "MASTER_TWILIGHT_FLAT"
#define VIMOS_CAL_CONF                "MASTER_CONF"
#define VIMOS_CAL_BPM                 "MASTER_BPM"
#define VIMOS_CAL_2MASS_A             "MASTER_2MASS_CATALOGUE_ASTROM"
#define VIMOS_CAL_PPMXL_A             "MASTER_PPMXL_CATALOGUE_ASTROM"
#define VIMOS_CAL_APASS_A             "MASTER_APASS_CATALOGUE_ASTROM"
#define VIMOS_CAL_LOCCAT_A            "MASTER_LOCAL_CATALOGUE_ASTROM"
#define VIMOS_CAL_APASS_P             "MASTER_APASS_CATALOGUE_PHOTOM"
#define VIMOS_CAL_LOCCAT_P            "MASTER_LOCAL_CATALOGUE_PHOTOM"
#define VIMOS_CAL_SCHL_N              "SCHLEGEL_MAP_NORTH"
#define VIMOS_CAL_SCHL_S              "SCHLEGEL_MAP_SOUTH"
#define VIMOS_CAL_READGAIN            "MASTER_READGAIN"
#define VIMOS_CAL_MSTD_PHOT           "MATCHSTD_PHOTOM"
#define VIMOS_CAL_FRINGE              "MASTER_FRINGE"
#define VIMOS_CAL_FRINGE_VAR          "MASTER_FRINGE_VAR"
#define VIMOS_CAL_STDSTAR             "OBJECT_CATALOGUE_STD"

/*********************************************************/
/*    DO.CATG keywords for static calibration            */
/*********************************************************/

#define VIMOS_CAL_PHOTTAB             "PHOTCAL_TAB"

/* Function prototypes */

CPL_BEGIN_DECLS

void vimos_dfs_set_product_primary_header(cpl_propertylist *plist,
                                          cpl_frame *frame,
                                          cpl_frameset *frameset,
                                          cpl_parameterlist *parlist,
                                          const char *recipeid,
                                          const char *dict,
                                          cpl_frame *inherit,
                                          int synch);
void vimos_dfs_set_product_exten_header(cpl_propertylist *plist,
                                        cpl_frame *frame,
                                        cpl_frameset *frameset,
                                        cpl_parameterlist *parlist,
                                        const char *recipeid,
                                        const char *dict,
                                        cpl_frame *inherit);

char *vmstrlower(char *s);
int dfs_save_image(cpl_frameset *, const cpl_image *, const char *, 
                   cpl_propertylist *, const cpl_parameterlist *, 
                   const char *, const char *);
int dfs_save_table(cpl_frameset *, const cpl_table *, const char *, 
                   cpl_propertylist *, const cpl_parameterlist *, 
                   const char *, const char *);
int dfs_save_table_with_header_ext(cpl_frameset *, const cpl_table *, const char *,
				   cpl_propertylist *, cpl_propertylist *, const cpl_parameterlist *,
                   const char *, const char *);

cpl_image *dfs_load_image(cpl_frameset *, const char *, cpl_type, int, int);
cpl_table *dfs_load_table(cpl_frameset *, const char *, int);
cpl_propertylist *dfs_load_header(const cpl_frameset *, const char *, int);
int dfs_equal_keyword(cpl_frameset *frameset, const char *keyword);
int dfs_get_parameter_bool(cpl_parameterlist *, const char *, 
                           const cpl_table *);
int dfs_get_parameter_int(cpl_parameterlist *, const char *, 
                          const cpl_table *);
double dfs_get_parameter_double(cpl_parameterlist *, const char *, 
                                const cpl_table *);
const char *dfs_get_parameter_string(cpl_parameterlist *, const char *, 
                                     const cpl_table *);
int dfs_get_parameter_bool_const(const cpl_parameterlist *, const char *);
int dfs_get_parameter_int_const(const cpl_parameterlist *, const char *);
double dfs_get_parameter_double_const(const cpl_parameterlist *, const char *);
const char *dfs_get_parameter_string_const(const cpl_parameterlist *, const char *);
char *dfs_generate_filename_tfits(const char *);

cpl_error_code dfs_save_table_ext_name(cpl_table *, const char *,
				  cpl_propertylist *, const char * );
cpl_error_code dfs_save_table_ext(cpl_table *, const char *,
				  cpl_propertylist *);

cpl_error_code dfs_save_image_ext_name(cpl_image *, const char *,
				  cpl_propertylist *, const char *);
cpl_error_code dfs_save_image_ext(cpl_image *, const char *,
				  cpl_propertylist *);
cpl_error_code dfs_save_image_null(cpl_frameset *, 
                                   cpl_propertylist *, cpl_parameterlist *,
				   const char *, const char *,
				   const char *);

int vimos_dfs_set_groups(cpl_frameset * set);

cpl_frameset *
vimos_frameset_extract(const cpl_frameset *frames,
                       const char *tag);
CPL_END_DECLS

#endif   /* VIMOS_DFS_H */
