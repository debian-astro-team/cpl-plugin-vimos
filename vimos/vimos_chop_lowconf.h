/* $Id: vimos_chop_lowconf.c,v 1.8 2015/08/07 13:07:15 jim Exp $
 *
 * This file is part of the VIMOS Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:07:15 $
 * $Revision: 1.8 $
 * $Name:  $
 */

/* Includes */

#ifndef VIMOS_CHOP_LOWCONF_H
#define VIMOS_CHOP_LOWCONF_H


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <casu_fits.h>


int vimos_chop_lowconfbands(casu_fits *in, casu_fits *conf, 
                            int *status);

int vimos_chop_lowconfpix(casu_fits *in, int *status);
         
#endif // VIMOS_CHOP_LOWCONF_H
