/* $Id: vimos_mods.h,v 1.7 2015/09/22 15:10:28 jim Exp $
 *
 * This file is part of the VIMOS Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/09/22 15:10:28 $
 * $Revision: 1.7 $
 * $Name:  $
 */

#ifndef VIMOS_MODS_H
#define VIMOS_MODS_H

#include <cpl.h>

#include <casu_fits.h>

int vimos_biascor(casu_fits *infile, casu_fits *biassrc, int overscan,
                  int trim, int *status);
int vimos_chop_lowconfbands(casu_fits *in, casu_fits *conf, int *status);
int vimos_chop_region(casu_fits *in, int nextn, int *status);
int vimos_chop_lowconfpix(casu_fits *in, int *status);

#endif

/*

$Log: vimos_mods.h,v $
Revision 1.7  2015/09/22 15:10:28  jim
fixed guard

Revision 1.6  2015/08/07 13:07:15  jim
Fixed copyright to ESO

Revision 1.5  2015/01/29 12:07:30  jim
Modified comments

Revision 1.4  2013/11/21 09:39:08  jim
detabbed

Revision 1.3  2013/11/05 05:57:46  jim
redefined chop routines

Revision 1.2  2013/11/04 06:02:08  jim
added vimos_chop_lowconf

Revision 1.1.1.1  2013-10-24 08:36:12  jim
Initial import


*/
