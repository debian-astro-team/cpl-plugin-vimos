/* $Id: vimos_science_impl.h,v 1.2 2010-07-20 11:11:41 cizzo Exp $
 *
 * This file is part of the FORS Library
 * Copyright (C) 2002-2010 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/*
 * $Author: cizzo $
 * $Date: 2010-07-20 11:11:41 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */

#ifndef VIMOS_SCIENCE_IMPL_H
#define VIMOS_SCIENCE_IMPL_H

#include <cpl.h>

#include <vector>
#include "wavelength_calibration.h"
#include "rect_region.h"

CPL_BEGIN_DECLS

int vimos_science_impl(cpl_frameset *, cpl_parameterlist *, int);

void vimos_science_set_object_coord(cpl_table * objects,
								 mosca::wavelength_calibration& wave_cal,
								 cpl_table* polytraces, const cpl_wcs *wcs,
								 const mosca::rect_region& overscan_region,
								 const double undeviated_wlen,
								 const double nx,
								 const cpl_propertylist * science_header);

double get_undeviated_wlen(const cpl_propertylist * science_header);

bool is_posang_present(const cpl_propertylist * plist);

CPL_END_DECLS

#endif
