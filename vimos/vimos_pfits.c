/* $Id: vimos_pfits.c,v 1.7 2015/08/07 13:07:15 jim Exp $
 *
 * This file is part of the VIMOS Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:07:15 $
 * $Revision: 1.7 $
 * $Name:  $
 */

/* Includes */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <cpl.h>
#include <string.h>

#include <casu_utils.h>
#include "vimos_pfits.h"

static int vimos_pfits_get_float(cpl_propertylist *plist, 
                                 const char *key, float *fval);
static int vimos_pfits_get_double(cpl_propertylist *plist, 
                                  const char *key, double *fval);

/**
   \defgroup vimos_pfits vimos_pfits
   \ingroup supportroutines

   \brief
   These are support routines used for extracting information from FITS 
   headers.

   \author Jim Lewis, CASU
*/

/**@{*/

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of exposure time
    \param    plist       property list to read from
    \param    exptime     requested value of exptime
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

int vimos_pfits_get_exptime(cpl_propertylist *plist, float *exptime) {

     return(vimos_pfits_get_float(plist,"EXPTIME",exptime));

}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of local sidereal time
    \param    plist       property list to read from
    \param    lst         requested value of LST
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

int vimos_pfits_get_lst(cpl_propertylist *plist, double *lst) {

     return(vimos_pfits_get_double(plist,"LST",lst));

}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of MJD
    \param    plist       property list to read from
    \param    mjd         requested value of exptime
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

int vimos_pfits_get_mjd(cpl_propertylist *plist, double *mjd) {

     return(vimos_pfits_get_double(plist,"MJD-OBS",mjd));

}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of ESO TPL START
    \param    plist       property list to read from
    \param    tpl_start   requested value of ESO TPL START
    \retval CASU_OK if everything went right
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/
int vimos_pfits_get_tpl_start(cpl_propertylist *plist, char const ** tpl_start) {

	const char * tag = "ESO TPL START";

	*tpl_start = NULL;

	if(!cpl_propertylist_has(plist, tag)) return CASU_FATAL;
	if(cpl_propertylist_get_type(plist, tag) != CPL_TYPE_STRING) return CASU_FATAL;

	*tpl_start = cpl_propertylist_get_string(plist, tag);

	return CASU_OK;
}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of gain
    \param    plist       property list to read from
    \param    gain        requested value of gain
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

int vimos_pfits_get_gain(cpl_propertylist *plist, float *gain) {

    return(vimos_pfits_get_float(plist,"GAIN",gain));

}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of the detector name
    \param    plist       property list to read from
    \param    chipname    the name of the detector
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

int vimos_pfits_get_chipname(cpl_propertylist *plist, char *chipname) {
    strcpy(chipname,cpl_propertylist_get_string(plist,"ESO DET CHIP1 ID"));
    if (cpl_error_get_code() == CPL_ERROR_NONE) {
        return(CASU_OK);
    } else {
        cpl_error_reset();
        return(CASU_FATAL);
    }
}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of the filter name
    \param    plist       property list to read from
    \param    chipname    the name of the detector
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

int vimos_pfits_get_filter(cpl_propertylist *plist, char *filter) {
    int i,gotit;
    char key[64];
    gotit = 0;
    for (i = 1; i <= 9; i++) {
        (void)snprintf(key,64,"ESO INS FILT%d NAME",i);
        if (cpl_propertylist_has(plist,key)) {
            gotit = 1;
            break;
        }
    }
    if (! gotit) 
        return(CASU_FATAL);
    strcpy(filter,cpl_propertylist_get_string(plist,key));
    if (cpl_error_get_code() == CPL_ERROR_NONE) {
        return(CASU_OK);
    } else {
        cpl_error_reset();
        return(CASU_FATAL);
    }
}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of DATE-OBS
    \param    plist       property list to read from
    \param    dateobs     the template start time
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

int vimos_pfits_get_dateobs(cpl_propertylist *plist, char *dateobs) {
    if (cpl_propertylist_has(plist,"DATE-OBS") == 0)
        return(CASU_FATAL);
    strcpy(dateobs,cpl_propertylist_get_string(plist,"DATE-OBS"));
    if (cpl_error_get_code() == CPL_ERROR_NONE) {
        return(CASU_OK);
    } else {
        cpl_error_reset();
        return(CASU_FATAL);
    }
}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of project id
    \param    plist       property list to read from
    \param    projid      the project id
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

int vimos_pfits_get_projid(cpl_propertylist *plist, char *projid) {
    if (cpl_propertylist_has(plist,"ESO OBS PROG ID") == 0)
        return(CASU_FATAL);
    strcpy(projid,cpl_propertylist_get_string(plist,"ESO OBS PROG ID"));
    if (cpl_error_get_code() == CPL_ERROR_NONE) {
        return(CASU_OK);
    } else {
        cpl_error_reset();
        return(CASU_FATAL);
    }
}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get a floating point value
    \param    plist       property list to read from
    \param    key         the keyword for the property
    \param    fval        the output floating point value
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

static int vimos_pfits_get_float(cpl_propertylist *plist, const char *key, 
                                 float *fval) {
    cpl_type type;
    const char *fctid = "vimos_pfits_get_float";

    /* Get the type of this keyword */

    type = cpl_propertylist_get_type(plist,key);
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
        *fval = 0.0;
        cpl_error_reset();
        return(CASU_FATAL);
    }

    /* Switch of the property type */

    switch (type) {
    case CPL_TYPE_FLOAT:
        *fval = cpl_propertylist_get_float(plist,key);
        break;
    case CPL_TYPE_DOUBLE:
        *fval = (float)cpl_propertylist_get_double(plist,key);
        break;
    default:
        *fval = 0.0;
        cpl_msg_error(fctid,"Keyword %s is not floating point in header",key);
        return(CASU_FATAL);
    }
    return(CASU_OK);
}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get a double precision value
    \param    plist       property list to read from
    \param    key         the keyword for the property
    \param    fval        the output double precision value
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

static int vimos_pfits_get_double(cpl_propertylist *plist, const char *key, 
                                  double *fval) {
    cpl_type type;
    const char *fctid = "vimos_pfits_get_float";

    /* Get the type of this keyword */

    type = cpl_propertylist_get_type(plist,key);
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
        *fval = 0.0;
        cpl_error_reset();
        return(CASU_FATAL);
    }

    /* Switch of the property type */

    switch (type) {
    case CPL_TYPE_FLOAT:
        *fval = (double)cpl_propertylist_get_float(plist,key);
        break;
    case CPL_TYPE_DOUBLE:
        *fval = cpl_propertylist_get_double(plist,key);
        break;
    default:
        *fval = 0.0;
        cpl_msg_error(fctid,"Keyword %s is not floating point in header",key);
        return(CASU_FATAL);
    }
    return(CASU_OK);
}


/**@}*/

/*

$Log: vimos_pfits.c,v $
Revision 1.7  2015/08/07 13:07:15  jim
Fixed copyright to ESO

Revision 1.6  2015/06/25 11:54:07  jim
Fixed includes to casu

Revision 1.5  2015/01/29 12:07:30  jim
Modified comments

Revision 1.4  2014/12/11 12:27:28  jim
new version

Revision 1.3  2013/11/21 09:39:08  jim
detabbed

Revision 1.2  2013/11/04 06:03:06  jim
cosmetic changes

Revision 1.1.1.1  2013-10-24 08:36:12  jim
Initial import


*/

