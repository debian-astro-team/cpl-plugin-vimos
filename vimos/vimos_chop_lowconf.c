/* $Id: vimos_chop_lowconf.c,v 1.8 2015/08/07 13:07:15 jim Exp $
 *
 * This file is part of the VIMOS Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:07:15 $
 * $Revision: 1.8 $
 * $Name:  $
 */

/* Includes */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <cpl.h>
#include "vimos_imaging_utils.h"
#include "vimos_mods.h"
#include <casu_fits.h>
#include <casu_utils.h>
#include <casu_stats.h>

/**@{*/

/*---------------------------------------------------------------------------*/
/**
    \ingroup reductionmodules
    \brief Chop bands with bad confidence from a frame
  
    \par Name:
        vimos_chop_lowconfbands
    \par Purpose:
        Remove blocks of rows with bad confidence
    \par Description:
        There are regions at the bottom and top of vimos images where 
        the confidence is low and which don't correct well. This routine
        removes those regions from an input image and corrects the WCS
        to take the new image size into account.
    \par Language:
        C
    \param in
        The input data image (overwritten by result).
    \param conf
        The input confidence map
    \param status 
        An input/output status that is the same as the returned values below.
    \retval CASU_OK 
        if everything is ok
    \retval CASU_FATAL 
        if image data fails to load
    \par DRS headers:
        The following DRS keywords are written to the infile extension header
        - \b CHOPMIN
            The first row included from the original image
        - \b CHOPMAX: 
            The last row included from the original image
        - \b CHOPCOR
            A flag to say that chopping has been done
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

int vimos_chop_lowconfbands(casu_fits *in, casu_fits *conf, 
                            int *status) {
    int imin,imax,nx,ny,i,*cdata;
    float crpix;
    cpl_propertylist *plist;
    cpl_image *im1,*im2;

    /* Inherited status */

    if (*status != CASU_OK)
        return(*status);

     /* Check that this hasn't aready been done... */

    plist = casu_fits_get_ehu(in);
    if (cpl_propertylist_has(plist,"ESO DRS CHOPCOR")) 
        return(*status);

    /* See if the information on which bits to cut are already in the 
       header of the confidence map */

    plist = casu_fits_get_ehu(conf);
    if (cpl_propertylist_has(plist,"ESO DRS CHOPMIN") && 
        cpl_propertylist_has(plist,"ESO DRS CHOPMAX")) {
        imin = cpl_propertylist_get_int(plist,"ESO DRS CHOPMIN");
        imax = cpl_propertylist_get_int(plist,"ESO DRS CHOPMAX");

    /* If not, then work out where the regions should be cut */

    } else {
        im1 = casu_fits_get_image(conf);
        im2 = cpl_image_collapse_median_create(im1,1,0,0);
        cdata = cpl_image_get_data_int(im2);
        ny = (int)cpl_image_get_size_y(im2);
        for (i = 1; i <= ny; i++) {
            if (cdata[i-1] > 80) {
                imin = i;
                break;
            }
        }
        for (i = ny; i >= 1; i--) {
            if (cdata[i-1] > 80) {
                imax = i;
                break;
            }
        }
        cpl_image_delete(im2);
        plist = casu_fits_get_ehu(conf);
        cpl_propertylist_append_int(plist,"ESO DRS CHOPMIN",imin);
        cpl_propertylist_set_comment(plist,"ESO DRS CHOPMIN",
                                     "First row included in subset");
        cpl_propertylist_append_int(plist,"ESO DRS CHOPMAX",imax);
        cpl_propertylist_set_comment(plist,"ESO DRS CHOPMAX",
                                     "Last row included in subset");
    }

    /* OK, subset the input image */

    im1 = casu_fits_get_image(in);
    nx = cpl_image_get_size_x(im1);
    im2 = cpl_image_extract(im1,1,(cpl_size)imin,(cpl_size)nx,(cpl_size)imax);
    casu_fits_replace_image(in,im2);

    /* Flag that we've done this... */

    plist = casu_fits_get_ehu(in);
    cpl_propertylist_append_int(plist,"ESO DRS CHOPMIN",imin);
    cpl_propertylist_set_comment(plist,"ESO DRS CHOPMIN",
                                 "First row included in subset");
    cpl_propertylist_append_int(plist,"ESO DRS CHOPMAX",imax);
    cpl_propertylist_set_comment(plist,"ESO DRS CHOPMAX",
                                 "Last row included in subset");
    cpl_propertylist_append_bool(plist,"ESO DRS CHOPCOR",1);
    cpl_propertylist_set_comment(plist,"ESO DRS CHOPCOR",
                                 "Regions of low confidence have been chopped");
    
    /* Modify the WCS if we can */

    if (cpl_propertylist_has(plist,"CRPIX2")) {
        if (cpl_propertylist_get_type(plist,"CRPIX2") == CPL_TYPE_FLOAT) {
            crpix = cpl_propertylist_get_float(plist,"CRPIX2");
            crpix -= (float)(imin - 1);
            cpl_propertylist_set_float(plist,"CRPIX2",crpix);
        } else {
            crpix = (float)cpl_propertylist_get_double(plist,"CRPIX2");
            crpix -= (float)(imin - 1);
            cpl_propertylist_set_double(plist,"CRPIX2",(double)crpix);
        }
    }
    return(CASU_OK);
}

/*---------------------------------------------------------------------------*/
/**
    \ingroup reductionmodules
    \brief Zero out all pixels in a confidence map with low confidence
  
    \par Name:
        vimos_chop_lowconf
    \par Purpose:
        Zero the confidence in a map where the input confidence is lowish
    \par Description:
        Zero the confidence in a map where the input confidence is on the
        low side. 
    \par Language:
        C
    \param in
        The input confidence map image (overwritten by result).
    \param status 
        An input/output status that is the same as the returned values below.
    \retval CASU_OK 
        if everything is ok
    \retval CASU_FATAL 
        if image data fails to load
    \par DRS headers:
        The following DRS keywords are written to the infile extension header
        - \b CHOPNUM
            The number of pixels that are zeroed (but weren't zeroed to
            start with)
        - \b CHOPCOR
            A flag to say that chopping has been done
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

int vimos_chop_lowconfpix(casu_fits *in, int *status) {
    int i,nx,ny,npts,nbad,*cdata;
    cpl_propertylist *plist;
    
    /* Inherited status */

    if (*status != CASU_OK)
        return(*status);

     /* Check that this hasn't aready been done... */

    plist = casu_fits_get_ehu(in);
    if (cpl_propertylist_has(plist,"ESO DRS CHOPCOR")) 
        return(*status);

    /* Loop through the data array */

    cdata = cpl_image_get_data_int(casu_fits_get_image(in));
    nx = (int)cpl_image_get_size_x(casu_fits_get_image(in));
    ny = (int)cpl_image_get_size_y(casu_fits_get_image(in));
    npts = nx*ny;
    nbad = 0;
    for (i = 0; i < npts; i++) {
        if (cdata[i] != 0 && cdata[i] < 80) {
            cdata[i] = 0;
            nbad++;
        }
    }

    /* Add some stuff to the header */

    cpl_propertylist_append_int(plist,"ESO DRS CHOPNUM",nbad);
    cpl_propertylist_set_comment(plist,"ESO DRS CHOPNUM",
                                 "Number of pixels re-flagged");
    cpl_propertylist_append_bool(plist,"ESO DRS CHOPCOR",1);
    cpl_propertylist_set_comment(plist,"ESO DRS CHOPCOR",
                                 "Regions of low confidence have been chopped");
    return(CASU_OK);
}

/*

$Log: vimos_chop_lowconf.c,v $
Revision 1.8  2015/08/07 13:07:15  jim
Fixed copyright to ESO

Revision 1.7  2015/06/25 11:54:07  jim
Fixed includes to casu

Revision 1.6  2015/01/29 12:07:30  jim
Modified comments

Revision 1.5  2014/12/11 12:27:28  jim
new version

Revision 1.4  2014/05/06 07:36:50  jim
Now uses floating point confidence

Revision 1.3  2013/11/21 09:39:08  jim
detabbed

Revision 1.2  2013/11/05 05:58:17  jim
added vimos_chop_lowconfpix

Revision 1.1  2013/11/04 06:00:45  jim
New entry


*/

         
