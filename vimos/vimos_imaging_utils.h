/* $Id: vimos_utils.h,v 1.15 2015/11/27 12:15:33 jim Exp $
 *
 * This file is part of the VIMOS Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/11/27 12:15:33 $
 * $Revision: 1.15 $
 * $Name:  $
 */


/* Includes */

#ifndef VIMOS_IMAGING_UTILS_H
#define VIMOS_IMAGING_UTILS_H

#include <casu_fits.h>
#include <casu_tfits.h>

#define SZ_ALLDESC 4096
#define VIMOS_NEXTN 4

/* Detector names and the order that we will store them in MEFs */

static const char *vimos_newnames[VIMOS_NEXTN] = {"BRIAN","Keith","Tom",
                                                  "DAVID"};
static const char *vimos_oldnames[VIMOS_NEXTN] = {"CCD-59B","CCD-59A","CCD-60A",
                                                  "CCD-60B"};

/* underscan and overscan regions (xmin,xmax,ymin,ymax) */

static int underlim[4] = {1,50,1,2440};
static int overlim[4] = {2099,2148,1,2440};

/* trim regions (xmin,xmax,ymin,ymax) for each detector in turn */

static int trimreg_new[4][4] = {{1,2048,190,2375},
                                {1,2026,200,2438},
                                {1,2048,75,2300},
                                {1,2048,30,2234}};
static int trimreg_old[4][4] = {{76,2048,100,2390},
                                {1,1950,59,2370},
                                {1,2048,55,2300},
                                {51,2048,50,2260}};

/* Group of 4 structure */

typedef struct {
    cpl_frame         *inf[4];
    char              name[32];
} groupof4;

/* Function prototypes */

cpl_table *vimos_create_diffimg_stats(int nrows);
void vimos_difference_image(cpl_image *master, cpl_image *prog, 
                            unsigned char *bpm, int ncells, int oper, 
                            float *global_diff, float *global_rms, 
                            cpl_image **diffim, 
                            cpl_table **diffimstats);
cpl_frameset *vimos_get_det_frameset(cpl_frameset *in, cpl_size *labels, 
                                            cpl_size nlab, char *testname);
cpl_frameset *vimos_get_obs_frameset(cpl_frameset *in, cpl_size *labels, 
                                     cpl_size nlab, cpl_size ilab,
                                     char *vimos_names[]);
int vimos_compare_names(const cpl_frame *fr1, const cpl_frame *fr2);
int vimos_compare_mjds(const cpl_frame *fr1, const cpl_frame *fr2);
int vimos_compare_lsts(const cpl_frame *fr1, const cpl_frame *fr2);
int vimos_compare_tpl_start(const cpl_frame *fr1, const cpl_frame *fr2);
int vimos_load_names(cpl_frame *frm, char *vimos_names[]);
int vimos_load_trimreg(cpl_propertylist *p, int *which);
void vimos_wcsfit(casu_fits **in, casu_tfits **incat, int nf,
                  char *catname, char *catpath, int cdssearch,
                  char *cacheloc, int keepme, int trim, 
                  casu_tfits **mstds);
int vimos_testfrms(cpl_frameset *frms, int nextn_expected, int isimg, 
                          int checkwcs);
int vimos_testfrm_1(cpl_frame *fr, int nextn_expected, int isimg,
                           int checkwcs);
int vimos_testrdgn(cpl_frame *fr, cpl_frame *readgain);
void vimos_getrdgn(cpl_frame *readgain, char *extname, float *readnoise,
                          float *gain);
void vimos_get_groupsof4(cpl_frameset *fset, char *vimos_names[],
                                groupof4 **gr, int *ng);
void vimos_free_groupsof4(int *ng, groupof4 **gr);
void vimos_copywcs(cpl_propertylist *p1, cpl_propertylist *p2);

#endif

/* 

$Log: vimos_utils.h,v $
Revision 1.15  2015/11/27 12:15:33  jim
detabbed some lines

Revision 1.14  2015/09/15 10:39:05  jim
added vimos_wcscopy

Revision 1.13  2015/08/07 13:07:15  jim
Fixed copyright to ESO

Revision 1.12  2015/05/01 09:02:22  jim
Modified to pass detector name information differently so that Mac compiler
doesn't gripe

Revision 1.11  2015/03/13 10:22:59  jim
Fixed some compiler warnings

Revision 1.10  2015/02/14 12:37:22  jim
Expanded and modified vimos_wcsfit

Revision 1.9  2015/01/29 12:07:20  jim
Removed some support routines from recipes to here. Modified comments

Revision 1.8  2014/12/11 12:27:28  jim
new version

Revision 1.7  2013/11/21 12:49:18  jim
Fixed to have separate trim regions for old and new chipsets

Revision 1.6  2013/11/21 09:39:08  jim
detabbed

Revision 1.5  2013/11/05 05:55:46  jim
added trimreg global

Revision 1.4  2013/10/25 15:20:38  jim
Fixed static declarations

Revision 1.3  2013/10/25 09:32:29  jim
fixed some docs

Revision 1.2  2013-10-24 09:16:28  jim
Changes made to remove compiler warnings

Revision 1.1.1.1  2013-10-24 08:36:12  jim
Initial import


*/

