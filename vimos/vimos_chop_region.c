/* $Id: vimos_chop_region.c,v 1.7 2015/08/07 13:07:15 jim Exp $
 *
 * This file is part of the VIMOS Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:07:15 $
 * $Revision: 1.7 $
 * $Name:  $
 */

/* Includes */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <cpl.h>
#include "vimos_imaging_utils.h"
#include "vimos_mods.h"
#include <casu_fits.h>
#include <casu_utils.h>
#include <casu_stats.h>

/**@{*/

/*---------------------------------------------------------------------------*/
/**
    \ingroup reductionmodules
    \brief Chop a previously defined region from an image
  
    \par Name:
        vimos_chop_region
    \par Purpose:
        Chop a previously defined region from an image
    \par Description:
        Regions of bad confidence and poor flat fielding have been
        defined and hardcoded for each detector. This routine removes
        these regions from an image.
    \par Language:
        C
    \param in
        The input data image (overwritten by result).
    \param nextn
        The extension number for the current image
    \param status 
        An input/output status that is the same as the returned values below.
    \retval CASU_OK 
        if everything is ok
    \retval CASU_FATAL 
        if image data fails to load
    \par DRS headers:
        The following DRS keywords are written to the infile extension header
        - \b CHOPREG
            The image section that's been included
        - \b CHOPCOR
            A flag to say that chopping has been done
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

int vimos_chop_region(casu_fits *in, int nextn, int *status) {
    int imin,imax,jmin,jmax,oldnew;
    float crpix;
    cpl_propertylist *plist;
    cpl_image *im1,*im2;
    char reg[64];

    /* Inherited status */

    if (*status != CASU_OK)
        return(*status);

     /* Check that this hasn't aready been done... */

    plist = casu_fits_get_ehu(in);
    if (cpl_propertylist_has(plist,"ESO DRS CHOPCOR")) 
        return(*status);

    /* Get the right trim region */

    (void)vimos_load_trimreg(plist,&oldnew);
    if (oldnew == 1) {
        imin = trimreg_old[nextn-1][0];
        imax = trimreg_old[nextn-1][1];
        jmin = trimreg_old[nextn-1][2];
        jmax = trimreg_old[nextn-1][3];
    } else {
        imin = trimreg_new[nextn-1][0];
        imax = trimreg_new[nextn-1][1];
        jmin = trimreg_new[nextn-1][2];
        jmax = trimreg_new[nextn-1][3];
    }

    /* OK, subset the input image */

    im1 = casu_fits_get_image(in);
    im2 = cpl_image_extract(im1,(cpl_size)imin,(cpl_size)jmin,(cpl_size)imax,
                            (cpl_size)jmax);
    casu_fits_replace_image(in,im2);
    (void)sprintf(reg,"[%d:%d,%d:%d]",imin,imax,jmin,jmax);

    /* Flag that we've done this... */

    cpl_propertylist_append_string(plist,"ESO DRS CHOPREG",reg);
    cpl_propertylist_set_comment(plist,"ESO DRS CHOPREG",
                                 "Region kept from original image");
    cpl_propertylist_append_bool(plist,"ESO DRS CHOPCOR",1);
    cpl_propertylist_set_comment(plist,"ESO DRS CHOPCOR",
                                 "Regions have been chopped");
    
    /* Modify the WCS if we can */


    if (cpl_propertylist_has(plist,"CRPIX2")) {
        if (cpl_propertylist_get_type(plist,"CRPIX2") == CPL_TYPE_FLOAT) {
            crpix = cpl_propertylist_get_float(plist,"CRPIX2");
            crpix -= (float)(jmin - 1);
            cpl_propertylist_set_float(plist,"CRPIX2",crpix);
        } else {
            crpix = (float)cpl_propertylist_get_double(plist,"CRPIX2");
            crpix -= (float)(jmin - 1);
            cpl_propertylist_set_double(plist,"CRPIX2",(double)crpix);
        }
    }
    return(CASU_OK);
}

/*

$Log: vimos_chop_region.c,v $
Revision 1.7  2015/08/07 13:07:15  jim
Fixed copyright to ESO

Revision 1.6  2015/06/25 11:54:07  jim
Fixed includes to casu

Revision 1.5  2015/05/01 09:02:22  jim
Modified to pass detector name information differently so that Mac compiler
doesn't gripe

Revision 1.4  2015/01/29 12:07:30  jim
Modified comments

Revision 1.3  2013/11/21 12:49:18  jim
Fixed to have separate trim regions for old and new chipsets

Revision 1.2  2013/11/21 09:39:08  jim
detabbed

Revision 1.1  2013/11/05 05:54:46  jim
New entry


*/

         
