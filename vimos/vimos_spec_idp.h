#ifndef VIMOS_SPEC_IDP_H
#define VIMOS_SPEC_IDP_H

#include <cpl.h>

cpl_error_code vimos_spec_idp_save(cpl_parameterlist * parameter, cpl_frameset * frames);

double vimos_spec_idp_get_sky_align_shift(const cpl_table * offsets,
		const cpl_table * slits);

const char * get_grism_name_tag(const cpl_propertylist * science_plist);

/*Tags used by vimos_spec_idp_save as inputs i.e. somebody has to write them*/
static const char * idp_input_screen_flat_nwave = "ESO DRS NWAVE";
static const char * idp_input_screen_flat_resolution = "ESO DRS RESOLUTION";
static const char * idp_input_screen_flat_accuracy = "ESO DRS ACCURACY";
static const char * idp_input_screen_skyaling_shift = "ESO DRS SKYALIGN SHIFT";

/* Extra tags that vimos_spec_idp_save generates because CSYER & CRDER are not usable
 * for tables and the corresponding tags for tables cannot be used because the columns
 * are not in the right units*/
static const char * idp_output_csyer_tag = "ESO DRS CSYER";
static const char * idp_output_crder_tag = "ESO DRS CRDER";

#endif
