## Process this file with automake to produce Makefile.in

##   This file is part of the ESO Common Pipeline Library
##   Copyright (C) 2015 European Southern Observatory

##   This program is free software; you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation; either version 2 of the License, or
##   (at your option) any later version.

##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.

##   You should have received a copy of the GNU General Public License
##   along with this program; if not, write to the Free Software
##   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


AUTOMAKE_OPTIONS = 1.8 foreign

DISTCLEANFILES = *~


if MAINTAINER_MODE

MAINTAINERCLEANFILES = $(srcdir)/Makefile.in

endif

AM_CPPFLAGS = $(all_includes) -DCX_LOG_DOMAIN=\"VimosLib\"
AM_LDFLAGS = $(CPL_LDFLAGS)


check_PROGRAMS = vimos_biascor-test vimos_chop_lowconf-test vimos_chop_region-test \
	vimos_utils-test vimos_var-test


vimos_biascor_test_SOURCES = vimos_biascor-test.c
vimos_biascor_test_LDFLAGS = $(CPL_LDFLAGS)
vimos_biascor_test_LDADD = $(LIBMOSCA) $(LIBCPLCORE) $(LIBVIMOS) $(LIBCASU) $(LIBCASUCAT)

vimos_chop_lowconf_test_SOURCES = vimos_chop_lowconf-test.c
vimos_chop_lowconf_test_LDFLAGS = $(CPL_LDFLAGS)
vimos_chop_lowconf_test_LDADD = $(LIBMOSCA) $(LIBCPLCORE) $(LIBVIMOS) $(LIBCASU) $(LIBCASUCAT)

vimos_chop_region_test_SOURCES = vimos_chop_region-test.c
vimos_chop_region_test_LDFLAGS = $(CPL_LDFLAGS)
vimos_chop_region_test_LDADD = $(LIBMOSCA) $(LIBCPLCORE) $(LIBVIMOS) $(LIBCASU) $(LIBCASUCAT)

vimos_utils_test_SOURCES = vimos_utils-test.c
vimos_utils_test_LDFLAGS = $(CPL_LDFLAGS)
vimos_utils_test_LDADD = $(LIBMOSCA) $(LIBCPLCORE) $(LIBVIMOS) $(LIBCASU) $(LIBCASUCAT)

vimos_var_test_SOURCES = vimos_var-test.c
vimos_var_test_LDFLAGS = $(CPL_LDFLAGS)
vimos_var_test_LDADD = $(LIBMOSCA) $(LIBCPLCORE) $(LIBVIMOS) $(LIBCASU) $(LIBCASUCAT)

TESTS = vimos_biascor-test vimos_chop_lowconf-test vimos_chop_region-test \
	vimos_utils-test vimos_var-test

# Be sure to reexport important environment variables.
TESTS_ENVIRONMENT = MAKE="$(MAKE)" CC="$(CC)" CFLAGS="$(CFLAGS)" \
	CPPFLAGS="$(CPPFLAGS)" LD="$(LD)" LDFLAGS="$(LDFLAGS)" \
	LIBS="$(LIBS)" LN_S="$(LN_S)" NM="$(NM)" RANLIB="$(RANLIB)" \
	OBJEXT="$(OBJEXT)" EXEEXT="$(EXEEXT)"

XFAIL_TESTS =

# We need to remove any files that the above tests created.
clean-local:
	$(RM) *.fits *.log

#if USE_PURIFY
#include $(top_builddir)/Makefile.purify
#endif
