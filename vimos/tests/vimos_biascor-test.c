/* $Id: vimos_biascor-test.c,v 1.1 2015/10/16 12:34:11 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/10/16 12:34:11 $
 * $Revision: 1.1 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_utils.h>
#include <casu_mods.h>
#include <vimos_biascor.h>
#include <vimos_imaging_utils.h>
#include <vimos_mods.h>

int main(void) {
    cpl_image *im1,*im2;
    casu_fits *sci,*bias;
    cpl_propertylist *ehu;
    int status,retval;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Create a couple of images */

    im1 = cpl_image_new(2148,2440,CPL_TYPE_FLOAT);
    im2 = cpl_image_new(2148,2440,CPL_TYPE_FLOAT);
    cpl_image_fill_noise_uniform(im1,999.0,1001.0);
    cpl_image_fill_window(im1,underlim[1]+1,underlim[2],overlim[0]-1,
                          underlim[3],10000);
    cpl_image_fill_noise_uniform(im2,998.5,999.5);
    sci = casu_fits_wrap(im1,NULL,NULL,NULL);
    ehu = cpl_propertylist_new();
    cpl_propertylist_update_float(ehu,"ESO QC OSCAN_MED",999.0);
    bias = casu_fits_wrap(im2,NULL,NULL,ehu);
    cpl_propertylist_delete(ehu);

    /* Now test vimos_biascor */

    status = CASU_FATAL;
    retval = vimos_biascor(sci,bias,1,1,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(retval,CASU_FATAL);
    status = CASU_OK;
    retval = vimos_biascor(sci,bias,1,1,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(retval,CASU_OK);
    cpl_test_eq(cpl_image_get_size_x(casu_fits_get_image(sci)),2048);
    cpl_test_eq(cpl_image_get_size_y(casu_fits_get_image(sci)),2440);
    cpl_test_rel(cpl_image_get_median(casu_fits_get_image(sci)),9000.0,0.01);
    cpl_test_rel(cpl_propertylist_get_float(casu_fits_get_ehu(sci),
                                            "ESO DRS OSCOR"),1.0,0.01);
    cpl_test_rel(cpl_propertylist_get_float(casu_fits_get_ehu(sci),
                                            "ESO DRS OSCAN_MED"),1000.0,0.01);

    /* Get out of here */

    casu_fits_delete(sci);
    casu_fits_delete(bias);
    return(cpl_test_end(0));

}

/*

$Log: vimos_biascor-test.c,v $
Revision 1.1  2015/10/16 12:34:11  jim
new



*/
