/* $Id: vimos_utils-test.c,v 1.1 2015/10/16 12:34:11 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/10/16 12:34:11 $
 * $Revision: 1.1 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_utils.h>
#include <casu_mods.h>
#include <vimos_imaging_utils.h>
#include <vimos_mods.h>

int main(void) {
    cpl_propertylist *p,*p2;
    int retval,which;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Test vimos_load_trimreg */
    
    p = cpl_propertylist_new();
    cpl_propertylist_update_string(p,"ESO DET CHIP1 ID","DAVID");
    retval = vimos_load_trimreg(p,&which);
    cpl_test_eq(retval,CASU_OK);
    cpl_test_eq(which,2);

    /* Test vimos_copywcs */

    p2 = cpl_propertylist_new();
    cpl_propertylist_update_string(p,"CTYPE1","RA---TAN");
    cpl_propertylist_update_double(p,"CD1_1",0.5);
    cpl_propertylist_update_double(p,"CRVAL2",-12.0);
    vimos_copywcs(p,p2);
    cpl_test_eq_string("RA---TAN",cpl_propertylist_get_string(p2,"CTYPE1"));
    cpl_test_rel(0.5,cpl_propertylist_get_double(p2,"CD1_1"),0.01);
    cpl_test_rel(-12.0,cpl_propertylist_get_double(p2,"CRVAL2"),0.01);

    /* Get out of here */

    cpl_propertylist_delete(p);
    cpl_propertylist_delete(p2);
    return(cpl_test_end(0));

}

/*

$Log: vimos_utils-test.c,v $
Revision 1.1  2015/10/16 12:34:11  jim
new



*/
