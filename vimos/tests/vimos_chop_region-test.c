/* $Id: vimos_chop_region-test.c,v 1.1 2015/10/16 12:34:11 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/10/16 12:34:11 $
 * $Revision: 1.1 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_utils.h>
#include <casu_mods.h>
#include <vimos_chop_region.h>
#include <vimos_imaging_utils.h>
#include <vimos_mods.h>

int main(void) {
    cpl_image *im1;
    casu_fits *sci;
    int status,retval,ix,iy;
    cpl_propertylist *p;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Create an image */

    im1 = cpl_image_new(2048,2440,CPL_TYPE_FLOAT);
    p = cpl_propertylist_new();
    cpl_propertylist_update_string(p,"ESO DET CHIP1 ID","BRIAN");
    sci = casu_fits_wrap(im1,NULL,NULL,p);
    cpl_propertylist_delete(p);

    /* That the correct bit has been chopped off */

    status = CASU_FATAL;
    retval = vimos_chop_region(sci,1,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(retval,CASU_FATAL);
    status = CASU_OK;
    retval = vimos_chop_region(sci,1,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(retval,CASU_OK);
    ix = trimreg_new[0][1] - trimreg_new[0][0] + 1;
    iy = trimreg_new[0][3] - trimreg_new[0][2] + 1;
    cpl_test_eq(cpl_image_get_size_x(casu_fits_get_image(sci)),ix);
    cpl_test_eq(cpl_image_get_size_y(casu_fits_get_image(sci)),iy);
    cpl_test_eq(cpl_propertylist_get_bool(casu_fits_get_ehu(sci),
                                          "ESO DRS CHOPCOR"),1);

    /* Get out of here */

    casu_fits_delete(sci);
    return(cpl_test_end(0));

}

/*

$Log: vimos_chop_region-test.c,v $
Revision 1.1  2015/10/16 12:34:11  jim
new



*/
