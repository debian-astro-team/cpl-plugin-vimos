/* $Id: vimos_chop_lowconf-test.c,v 1.1 2015/10/16 12:34:11 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/10/16 12:34:11 $
 * $Revision: 1.1 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_utils.h>
#include <casu_mods.h>
#include <vimos_chop_lowconf.h>
#include <vimos_imaging_utils.h>
#include <vimos_mods.h>

int main(void) {
    cpl_image *im1,*im2;
    casu_fits *sci,*conf;
    int status,retval,nbad;
    double val;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Create a couple of images */

    im1 = cpl_image_new(2048,2440,CPL_TYPE_FLOAT);
    im2 = cpl_image_new(2048,2440,CPL_TYPE_INT);
    cpl_image_fill_noise_uniform(im2,98,102);
    cpl_image_fill_window(im2,1,2401,2048,2440,50);
    sci = casu_fits_wrap(im1,NULL,NULL,NULL);
    conf = casu_fits_wrap(im2,NULL,NULL,NULL);

    /* Check that the top band is zeroed */

    status = CASU_FATAL;
    retval = vimos_chop_lowconfpix(conf,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(retval,CASU_FATAL);
    status = CASU_OK;
    retval = vimos_chop_lowconfpix(conf,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(retval,CASU_OK);
    val = cpl_image_get_median_window(casu_fits_get_image(conf),1,2401,2048,
                                      2440);
    cpl_test_rel(val,0.0,0.01);
    nbad = 40*2048;
    cpl_test_eq(nbad,cpl_propertylist_get_int(casu_fits_get_ehu(conf),
                                              "ESO DRS CHOPNUM"));
    cpl_test_eq(1,cpl_propertylist_get_bool(casu_fits_get_ehu(conf),
                                            "ESO DRS CHOPCOR"));

    /* Now test vimos_chop_lowconfbands */

    status = CASU_FATAL;
    retval = vimos_chop_lowconfbands(sci,conf,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(retval,CASU_FATAL);
    status = CASU_OK;
    retval = vimos_chop_lowconfbands(sci,conf,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(retval,CASU_OK);
    cpl_test_eq(1,cpl_propertylist_get_int(casu_fits_get_ehu(conf),
                                           "ESO DRS CHOPMIN"));
    cpl_test_eq(2400,cpl_propertylist_get_int(casu_fits_get_ehu(conf),
                                              "ESO DRS CHOPMAX"));
    cpl_test_eq(1,cpl_propertylist_get_int(casu_fits_get_ehu(sci),
                                           "ESO DRS CHOPMIN"));
    cpl_test_eq(2400,cpl_propertylist_get_int(casu_fits_get_ehu(sci),
                                              "ESO DRS CHOPMAX"));
    cpl_test_eq(1,cpl_propertylist_get_bool(casu_fits_get_ehu(sci),
                                            "ESO DRS CHOPCOR"));
    cpl_test_eq(2048,cpl_image_get_size_x(casu_fits_get_image(sci)));
    cpl_test_eq(2400,cpl_image_get_size_y(casu_fits_get_image(sci)));
    

    /* Get out of here */

    casu_fits_delete(sci);
    casu_fits_delete(conf);
    return(cpl_test_end(0));

}

/*

$Log: vimos_chop_lowconf-test.c,v $
Revision 1.1  2015/10/16 12:34:11  jim
new



*/
