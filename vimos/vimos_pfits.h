/* $Id: vimos_pfits.h,v 1.6 2015/08/07 13:07:15 jim Exp $
 *
 * This file is part of the VIMOS Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:07:15 $
 * $Revision: 1.6 $
 * $Name:  $
 */

/* Includes */

#ifndef VIMOS_PFITS_H
#define VIMOS_PFITS_H

#include <cpl.h>

/* Function prototypes */

int vimos_pfits_get_exptime(cpl_propertylist *plist, float *exptime);
int vimos_pfits_get_lst(cpl_propertylist *plist, double *lst);
int vimos_pfits_get_mjd(cpl_propertylist *plist, double *mjd);
int vimos_pfits_get_gain(cpl_propertylist *plist, float *gain);
int vimos_pfits_get_chipname(cpl_propertylist *plist, char *chipname);
int vimos_pfits_get_filter(cpl_propertylist *plist, char *filter);
int vimos_pfits_get_dateobs(cpl_propertylist *plist, char *dateobs);
int vimos_pfits_get_projid(cpl_propertylist *plist, char *projid);
int vimos_pfits_get_tpl_start(cpl_propertylist *plist, char const ** tpl_start);
#endif

/* 

$Log: vimos_pfits.h,v $
Revision 1.6  2015/08/07 13:07:15  jim
Fixed copyright to ESO

Revision 1.5  2015/01/29 12:07:30  jim
Modified comments

Revision 1.4  2014/12/11 12:27:28  jim
new version

Revision 1.3  2013/11/28 12:24:39  jim
Fixed a few typos

Revision 1.2  2013/11/21 09:39:08  jim
detabbed

Revision 1.1.1.1  2013-10-24 08:36:12  jim
Initial import


*/
