/* $Id: vimos_ima_twilight_flat.c,v 1.16 2015/10/15 11:28:34 jim Exp $
 *
 * This file is part of the VIMOS Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/10/15 11:28:34 $
 * $Revision: 1.16 $
 * $Name:  $
 */

/* Includes */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <string.h>
#include <cpl.h>
#include <math.h>

#include "vmutils.h"
#include "vimos_imaging_utils.h"
#include "vimos_pfits.h"
#include "vimos_dfs.h"
#include "vimos_mods.h"
#include "casu_utils.h"
#include "casu_stats.h"
#include "casu_fits.h"
#include "casu_mask.h"
#include "casu_wcsutils.h"
#include "casu_mods.h"

/* Define values for bit mask that flags dummy results */

#define MEANTWI     1
#define CONFMAP     2
#define RATIMG      4
#define STATS_TAB   8
#define BPM        16

/* Structure definitions */

typedef struct {

    /* Input */

    float       lthr;
    float       hthr;
    int         combtype;
    int         scaletype;
    int         xrej;
    float       thresh;
    int         ncells;
    int         prettynames;

    /* Output */

    float       flatrms;
    float       flatratio_med;
    float       flatratio_rms;
    float       minv;
    float       maxv;
    float       avev;
    float       photnoise;
    float       snratio;
    int         nbad;
    float       badfrac;

    /* Bitmasks used to make sure we get all the products we're expecting */

    int         we_expect;
    int         we_get;
} configstruct;

typedef struct {
    cpl_size         *labels;
    cpl_size         *labels2;
    cpl_frameset     *twilightlist;
    cpl_frame        *master_bias;
    cpl_frame        *master_dark;
    cpl_frame        *ref_twilight_flat;

    cpl_image        *outimage;
    cpl_image        *outconf;
    cpl_image        *outbpm;
    casu_fits        **twilights;
    int              ntwilights;
    cpl_propertylist *drs;
    cpl_propertylist *drs2;
    casu_fits        *rfimage;
    cpl_image        *ratioimg;
    cpl_table        *ratioimstats;
    casu_fits        *mbias;
    casu_fits        *mdark;
} memstruct;

/* Function prototypes */

static int vimos_ima_twilight_flat_create(cpl_plugin *) ;
static int vimos_ima_twilight_flat_exec(cpl_plugin *) ;
static int vimos_ima_twilight_flat_destroy(cpl_plugin *) ;
static int vimos_ima_twilight_flat(cpl_parameterlist *, cpl_frameset *) ;
static int vimos_ima_twilight_flat_save(cpl_frameset *framelist, 
                                            cpl_parameterlist *parlist,
                                            char *cname, configstruct *cs,
                                            memstruct *ps, int isfirst,
                                            cpl_frame **product_frame_mean_twi,
                                            cpl_frame **product_frame_conf,
                                            cpl_frame **product_frame_ratioimg,
                                            cpl_frame **product_frame_ratioimg_stats,
                                            cpl_frame **product_frame_bpm);
static void vimos_ima_twilight_flat_dummy_products(configstruct *cs,
                                                       memstruct *ps);
static void vimos_ima_twilight_flat_normal(int jext, configstruct *cs,
                                               memstruct *ps, 
                                               char *vimos_names[]);
static int vimos_ima_twilight_flat_lastbit(cpl_frameset *framelist,
                                               cpl_parameterlist *parlist,
                                               char *chipname, 
                                               configstruct *cs, memstruct *ps,
                                               int isfirst,
                                               cpl_frame **product_frame_mean_twi,
                                               cpl_frame **product_frame_conf,
                                               cpl_frame **product_frame_ratioimg,
                                               cpl_frame **product_frame_ratioimg_stats,
                                               cpl_frame **product_frame_bpm);
                                                   
static void vimos_ima_twilight_flat_init(memstruct *ps);
static void vimos_ima_twilight_flat_tidy(memstruct *ps, int level);

static char vimos_ima_twilight_flat_description[] =
"vimos_ima_twilight_flat -- VIMOS twilight flat combine recipe.\n\n"
"Combine a list of twilight flat frames into a mean frame. Optionally\n"
"compare the output frame to a reference twilight flat frame\n\n"
"The program accepts the following files in the SOF:\n\n"
"    Tag                   Description\n"
"    -----------------------------------------------------------------------\n"
"    %-21s A list of raw twilight flat images\n"
"    %-21s A master bias frame\n"
"    %-21s A master dark frame\n"
"    %-21s Optional reference twilight flat frame\n"
"If no reference twilight flat is made available, then no comparison will be\n"
"done. This means there will be no output ratio image and no stats"
"\n";

/**@{*/

/**
    \ingroup recipelist
    \defgroup vimos_ima_twilight_flat vimos_ima_twilight_flat
    \brief Combine a series of twilight flat exposures into a single mean 
           frame

    \par Name: 
        vimos_ima_twilight_flat
    \par Purpose: 
        Combine a series of twilight flat exposures into a single mean frame
    \par Description: 
        A list of twilight flats are corrected with appropriate master bias
        and dark frames. These corrected images are combined with rejection 
        to form a mean twilight flat. If a reference twilight flat is 
        supplied, then a ratio image is formed between it and the combined 
        result from the current frame list. This ratio image can be useful 
        for looking at the evolution of the structure of the flat field. The 
        ratio image is broken into lots of little cells. The median value of 
        the ratio image as well as the RMS in each cell is written to a 
        ratio image statistics table.
    \par Language:
        C
    \par Parameters:
        - \b lthr (float): The low threshold below which an image is to be
             considered underexposed.
        - \b hthr (float): The high threshold above which an image is to be
             considered overexposed.
        - \b combtype (string): Determines the type of combination that is done
             to form the output map. Can take the following values:
            - median: The output pixels are medians of the input pixels
            - mean: The output pixels are means of the input pixels
        - \b scaletype (string): Determines how the input data are scaled or 
             offset before they are combined. Can take the following values:
            - none: No scaling of offsetting
            - additive: All input frames are biassed additively to bring their
              backgrounds to a common mean.
            - multiplicative: All input frames are scaled multiplicatively to 
              bring their backgrounds to a common mean.
            - exptime: All input frames are scaled to a uniform exposure time 
              and then additively corrected to bring their backgrounds to a 
              common mean
        - \b xrej (int): If set, then an extra rejection cycle will be run. 
        - \b thresh (float): The rejection threshold in numbers of sigmas.
        - \b ncells (int): If a ratio image statistics table is being 
             done, then this is the number of cells in which to divide each
             image. The value should be a power of 2, up to 64.
        - \b prettynames (bool): If set then a descriptive file name will
             be used for the output products. Otherwise the standard ESO 
             'predictable' name will be used.
    \par Input File Types:
        The following list gives the file types that can appear in the SOF
        file. The word in bold is the DO category value.
        - \b FLAT_TWILIGHT (required): A list of raw dark images for 
             combining
        - \b MASTER_BIAS (required): A master bias frame. This is needed 
             for the bias correction of the input twilight flat images.
        - \b MASTER_DARK (required): A master dark frame. This is needed 
             for the dark correction of the input twilight flat images.
        - \b REFERENCE_TWILIGHT_FLAT (optional): A library twilight flat frame
             from a previous run. If this is given, then a ratio image will be 
             formed and some basic statistics run on it.
    \par Output Products:
        The following list gives the output data products that are generated
        by this recipe. The word in bold gives the DPR CATG keyword value for
        each product:
        - The output mean/median twilight frame, formed by either a mean or 
          median combination of the input raw frames with rejection 
          (\b MASTER_TWILIGHT_FLAT).
        - The output ratio image, formed by dividing the input reference
          twilight frame into the current mean/median twilight frame. This is 
          only done if a master twilight flat frame is specified from the 
          outset. (\b RATIOIMG_TWILIGHT_FLAT)
        - The output ratio image statistics table
          (\b RATIOIMG_STATS_TWLIGHT_FLAT).
        - The output master confidence map (\b MASTER_CONF).
        - The output master bad pixel mask (\b MASTER_BPM).
    \par Output QC Parameters:
        - \b FLATRMS
          The RMS value of the mean twilight frame.
        - \b FLATRATIO_MED
          The median of the twilight flat ratio image
        - \b FLATRATIO_RMS
          The RMS of the twilight flat ratio image
        - \b FLATMIN
          The ensemble minimum of all of the input images
        - \b FLATMAX
          The ensemble maximum of all of the input images
        - \b FLATAVG
          The ensemble average of all of the input images
        - \b FLATRNG
          The ensemble range of all of the input images
        - \b TWIPHOT
          The photon noise estimated from the middle two images of
          the input list (in ADU).
        - \b TWISNRATIO
          The estimated s/n ratio from the middle two images of the 
          input list
        - \b NBAD
          The number of bad pixels detected
        - \b BADFRAC
          The fraction of the pixels that are flagged as bad
    \par Notes
        None.
    \par Fatal Error Conditions:
        - NULL input frameset
        - Input frameset headers incorrect meaning that RAW and CALIB frame
          can't be distinguished
        - No twilight flat frames in the input frameset
        - No master bias frame in input frameset
        - No master dark frame in input frameset
        - Unable to save data products
        - Unable to load any of the images in the input frameset
    \par Non-Fatal Error Conditions:
        - No reference twilight flat. No ratio image formed.
    \par Conditions Leading To Dummy Products:
        - The detector for the current image extension is flagged dead
        - All images are either below the under-exposure threshold or above
          the over exposure threshold.
        - Master dark or master bias is a dummy
        - Combination routine failed
        - Reference twilight frame is flagged as a dummy
    \par Author:
        Jim Lewis, CASU
    \par Code Reference: 
        vimos_ima_twilight_flat.c
*/

/* Function code */

/*---------------------------------------------------------------------------*/
/**
  @brief    Build the list of available plugins, for this module.
  @param    list    the plugin list
  @return   0 if everything is ok

  This function is exported.
 */
/*---------------------------------------------------------------------------*/

int cpl_plugin_get_info(cpl_pluginlist *list) {
    cpl_recipe  *recipe = cpl_calloc(1,sizeof(*recipe));
    cpl_plugin  *plugin = &recipe->interface;
    char alldesc[SZ_ALLDESC];
    (void)snprintf(alldesc,SZ_ALLDESC,vimos_ima_twilight_flat_description,
                   VIMOS_TWI_RAW,VIMOS_CAL_BIAS,VIMOS_CAL_DARK,
                   VIMOS_REF_TWILIGHT_FLAT,VIMOS_REF_BPM,VIMOS_CAL_CONF);

    cpl_plugin_init(plugin,
                    CPL_PLUGIN_API,
                    VIMOS_BINARY_VERSION,
                    CPL_PLUGIN_TYPE_RECIPE,
                    "vimos_ima_twilight_flat",
                    "Twilight flat combination for imaging",
                    alldesc,
                    "Jim Lewis",
                    "jrl@ast.cam.ac.uk",
                    vimos_get_license(),
                    vimos_ima_twilight_flat_create,
                    vimos_ima_twilight_flat_exec,
                    vimos_ima_twilight_flat_destroy);

    cpl_pluginlist_append(list,plugin);

    return(0);
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options
  @param    plugin  the plugin
  @return   0 if everything is ok

  Create the recipe instance and make it available to the application using the
  interface.
 */
/*---------------------------------------------------------------------------*/

static int vimos_ima_twilight_flat_create(cpl_plugin *plugin) {
    cpl_recipe      *recipe;
    cpl_parameter   *p;

    /* Get the recipe out of the plugin */

    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE)
        recipe = (cpl_recipe *)plugin;
    else 
        return(-1);

    /* Create the parameters list in the cpl_recipe object */

    recipe->parameters = cpl_parameterlist_new();

    /* Lower threshold for rejecting underexposed images */

    p = cpl_parameter_new_range("vimos.vimos_ima_twilight_flat.lthr",
                                CPL_TYPE_DOUBLE,
                                "Low rejection threshold for underexpsed images",
                                "vimos.vimos_ima_twilight_flat",
                                4000.0,0.0,65535.0);
    cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,"lthr");
    cpl_parameterlist_append(recipe->parameters,p);

    /* Upper threshold for rejecting overexposed images */

    p = cpl_parameter_new_range("vimos.vimos_ima_twilight_flat.hthr",
                                CPL_TYPE_DOUBLE,
                                "High rejection threshold for overexposed images",
                                "vimos.vimos_ima_twilight_flat",
                                60000.0,0.0,65535.0);
    cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,"hthr");
    cpl_parameterlist_append(recipe->parameters,p);

    /* Fill in the parameters. First the combination type */

    p = cpl_parameter_new_enum("vimos.vimos_ima_twilight_flat.combtype",
                               CPL_TYPE_STRING,
                               "Combination algorithm",
                               "vimos.vimos_ima_twilight_flat",
                               "median",2,"median","mean");
    cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,"combtype");
    cpl_parameterlist_append(recipe->parameters,p);

    /* The requested scaling */

    p = cpl_parameter_new_enum("vimos.vimos_ima_twilight_flat.scaletype",
                               CPL_TYPE_STRING,
                               "Scaling algorithm",
                               "vimos.vimos_ima_twilight_flat",
                               "multiplicative",4,"none","additive",
                               "multiplicative","exptime");
    cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,"scaletype");
    cpl_parameterlist_append(recipe->parameters,p);
    
    /* Extra rejection cycle */

    p = cpl_parameter_new_value("vimos.vimos_ima_twilight_flat.xrej",
                                CPL_TYPE_BOOL,
                                "True if using extra rejection cycle",
                                "vimos.vimos_ima_twilight_flat",
                                TRUE);
    cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,"xrej");
    cpl_parameterlist_append(recipe->parameters,p);

    /* Rejection threshold */

    p = cpl_parameter_new_range("vimos.vimos_ima_twilight_flat.thresh",
                                CPL_TYPE_DOUBLE,
                                "Rejection threshold in sigma above background",
                                "vimos.vimos_ima_twilight_flat",5.0,1.0e-6,
                                1.0e10);
    cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,"thresh");
    cpl_parameterlist_append(recipe->parameters,p);

    /* How many cells to divide each image */

    p = cpl_parameter_new_enum("vimos.vimos_ima_twilight_flat.ncells",
                               CPL_TYPE_INT,
                               "Number of cells for diff image stats",
                               "vimos.vimos_ima_twilight_flat",8,7,1,2,4,
                               8,16,32,64);
    cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,"ncells");
    cpl_parameterlist_append(recipe->parameters,p);     

    /* Flag to use pretty output product file names */

    p = cpl_parameter_new_value("vimos.vimos_ima_twilight_flat.prettynames",
                                CPL_TYPE_BOOL,
                                "Use pretty output file names?",
                                "vimos.vimos_ima_twilight_flat",FALSE);
    cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,"prettynames");
    cpl_parameterlist_append(recipe->parameters,p);     

    /* Get out of here */

    return(0);
}
        
/*---------------------------------------------------------------------------*/
/**
  @brief    Execute the plugin instance given by the interface
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/

static int vimos_ima_twilight_flat_exec(cpl_plugin *plugin) {
    cpl_recipe  *recipe;

    /* Get the recipe out of the plugin */

    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE)
        recipe = (cpl_recipe *)plugin;
    else 
        return(-1);

    return(vimos_ima_twilight_flat(recipe->parameters,recipe->frames));
}
                                
/*---------------------------------------------------------------------------*/
/**
  @brief    Destroy what has been created by the 'create' function
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/

static int vimos_ima_twilight_flat_destroy(cpl_plugin *plugin) {
    cpl_recipe *recipe ;

    /* Get the recipe out of the plugin */

    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE)
        recipe = (cpl_recipe *)plugin;
    else 
        return(-1);

    cpl_parameterlist_delete(recipe->parameters);
    return(0);
}

/*---------------------------------------------------------------------------*/
/**
  @brief    The recipe data reduction part is implemented here 
  @param    parlist     the parameters list
  @param    framelist   the frames list
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/

static int vimos_ima_twilight_flat(cpl_parameterlist *parlist, 
                                       cpl_frameset *framelist) {
    const char *fctid="vimos_ima_twilight_flat";
    int j,retval,status,nx,ny,npts,isfirst;
    cpl_size nlab,nlab2;
    long i;
    cpl_parameter *p;
    cpl_image *im1,*im2,*newim,*diffim;
    cpl_frameset *chiplist;
    double val1,val2,scl;
    float *data,med,mad,texp;
    unsigned char *rejmask,*rejplus;
    configstruct cs;
    memstruct ps;
    cpl_frame *product_frame_mean_twi,*product_frame_conf,*product_frame_ratioimg;
    cpl_frame *product_frame_ratioimg_stats,*product_frame_bpm,*testfrm;
    cpl_image *junk;
    char *vimos_names[VIMOS_NEXTN];

    /* Check validity of input frameset */

    if (framelist == NULL || cpl_frameset_get_size(framelist) <= 0) {
        cpl_msg_error(fctid,"Input framelist NULL or has no input data");
        return(-1);
    }

    /* Initialise some things */

    cs.we_expect = 0;
    cs.we_get = 0;
    vimos_ima_twilight_flat_init(&ps);
    cs.we_expect = MEANTWI + CONFMAP + BPM;

    /* Get the parameters */

    p = cpl_parameterlist_find(parlist,
                               "vimos.vimos_ima_twilight_flat.lthr");
    cs.lthr = (float)cpl_parameter_get_double(p);
    p = cpl_parameterlist_find(parlist,
                               "vimos.vimos_ima_twilight_flat.hthr");
    cs.hthr = (float)cpl_parameter_get_double(p);
    p = cpl_parameterlist_find(parlist,
                               "vimos.vimos_ima_twilight_flat.combtype");
    cs.combtype = (strcmp(cpl_parameter_get_string(p),"median") ? 2 : 1);
    p = cpl_parameterlist_find(parlist,
                               "vimos.vimos_ima_twilight_flat.scaletype");
    if (! strcmp(cpl_parameter_get_string(p),"none")) 
        cs.scaletype = 0;
    else if (! strcmp(cpl_parameter_get_string(p),"additive")) 
        cs.scaletype = 1;
    else if (! strcmp(cpl_parameter_get_string(p),"multiplicative")) 
        cs.scaletype = 2;
    else if (! strcmp(cpl_parameter_get_string(p),"exptime")) 
        cs.scaletype = 3;
    p = cpl_parameterlist_find(parlist,
                               "vimos.vimos_ima_twilight_flat.xrej");
    cs.xrej = cpl_parameter_get_bool(p);
    p = cpl_parameterlist_find(parlist,
                               "vimos.vimos_ima_twilight_flat.thresh");
    cs.thresh = (float)cpl_parameter_get_double(p);
    p = cpl_parameterlist_find(parlist,
                               "vimos.vimos_ima_twilight_flat.ncells");
    cs.ncells = cpl_parameter_get_int(p);
    p = cpl_parameterlist_find(parlist,"vimos.vimos_ima_twilight_flat.prettynames");
    cs.prettynames = (cpl_parameter_get_bool(p) ? 1 : 0);

    /* Sort out raw from calib frames */

    if (vimos_dfs_set_groups(framelist) != CASU_OK) {
        cpl_msg_error(fctid,"Cannot identify RAW and CALIB frames");
        vimos_ima_twilight_flat_tidy(&ps,0);
        return(-1);
    }

    /* Get the twilight frames */

    if ((ps.labels = cpl_frameset_labelise(framelist,casu_compare_tags,
                                           &nlab)) == NULL) {
        cpl_msg_error(fctid,"Cannot labelise the input frames");
        vimos_ima_twilight_flat_tidy(&ps,0);
        return(-1);
    }
    if ((ps.twilightlist = casu_frameset_subgroup(framelist,ps.labels,nlab,
                                                  VIMOS_TWI_RAW)) == NULL) {
        cpl_msg_error(fctid,"Cannot find twilight frames in input frameset");
        vimos_ima_twilight_flat_tidy(&ps,0);
        return(-1);
    }

    /* Now labelise the input frameset so that we separate them by the
       detector name */

    if ((ps.labels2 = cpl_frameset_labelise(ps.twilightlist,vimos_compare_names,
                                            &nlab2)) == NULL) {
        cpl_msg_error(fctid,"Cannot labelise the twilight frames");
        vimos_ima_twilight_flat_tidy(&ps,0);
        return(-1);
    }

    /* See what chip set this is */

    if (vimos_load_names(cpl_frameset_get_position(ps.twilightlist,0),
                         vimos_names) != CASU_OK) {
        cpl_msg_error(fctid,"Cannot get the name set");
        vimos_ima_twilight_flat_tidy(&ps,0);
        return(-1);
    }

    /* Check to see if there is a master bias frame */

    if ((ps.master_bias = casu_frameset_subgroup_1(framelist,ps.labels,nlab,
                                                   VIMOS_CAL_BIAS)) == NULL) {
        cpl_msg_error(fctid,"No master bias found");
        vimos_ima_twilight_flat_tidy(&ps,0);
        return(-1);
    }
        
    /* Check to see if there is a master dark frame */

    if ((ps.master_dark = casu_frameset_subgroup_1(framelist,ps.labels,nlab,
                                                   VIMOS_CAL_DARK)) == NULL) {
        cpl_msg_error(fctid,"No master dark found");
        vimos_ima_twilight_flat_tidy(&ps,0);
        return(-1);
    }
        
    /* Check to see if there is a reference twilight flat frame */

    if ((ps.ref_twilight_flat = casu_frameset_subgroup_1(framelist,ps.labels,nlab,
                                                         VIMOS_REF_TWILIGHT_FLAT)) == NULL)
        cpl_msg_info(fctid,"No reference twilight flat found -- no ratio image will be formed");
    else
        cs.we_expect |= RATIMG;
        
    /* Check to see we should expect a stats table */

    if (cs.we_expect & RATIMG) 
        cs.we_expect |= STATS_TAB;

    /* Now loop for all detectors... */

    for (j = 1; j <= VIMOS_NEXTN; j++) {
        status = CASU_OK;
        cs.we_get = 0;
        isfirst = (j == 1);

        /* Get the subset of images in the input frameset that belong to
           each detector */

        if ((chiplist = vimos_get_det_frameset(ps.twilightlist,ps.labels2,nlab2,
                                               vimos_names[j-1])) == NULL) {
            cpl_msg_info(fctid,"No flats in input list for detector %s\n",
                         vimos_names[j-1]);
            retval = vimos_ima_twilight_flat_lastbit(framelist,parlist,
                                                         vimos_names[j-1],
                                                         &cs,&ps,isfirst,
                                                         &product_frame_mean_twi,
                                                         &product_frame_conf,
                                                         &product_frame_ratioimg,
                                                         &product_frame_ratioimg_stats,
                                                         &product_frame_bpm);
            if (retval != 0) 
                return(-1);
            continue;
        }

        /* Load the images */

        testfrm = cpl_frameset_get_position(chiplist,0);
        ps.twilights = casu_fits_load_list(chiplist,CPL_TYPE_FLOAT,
                                           cpl_frame_get_nextensions(testfrm));
        if (ps.twilights == NULL) {
            cpl_msg_info(fctid,"Detector %s flats wouldn't load",
                         vimos_names[j-1]);
            retval = vimos_ima_twilight_flat_lastbit(framelist,parlist,
                                                         vimos_names[j-1],
                                                         &cs,&ps,isfirst,
                                                         &product_frame_mean_twi,
                                                         &product_frame_conf,
                                                         &product_frame_ratioimg,
                                                         &product_frame_ratioimg_stats,
                                                         &product_frame_bpm);
            if (retval != 0)
                return(-1);
            continue;
        }
        ps.ntwilights = cpl_frameset_get_size(chiplist);
        cpl_frameset_delete(chiplist);

        /* Sort out the images that are either over or under exposed */

        casu_overexp(ps.twilights,&(ps.ntwilights),1,cs.lthr,cs.hthr,1,
                     &(cs.minv),&(cs.maxv),&(cs.avev));

        /* Check to see how many are left. If there aren't any, then
           signal a major error */

        if (ps.ntwilights == 0) {
            cpl_msg_info(fctid,"All %s images either under or overexposed",
                         vimos_names[j-1]);
            retval = vimos_ima_twilight_flat_lastbit(framelist,parlist,
                                                         vimos_names[j-1],
                                                         &cs,&ps,isfirst,
                                                         &product_frame_mean_twi,
                                                         &product_frame_conf,
                                                         &product_frame_ratioimg,
                                                         &product_frame_ratioimg_stats,
                                                         &product_frame_bpm);
            if (retval != 0)
                return(-1);
            continue;
        }

        /* Take the middle two exposures and create a scaled difference image
           estimate */

        nx = (int)cpl_image_get_size_x(casu_fits_get_image(ps.twilights[0]));
        ny = (int)cpl_image_get_size_y(casu_fits_get_image(ps.twilights[0]));
        if (ps.ntwilights > 1) {
            i = ps.ntwilights/2 - 1;
            im1 = casu_fits_get_image(ps.twilights[i]);
            im2 = casu_fits_get_image(ps.twilights[i+1]);
            val1 = cpl_image_get_median_window(im1,500,500,1000,1000);
            val2 = cpl_image_get_median_window(im2,500,500,1000,1000);
            scl = val1/val2;
            newim = cpl_image_multiply_scalar_create(im2,scl);
            diffim = cpl_image_subtract_create(im1,newim);
            cpl_image_delete(newim);
            data = cpl_image_get_data_float(diffim);
            npts = nx*ny;
            casu_medmad(data,NULL,npts,&med,&mad);
            mad *= 1.48/CPL_MATH_SQRT2;
            cs.photnoise = mad;
            cs.snratio = val1*sqrt((double)(ps.ntwilights))/mad;
            cpl_image_delete(diffim);
        } else {
            cs.photnoise = 0.0;
            cs.snratio = 0.0;
        }

        /* Right, we want to bias correct, so we need to load the mean 
           bias and make sure it isn't a dummy */

        ps.mbias = casu_fits_load(ps.master_bias,CPL_TYPE_FLOAT,j);
        if (ps.mbias == NULL) {
            cpl_msg_info(fctid,
                         "Can't load master bias for detector %s",
                         vimos_names[j-1]);
            retval = vimos_ima_twilight_flat_lastbit(framelist,parlist,
                                                         vimos_names[j-1],
                                                         &cs,&ps,isfirst,
                                                         &product_frame_mean_twi,
                                                         &product_frame_conf,
                                                         &product_frame_ratioimg,
                                                         &product_frame_ratioimg_stats,
                                                         &product_frame_bpm);
            if (retval != 0)
                return(-1);
            continue;
        } else if (casu_is_dummy(casu_fits_get_ehu(ps.mbias))) {
            cpl_msg_info(fctid,
                         "Master bias for detector %s is a dummy",
                         vimos_names[j-1]);
            retval = vimos_ima_twilight_flat_lastbit(framelist,parlist,
                                                         vimos_names[j-1],
                                                         &cs,&ps,isfirst,
                                                         &product_frame_mean_twi,
                                                         &product_frame_conf,
                                                         &product_frame_ratioimg,
                                                         &product_frame_ratioimg_stats,
                                                         &product_frame_bpm);
            if (retval != 0)
                return(-1);
            continue;
        }

        /* Loop for each image and bias correct */

        cpl_msg_info(fctid,"Bias correcting detector %s",vimos_names[j-1]);
        for (i = 0; i < ps.ntwilights; i++)
            vimos_biascor((ps.twilights)[i],ps.mbias,1,1,&status);

        /* Now we want to dark correct, so we need to load the mean 
           dark and make sure it isn't a dummy */

        ps.mdark = casu_fits_load(ps.master_dark,CPL_TYPE_FLOAT,j);
        if (ps.mdark == NULL) {
            cpl_msg_info(fctid,
                         "Can't load master dark for detector %s",
                         vimos_names[j-1]);
            retval = vimos_ima_twilight_flat_lastbit(framelist,parlist,
                                                         vimos_names[j-1],
                                                         &cs,&ps,isfirst,
                                                         &product_frame_mean_twi,
                                                         &product_frame_conf,
                                                         &product_frame_ratioimg,
                                                         &product_frame_ratioimg_stats,
                                                         &product_frame_bpm);
            if (retval != 0)
                return(-1);
            continue;
        } else if (casu_is_dummy(casu_fits_get_ehu(ps.mdark))) {
            cpl_msg_info(fctid,
                         "Master dark for detector %s is a dummy",
                         vimos_names[j-1]);
            retval = vimos_ima_twilight_flat_lastbit(framelist,parlist,
                                                         vimos_names[j-1],
                                                         &cs,&ps,isfirst,
                                                         &product_frame_mean_twi,
                                                         &product_frame_conf,
                                                         &product_frame_ratioimg,
                                                         &product_frame_ratioimg_stats,
                                                         &product_frame_bpm);
            if (retval != 0)
                return(-1);
            continue;
        }

        /* Loop for each image and dark correct */

        cpl_msg_info(fctid,"Dark correcting detector %s",vimos_names[j-1]);
        for (i = 0; i < ps.ntwilights; i++) {
            vimos_pfits_get_exptime(casu_fits_get_ehu((ps.twilights)[i]),&texp);
            casu_darkcor((ps.twilights)[i],ps.mdark,texp,&status);
        }

        /* Call the combine module */

        cpl_msg_info(fctid,"Doing combination for detector %s",
                     vimos_names[j-1]);
        (void)casu_imcombine(ps.twilights,NULL,ps.ntwilights,cs.combtype,
                             cs.scaletype,cs.xrej,cs.thresh,"",&(ps.outimage),
                             &junk,&rejmask,&rejplus,&(ps.drs),&status);
        freespace(rejmask);
        freespace(rejplus);

        /* If these correction and combination routines failed at any stage
           then get out of here */

        if (status == CASU_OK) {
            cs.we_get |= MEANTWI;
            vimos_ima_twilight_flat_normal(j,&cs,&ps,vimos_names);
        } else {
            cpl_msg_info(fctid,"A processing step failed");
        }

        /* Create any dummies and save the products */
        
        retval = vimos_ima_twilight_flat_lastbit(framelist,parlist,
                                                     vimos_names[j-1],&cs,&ps,isfirst,
                                                     &product_frame_mean_twi,
                                                     &product_frame_conf,
                                                     &product_frame_ratioimg,
                                                     &product_frame_ratioimg_stats,
                                                     &product_frame_bpm);
        if (retval != 0)
            return(-1);

    }
    vimos_ima_twilight_flat_tidy(&ps,0);
    return(0);
}

/*---------------------------------------------------------------------------*/
/**
  @brief    The recipe data products are saved here
  @param    framelist    the input frame list
  @param    parlist      the input recipe parameter list
  @param    cname        The name of the current detector
  @param    cs           The configuration structure defined in the main routine
  @param    ps           The memory structure defined in the main routine
  @param    isfirst      Set if this is the first extension to be saved
  @param    product_frame_mean_twi    The frame for the mean flat product
  @param    product_frame_conf        The frame for the conf map product
  @param    product_frame_ratioimg    The frame for the ratio image product
  @param    product_frame_ratioimg_stats The frame for the ratioimg stats table
  @param    product_frame_bpm         The frame for the bad pixel mask product
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/

static int vimos_ima_twilight_flat_save(cpl_frameset *framelist, 
                                            cpl_parameterlist *parlist,
                                            char *cname, configstruct *cs,
                                            memstruct *ps, int isfirst,
                                            cpl_frame **product_frame_mean_twi,
                                            cpl_frame **product_frame_conf,
                                            cpl_frame **product_frame_ratioimg,
                                            cpl_frame **product_frame_ratioimg_stats,
                                            cpl_frame **product_frame_bpm) {
    cpl_propertylist *plist,*elist,*p;
    int status,i,night;
    float val;
    const char *fctid = "vimos_ima_twilight_flat_save";
    const char *esoout[] = {"twilightcomb.fits","twilightratio.fits",
                            "twilightratiotab.fits","twilightconf.fits",
                            "bpmmap.fits"};
    const char *prettypfx[] = {"flat","ratio","ratiotab","conf","bpm"};
    char outfile[5][BUFSIZ],dateobs[81],filter[16];
    const char *recipeid = "vimos_ima_twilight_flat";

   /* Get info to make pretty names if requested. */

    for (i = 0; i < 5; i++) 
        strcpy(outfile[i],esoout[i]);
    if (cs->prettynames) {
        plist = casu_fits_get_phu(ps->twilights[0]);
        elist = casu_fits_get_ehu(ps->twilights[0]);
        if ((vimos_pfits_get_dateobs(plist,dateobs) != CASU_OK ||
            vimos_pfits_get_filter(plist,filter) != CASU_OK) && 
            (vimos_pfits_get_dateobs(elist,dateobs) != CASU_OK ||
             vimos_pfits_get_filter(elist,filter) != CASU_OK)) {
            cpl_msg_warning(fctid,"Missing header information. Reverting to predictable names");
        } else {
            night = casu_night_from_dateobs(dateobs);
            for (i = 0; i < 5; i++) 
                (void)sprintf(outfile[i],"%s_%s_%08d.fits",filter,prettypfx[i],
                              night);
        }
    }

    /* If we need to make a PHU then do that now based on the first frame
       in the input frame list */

    if (isfirst) {

        /* Create a new product frame object and define some tags */

        *product_frame_mean_twi = cpl_frame_new();
        cpl_frame_set_filename(*product_frame_mean_twi,outfile[0]);
        cpl_frame_set_tag(*product_frame_mean_twi,VIMOS_PRO_TWILIGHT_FLAT);
        cpl_frame_set_type(*product_frame_mean_twi,CPL_FRAME_TYPE_IMAGE);
        cpl_frame_set_group(*product_frame_mean_twi,CPL_FRAME_GROUP_PRODUCT);
        cpl_frame_set_level(*product_frame_mean_twi,CPL_FRAME_LEVEL_FINAL);

        /* Set up the PHU header */

        plist = cpl_propertylist_duplicate(casu_fits_get_ehu(ps->twilights[0]));
        vimos_dfs_set_product_primary_header(plist,*product_frame_mean_twi,
                                             framelist,parlist,
                                             (char *)recipeid,
                                             "PRO-1.15",NULL,0);

        /* 'Save' the PHU image */                       

        if (vimos_pfits_get_filter(plist,filter) == CASU_OK) 
            cpl_propertylist_update_string(plist,"FILTER",filter);
        if (cpl_image_save(NULL,outfile[0],CPL_TYPE_UCHAR,plist,
                           CPL_IO_DEFAULT) != CPL_ERROR_NONE) {
            cpl_msg_error(fctid,"Cannot save product PHU");
            cpl_frame_delete(*product_frame_mean_twi);
            return(-1);
        }
        cpl_frameset_insert(framelist,*product_frame_mean_twi);
        cpl_propertylist_delete(plist);

        /* Create a new product frame object and define some tags for the
           output confidence map */

        *product_frame_conf = cpl_frame_new();
        cpl_frame_set_filename(*product_frame_conf,outfile[3]);
        cpl_frame_set_tag(*product_frame_conf,VIMOS_PRO_CONF);
        cpl_frame_set_type(*product_frame_conf,CPL_FRAME_TYPE_IMAGE);
        cpl_frame_set_group(*product_frame_conf,CPL_FRAME_GROUP_PRODUCT);
        cpl_frame_set_level(*product_frame_conf,CPL_FRAME_LEVEL_FINAL);

        /* Set up the PHU header */

        plist = cpl_propertylist_duplicate(casu_fits_get_ehu(ps->twilights[0]));
        vimos_dfs_set_product_primary_header(plist,*product_frame_conf,
                                             framelist,parlist,
                                             (char *)recipeid,"PRO-1.15",
                                             NULL,0);

        /* 'Save' the PHU image */                       

        if (cpl_image_save(NULL,outfile[3],CPL_TYPE_UCHAR,plist,
                           CPL_IO_DEFAULT) != CPL_ERROR_NONE) {
            cpl_msg_error(fctid,"Cannot save product PHU");
            cpl_frame_delete(*product_frame_conf);
            return(-1);
        }
        cpl_frameset_insert(framelist,*product_frame_conf);
        cpl_propertylist_delete(plist);

        /* Create a new product frame object and define some tags for the
           output bad pixel mask */

        *product_frame_bpm = cpl_frame_new();
        cpl_frame_set_filename(*product_frame_bpm,outfile[4]);
        cpl_frame_set_tag(*product_frame_bpm,VIMOS_PRO_BPM);
        cpl_frame_set_type(*product_frame_bpm,CPL_FRAME_TYPE_IMAGE);
        cpl_frame_set_group(*product_frame_bpm,CPL_FRAME_GROUP_PRODUCT);
        cpl_frame_set_level(*product_frame_bpm,CPL_FRAME_LEVEL_FINAL);

        /* Set up the PHU header */

        plist = cpl_propertylist_duplicate(casu_fits_get_ehu(ps->twilights[0]));
        vimos_dfs_set_product_primary_header(plist,*product_frame_bpm,
                                             framelist,parlist,
                                             (char *)recipeid,"PRO-1.15",
                                             NULL,0);

        /* 'Save' the PHU image */                       

        if (cpl_image_save(NULL,outfile[4],CPL_TYPE_UCHAR,plist,
                           CPL_IO_DEFAULT) != CPL_ERROR_NONE) {
            cpl_msg_error(fctid,"Cannot save product PHU");
            cpl_frame_delete(*product_frame_bpm);
            return(-1);
        }
        cpl_frameset_insert(framelist,*product_frame_bpm);
        cpl_propertylist_delete(plist);

        /* Create a new product frame object for the difference image */

        if (cs->we_expect & RATIMG) {
            *product_frame_ratioimg = cpl_frame_new();
            cpl_frame_set_filename(*product_frame_ratioimg,outfile[1]);
            cpl_frame_set_tag(*product_frame_ratioimg,
                              VIMOS_PRO_RATIOIMG_TWILIGHT_FLAT);
            cpl_frame_set_type(*product_frame_ratioimg,CPL_FRAME_TYPE_IMAGE);
            cpl_frame_set_group(*product_frame_ratioimg,
                                CPL_FRAME_GROUP_PRODUCT);
            cpl_frame_set_level(*product_frame_ratioimg,CPL_FRAME_LEVEL_FINAL);

            /* Set up the PHU header */

            plist = cpl_propertylist_duplicate(casu_fits_get_ehu(ps->twilights[0]));
            vimos_dfs_set_product_primary_header(plist,*product_frame_ratioimg,
                                                 framelist,parlist,
                                                 (char *)recipeid,"PRO-1.15",
                                                 NULL,0);

            /* 'Save' the PHU image */                   

            if (cpl_image_save(NULL,outfile[1],CPL_TYPE_UCHAR,plist,
                               CPL_IO_DEFAULT) != CPL_ERROR_NONE) {
                cpl_msg_error(fctid,"Cannot save product PHU");
                cpl_frame_delete(*product_frame_ratioimg);
                return(-1);
            }
            cpl_frameset_insert(framelist,*product_frame_ratioimg);
            cpl_propertylist_delete(plist);
        }

        /* Create a new product frame object for the difference image stats 
           table */

        if (cs->we_expect & STATS_TAB) {
            *product_frame_ratioimg_stats = cpl_frame_new();
            cpl_frame_set_filename(*product_frame_ratioimg_stats,outfile[2]);
            cpl_frame_set_tag(*product_frame_ratioimg_stats,
                              VIMOS_PRO_RATIOIMG_TWILIGHT_FLAT_STATS);
            cpl_frame_set_type(*product_frame_ratioimg_stats,
                               CPL_FRAME_TYPE_TABLE);
            cpl_frame_set_group(*product_frame_ratioimg_stats,
                                CPL_FRAME_GROUP_PRODUCT);
            cpl_frame_set_level(*product_frame_ratioimg_stats,
                                CPL_FRAME_LEVEL_FINAL);

            /* Set up PHU header */

            plist = cpl_propertylist_duplicate(casu_fits_get_ehu(ps->twilights[0]));
            vimos_dfs_set_product_primary_header(plist,*product_frame_ratioimg_stats,
                                                 framelist,parlist,(char *)recipeid,
                                                 "PRO-1.15",NULL,0);

            /* Fiddle with the extension header now */

            elist = casu_fits_get_ehu(ps->twilights[0]);
            p = cpl_propertylist_duplicate(elist);
            casu_merge_propertylists(p,ps->drs);
            if (! (cs->we_get & STATS_TAB))
                casu_dummy_property(p);
            cpl_propertylist_update_string(p,"EXTNAME",cname);
            vimos_dfs_set_product_exten_header(p,*product_frame_ratioimg_stats,framelist,
                                               parlist,(char *)recipeid,"PRO-1.15",NULL);
            status = CASU_OK;
            casu_removewcs(p,&status);

            /* And finally the difference image stats table */

            if (cpl_table_save(ps->ratioimstats,plist,p,outfile[2],
                               CPL_IO_DEFAULT) != CPL_ERROR_NONE) {
                cpl_msg_error(fctid,"Cannot save product table extension");
                cpl_propertylist_delete(p);
                return(-1);
            }
            cpl_propertylist_delete(p);
            cpl_propertylist_delete(plist);
            cpl_frameset_insert(framelist,*product_frame_ratioimg_stats);
        }
    }

    /* Get the extension property list */

    plist = casu_fits_get_ehu(ps->twilights[0]);
    casu_merge_propertylists(plist,ps->drs);
    cpl_propertylist_update_int(plist,"ESO PRO DATANCOM",ps->ntwilights);

    /* Fiddle with the header now */

    p = cpl_propertylist_duplicate(plist);
    if (! (cs->we_get & MEANTWI)) 
        casu_dummy_property(p);
    cpl_propertylist_update_string(p,"EXTNAME",cname);
    vimos_dfs_set_product_exten_header(p,*product_frame_mean_twi,framelist,parlist,
                                       (char *)recipeid,"PRO-1.15",NULL);
                
    /* Now save the mean twilight flat image extension */

    cpl_propertylist_update_float(p,"ESO QC FLATRMS",cs->flatrms);
    cpl_propertylist_set_comment(p,"ESO QC FLATRMS","RMS of output flat");
    cpl_propertylist_update_float(p,"ESO QC FLATMIN",cs->minv);
    cpl_propertylist_set_comment(p,"ESO QC FLATMIN","Ensemble minimum");
    cpl_propertylist_update_float(p,"ESO QC FLATMAX",cs->maxv);
    cpl_propertylist_set_comment(p,"ESO QC FLATMAX","Ensemble maximum");
    cpl_propertylist_update_float(p,"ESO QC FLATAVG",cs->avev);
    cpl_propertylist_set_comment(p,"ESO QC FLATAVG","Ensemble average");
    val = cs->maxv - cs->minv;
    cpl_propertylist_update_float(p,"ESO QC FLATRNG",val);
    cpl_propertylist_set_comment(p,"ESO QC FLATRNG","Ensemble range");
    cpl_propertylist_update_float(p,"ESO QC TWIPHOT",cs->photnoise);
    cpl_propertylist_set_comment(p,"ESO QC TWIPHOT",
                                 "[adu] Estimated photon noise");
    cpl_propertylist_update_float(p,"ESO QC TWISNRATIO",cs->snratio);
    cpl_propertylist_set_comment(p,"ESO QC TWISNRATIO","Estimated S/N ratio");
    if (cpl_image_save(ps->outimage,outfile[0],CPL_TYPE_FLOAT,p,
                       CPL_IO_EXTEND) != CPL_ERROR_NONE) {
        cpl_msg_error(fctid,"Cannot save product image extension");
        cpl_propertylist_delete(p);
        return(-1);
    }
    cpl_propertylist_delete(p);

    /* Now save the twilight ratio image extension */

    if (cs->we_expect & RATIMG) {
        p = cpl_propertylist_duplicate(plist);
        if (! (cs->we_get & RATIMG))
            casu_dummy_property(p);
        cpl_propertylist_update_float(p,"ESO QC FLATRATIO_MED",
                                      cs->flatratio_med);
        cpl_propertylist_set_comment(p,"ESO QC FLATRATIO_MED",
                                     "Median of ratio map");
        cpl_propertylist_update_float(p,"ESO QC FLATRATIO_RMS",
                                      cs->flatratio_rms);
        cpl_propertylist_set_comment(p,"ESO QC FLATRATIO_RMS",
                                     "RMS of ratio map");
        cpl_propertylist_update_string(p,"EXTNAME",cname);
        vimos_dfs_set_product_exten_header(p,*product_frame_ratioimg,framelist,parlist,
                                           (char *)recipeid,"PRO-1.15",NULL);
        if (cpl_image_save(ps->ratioimg,outfile[1],CPL_TYPE_FLOAT,p,
                           CPL_IO_EXTEND) != CPL_ERROR_NONE) {
            cpl_propertylist_delete(p);
            cpl_msg_error(fctid,"Cannot save product image extension");
            return(-1);
        }
        cpl_propertylist_delete(p);
    }

    /* Now any further ratio image stats tables */

    if (! isfirst && (cs->we_expect & STATS_TAB)) {
        p = cpl_propertylist_duplicate(plist);
        if (! (cs->we_get & STATS_TAB))
            casu_dummy_property(p);
        cpl_propertylist_update_string(p,"EXTNAME",cname);
        vimos_dfs_set_product_exten_header(p,*product_frame_ratioimg_stats,framelist,
                                           parlist,(char *)recipeid,"PRO-1.15",NULL);
        status = CASU_OK;
        casu_removewcs(p,&status);
        if (cpl_table_save(ps->ratioimstats,NULL,p,outfile[2],CPL_IO_EXTEND)
                           != CPL_ERROR_NONE) {
            cpl_msg_error(fctid,"Cannot save product table extension");
            cpl_propertylist_delete(p);
            return(-1);
        }       
        cpl_propertylist_delete(p);
    }

    /* Fiddle with BPM header */

    p = cpl_propertylist_duplicate(plist);
    if (! (cs->we_get & BPM))
        casu_dummy_property(p);
    cpl_propertylist_update_int(p,"ESO QC NBAD",cs->nbad);
    cpl_propertylist_set_comment(p,"ESO QC NBAD",
                                 "Number of bad pixels detected");
    cpl_propertylist_update_float(p,"ESO QC BADFRAC",cs->badfrac);
    cpl_propertylist_set_comment(p,"ESO QC BADFRAC",
                                 "Fraction of bad pixels detected");

    /* Now save the BPM */

    cpl_propertylist_update_string(p,"EXTNAME",cname);
    vimos_dfs_set_product_exten_header(p,*product_frame_bpm,framelist,
                                       parlist,(char *)recipeid,"PRO-1.15",
                                       NULL);
    if (cpl_image_save(ps->outbpm,outfile[4],CPL_TYPE_UCHAR,p,
                       CPL_IO_EXTEND) != CPL_ERROR_NONE) {
        cpl_msg_error(fctid,"Cannot save product image extension");
        cpl_propertylist_delete(p);
        return(-1);
    }
    cpl_propertylist_delete(p);

    /* Fiddle with the confidence map header now */

    casu_merge_propertylists(plist,ps->drs2);
    p = cpl_propertylist_duplicate(plist);
    if (! (cs->we_get & CONFMAP)) 
        casu_dummy_property(p);

    /* Now save the confidence map */

    cpl_propertylist_update_string(p,"EXTNAME",cname);
    vimos_dfs_set_product_exten_header(p,*product_frame_conf,framelist,
                                       parlist,(char *)recipeid,"PRO-1.15",
                                       NULL);
    if (cpl_image_save(ps->outconf,outfile[3],CPL_TYPE_INT,p,
                       CPL_IO_EXTEND) != CPL_ERROR_NONE) {
        cpl_msg_error(fctid,"Cannot save product image extension");
        cpl_propertylist_delete(p);
        return(-1);
    }
    cpl_propertylist_delete(p);

    /* Get out of here */

    return(0);
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Fill undefined products with dummy products
  @param    cs           The configuration structure defined in the main routine
  @param    ps           The memory structure defined in the main routine
  @return   nothing
 */
/*---------------------------------------------------------------------------*/

static void vimos_ima_twilight_flat_dummy_products(configstruct *cs,
                                                       memstruct *ps) {
    cpl_image *test;

    /* See if you even need to be here */

    if (cs->we_get == cs->we_expect)
        return;

    /* First an output combined twilight frame */

    if (! (cs->we_get & MEANTWI)) {
        ps->outimage = casu_dummy_image(ps->twilights[0]);
        cs->flatrms = 0.0;
    }

    /* Now the confidence map */

    if (! (cs->we_get & CONFMAP)) {
        ps->outconf = casu_dummy_image(ps->twilights[0]);
        cs->flatrms = 0.0;
    }

    /* Do a ratio image */

    if ((cs->we_expect & RATIMG) && ! (cs->we_get & RATIMG)) {
        cs->flatratio_med = 0.0;
        cs->flatratio_rms = 0.0;
        ps->ratioimg = casu_dummy_image(ps->twilights[0]);
    }

    /* Do the bad pixel mask */

    if ((cs->we_expect & BPM) && ! (cs->we_get & BPM)) {
        cs->nbad = 0;
        cs->badfrac = 0.0;
        test = casu_dummy_image(ps->twilights[0]);
        ps->outbpm = cpl_image_cast(test,CPL_TYPE_UCHAR);
        freeimage(test);
    }

    /* If a ratio image stats table is required, then do that now */
   
    if ((cs->we_expect & STATS_TAB) && ! (cs->we_get & STATS_TAB)) 
        ps->ratioimstats = vimos_create_diffimg_stats(0);

    return;
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Normalise the twilight frame and create ratio image and stats table
  @param    jext         the extension number
  @param    cs           The configuration structure defined in the main routine
  @param    ps           The memory structure defined in the main routine
  @return   nothing
 */
/*---------------------------------------------------------------------------*/

static void vimos_ima_twilight_flat_normal(int jext, configstruct *cs,
                                               memstruct *ps,
                                               char *vimos_names[]) {
    int nx,ny,ncells,status,*ba,*bpmim;
    long i,npi;
    unsigned char *bpm;
    float *idata,med,sig,gdiff,grms;
    casu_mask *bpmmask;
    cpl_array *bpmarray;
    const char *fctid="vimos_ima_twilight_flat_normal";

    /* Create the bad pixel mask */
    
    nx = cpl_image_get_size_x(ps->outimage); 
    ny = cpl_image_get_size_y(ps->outimage); 
    npi = nx*ny;
    status = CASU_OK;
    (void)casu_genbpm(ps->twilights,ps->ntwilights,ps->outimage,5.0,5.0,"",
                      &bpmarray,&(cs->nbad),&(cs->badfrac),&status);
    if (status == CASU_OK) {
        cs->we_get |= BPM;
        ba = cpl_array_get_data_int(bpmarray);
        bpm = cpl_malloc(npi*sizeof(*bpm));
        bpmim = cpl_malloc(npi*sizeof(*bpmim));
        for (i = 0; i < npi; i++) {
            bpm[i] = (unsigned char)ba[i];
            bpmim[i] = ba[i];
        }
        cpl_array_delete(bpmarray);
        bpmmask = casu_mask_wrap_bpm(bpm,nx,ny);
        ps->outbpm = cpl_image_wrap_int((cpl_size)nx,(cpl_size)ny,bpmim);
    } else {
        bpm = cpl_calloc(npi,sizeof(*bpm));
        bpmmask = casu_mask_wrap_bpm(bpm,nx,ny);        
        cpl_msg_info(fctid,
                     "Bad pixel map creation failed extension %" CPL_SIZE_FORMAT,
                     (cpl_size)jext);
        status = CASU_OK;
    }

    /* Now make the confidence map */

    (void)casu_mkconf(ps->outimage,(char *)"None Available",bpmmask,
                      &(ps->outconf),&(ps->drs2),&status);
    if (status == CASU_OK) 
        cs->we_get |= CONFMAP;
    else {
        cpl_msg_info(fctid,"Confidence map creation failed detector %s",
                     vimos_names[jext-1]);
        status = CASU_OK;
    }
    
    /* Work out the RMS of the mean twilight frame */

    idata = cpl_image_get_data(ps->outimage);
    casu_medsig(idata,bpm,npi,&med,&sig);

    /* Fill in the bad bits... */

    for (i = 0; i < npi; i++)
        if (bpm[i])
            idata[i] = med;

    /* Divide through by the median */

    cpl_propertylist_update_float(ps->drs,"ESO DRS MEDFLAT",med);
    cpl_propertylist_set_comment(ps->drs,"ESO DRS MEDFLAT",
                                 "Median value before normalisation");
    cpl_image_divide_scalar(ps->outimage,med);
    casu_medmad(idata,bpm,npi,&med,&sig);
    sig *= 1.48;
    cs->flatrms = sig;

    /* Load up the master twilight flat */

    if (ps->ref_twilight_flat != NULL) {
        ps->rfimage = casu_fits_load(ps->ref_twilight_flat,CPL_TYPE_FLOAT,jext);
        if (ps->rfimage == NULL) {
            cpl_msg_info(fctid,"Master twilight detector %s won't load",
                         vimos_names[jext-1]);
        } else if (casu_is_dummy(casu_fits_get_ehu(ps->rfimage))) {
            cpl_msg_info(fctid,"Master twilight extension %s is a dummy",
                         vimos_names[jext-1]);
            freefits(ps->rfimage);
        }
    } else
        ps->rfimage = NULL;


    /* Create a ratio image. NB: the difference image routine copes if the 
       input mean image is null and will return nulls */

    cs->flatratio_med = 0.0;
    cs->flatratio_rms = 0.0;
    ncells = cs->ncells;
    vimos_difference_image(casu_fits_get_image(ps->rfimage),ps->outimage,bpm,
                           ncells,2,&gdiff,&grms,&(ps->ratioimg),
                           &(ps->ratioimstats));
    casu_mask_delete(bpmmask);
    cs->flatratio_med = gdiff;
    cs->flatratio_rms = grms;
    if (ps->ratioimg != NULL)
        cs->we_get |= RATIMG;
    if (ps->ratioimstats != NULL)
        cs->we_get |= STATS_TAB;
    return;
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Make dummys if needed and save
  @param    framelist    the input frame list
  @param    parlist      the input recipe parameter list
  @param    chipname     The name of the current detector
  @param    cs           The configuration structure defined in the main routine
  @param    ps           The memory structure defined in the main routine
  @param    isfirst      Set if this is the first extension to be saved
  @param    product_frame_mean_twi    The frame for the mean flat product
  @param    product_frame_conf        The frame for the conf map product
  @param    product_frame_ratioimg    The frame for the ratio image product
  @param    product_frame_ratioimg_stats The frame for the ratioimg stats table
  @param    product_frame_bpm         The frame for the bad pixel mask product
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/


static int vimos_ima_twilight_flat_lastbit(cpl_frameset *framelist,
                                               cpl_parameterlist *parlist,
                                               char *chipname, configstruct *cs,
                                               memstruct *ps,
                                               int isfirst,
                                               cpl_frame **product_frame_mean_twi,
                                               cpl_frame **product_frame_conf,
                                               cpl_frame **product_frame_ratioimg,
                                               cpl_frame **product_frame_ratioimg_stats,
                                               cpl_frame **product_frame_bpm) {

    int retval;
    const char *fctid="vimos_ima_twilight_flat_lastbit";

    /* Make whatever dummy products you need */

    vimos_ima_twilight_flat_dummy_products(cs,ps);

    /* Save everything */

    cpl_msg_info(fctid,"Saving products for detector %s",chipname);
    retval = vimos_ima_twilight_flat_save(framelist,parlist,chipname,cs,ps,
                                              isfirst,product_frame_mean_twi,
                                              product_frame_conf,
                                              product_frame_ratioimg,
                                              product_frame_ratioimg_stats,
                                              product_frame_bpm);
    if (retval != 0) {
        vimos_ima_twilight_flat_tidy(ps,0);
        return(-1);
    }

    /* Free some stuff up */

    vimos_ima_twilight_flat_tidy(ps,1);
    return(0);
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Initialise the pointers in the memory structure
  @param    ps           The memory structure defined in the main routine
 */
/*---------------------------------------------------------------------------*/

static void vimos_ima_twilight_flat_init(memstruct *ps) {
    ps->labels = NULL;
    ps->labels2 = NULL;
    ps->twilightlist = NULL;
    ps->master_bias = NULL;
    ps->master_dark = NULL;
    ps->ref_twilight_flat = NULL;
    ps->outimage = NULL;
    ps->outconf = NULL;
    ps->outbpm = NULL;
    ps->twilights = NULL;
    ps->ntwilights = 0;
    ps->drs = NULL;
    ps->drs2 = NULL;
    ps->rfimage = NULL;
    ps->ratioimg = NULL;
    ps->ratioimstats = NULL;
    ps->mbias = NULL;
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Free any allocated workspace in the memory structure
  @param    ps           The memory structure defined in the main routine
  @param    level        The release level. 0 == everything, 1 == just
                         what is needed for the current detector
 */
/*---------------------------------------------------------------------------*/

static void vimos_ima_twilight_flat_tidy(memstruct *ps, int level) {
    freeimage(ps->outimage);
    freeimage(ps->outconf);
    freeimage(ps->outbpm);
    freefitslist(ps->twilights,ps->ntwilights);
    freepropertylist(ps->drs);
    freepropertylist(ps->drs2);
    freefits(ps->rfimage);
    freeimage(ps->ratioimg);
    freetable(ps->ratioimstats);
    freefits(ps->mbias);
    freefits(ps->mdark);
    if (level == 1)
        return;

    freespace(ps->labels);
    freespace(ps->labels2);
    freeframeset(ps->twilightlist);
    freeframe(ps->master_bias);
    freeframe(ps->master_dark);
    freeframe(ps->ref_twilight_flat);
}

/**@}*/

/*

$Log: vimos_ima_twilight_flat.c,v $
Revision 1.16  2015/10/15 11:28:34  jim
make sure FILTER keyword is present in products

Revision 1.15  2015/09/14 08:20:57  jim
fixed missing comma

Revision 1.14  2015/09/11 09:48:16  jim
changed declaration of some parameters to ranges where appropriate

Revision 1.13  2015/08/07 13:07:15  jim
Fixed copyright to ESO

Revision 1.12  2015/05/13 12:00:28  jim
Fixed missing initialisation

Revision 1.11  2015/05/01 09:02:22  jim
Modified to pass detector name information differently so that Mac compiler
doesn't gripe

Revision 1.10  2015/03/09 15:39:26  jim
Fixed memory leak

Revision 1.9  2015/02/14 12:38:54  jim
Fixed parameter declaration bug

Revision 1.8  2015/01/29 12:10:23  jim
Modified comments, some command line arguments are now strings where it
makes sense to do so, some support routines moved to vimos_utils.c

Revision 1.7  2014/12/11 12:27:28  jim
new version

Revision 1.6  2014/05/06 07:35:19  jim
modified to use new casu_getstds, to use readgain table and to create
modified confidence maps

Revision 1.5  2013/11/21 09:39:08  jim
detabbed

Revision 1.4  2013/11/04 05:59:11  jim
Modified parameter list to save routine

Revision 1.3  2013/10/25 09:31:22  jim
Fixed docs

Revision 1.2  2013-10-24 09:16:28  jim
Changes made to remove compiler warnings

Revision 1.1.1.1  2013-10-24 08:36:12  jim
Initial import


*/
