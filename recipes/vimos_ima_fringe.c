/* $Id: vimos_ima_fringe.c,v 1.5 2015/09/11 09:48:16 jim Exp $
 *
 * This file is part of the VIMOS Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/09/11 09:48:16 $
 * $Revision: 1.5 $
 * $Name:  $
 */

/* Includes */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <libgen.h>
#include <string.h>

#include <cpl.h>
#include <math.h>

#include "vmutils.h"
#include "vimos_imaging_utils.h"
#include "vimos_pfits.h"
#include "vimos_dfs.h"
#include "vimos_mods.h"
#include "vimos_var.h"
#include "casu_utils.h"
#include "casu_stats.h"
#include "casu_fits.h"
#include "casu_mask.h"
#include "casu_wcsutils.h"
#include "casu_mods.h"
#include "casu_sky.h"

/* Structure definitions */

typedef struct {

    /* Input */

    int         prettynames;

    /* Source detection parameters needed for pawsky_mask */

    int         psm_ipix;
    float       psm_thresh;
    int         psm_nbsize;

} configstruct;

typedef struct {
    cpl_size         *labels;
    groupof4         *gr4;
    int              ngr4;
    cpl_frame        *master_bias;
    cpl_frame        *master_dark;
    cpl_frame        *master_twilight_flat;
    cpl_frame        *master_conf;
    casu_mask        *mask;
    cpl_frameset     *science_frames;
    cpl_frame        *product_frame_fringe;
    cpl_frame        *product_frame_fringe_var;
    float            *gaincors;
    cpl_frame        *readgain;

    /* Level 1 stuff */

    casu_fits        *fbias;
    casu_fits        *fdark;
    casu_fits        *fflat;
    casu_fits        *fconf;
    casu_fits        *ffringe;
    casu_fits        *ffringe_var;
    casu_fits        **sci_fits;
    casu_fits        **sci_vars;

} memstruct;

/* List of data product types */

enum {FRINGE_FILE,
      FRINGE_VAR
};

/* Recipe name */

#define VIMOS_RECIPENAME "vimos_ima_fringe"

/* Function prototypes */

static int vimos_ima_fringe_create(cpl_plugin *) ;
static int vimos_ima_fringe_exec(cpl_plugin *) ;
static int vimos_ima_fringe_destroy(cpl_plugin *) ;
static int vimos_ima_fringe(cpl_parameterlist *, cpl_frameset *) ;
static int vimos_ima_fringe_save(casu_fits *obj, cpl_frameset *framelist,
                             cpl_parameterlist *parlist, cpl_frame *template, 
                             int isfirst, const char *tag, char *fname, 
                             char *cname, cpl_frame **product_frame);
static void vimos_ima_fringe_init(memstruct *ps);
static void vimos_ima_fringe_tidy(memstruct *ps, int level);
static void vimos_ima_fringe_product_name(cpl_frame *template, int producttype, 
                                      int nametype, char *outfname);

static char vimos_ima_fringe_description[] =
"vimos_ima_fringe -- VIMOS fringe frame creation recipe.\n\n"
"Take a list of science frames and correct them to remove bias, dark\n"
"and flat field signatures. Combine the images to form a new master\n"
"fringe frame\n\n"
"The program accepts the following files in the SOF:\n\n"
"    Tag                   Description\n"
"    -----------------------------------------------------------------------\n"
"    %-21s A list of raw science images\n"
"    %-21s A master bias frame\n"
"    %-21s A master dark frame\n"
"    %-21s A master flat frame\n"
"    %-21s A master confidence map\n"
"    %-21s A master readgain table\n"
"All of these are required."
"\n";

/**@{*/

/**
    \ingroup recipelist
    \defgroup vimos_ima_fringe vimos_ima_fringe
    \brief VIMOS master fringe creation recipe

    \par Name: 
        vimos_ima_fringe
    \par Purpose: 
        Create a master fringe frame from a list of science images
    \par Language:
        C
    \par Parameters:
        - General reduction parameters:
            - \b prettynames (bool): True if we're using nice file names       
        - Parameters for source detection for pawsky_mask:
            - \b psm_ipix (int): The minimum allowable size of an object
            - \b psm_thresh (float): The detection threshold in sigma above sky
            - \b psm_nbsize (int): The smoothing box size for background map estimation        
    \par Input File Types:
        The following list gives the file types that can appear in the SOF
        file. The word in bold is the DO category value.
        - \b OBJECT (required): A list of raw science images for processing
        - \b MASTER_BIAS (required): A master bias frame. This is needed 
             for the bias correction of the input images.
        - \b MASTER_DARK (required): A master dark frame. This is needed 
             for the dark correction of the input images.
        - \b MASTER_TWILIGHT_FLAT (required): A master twilight flat frame
             appropriate for the filter used in the science observations.
             This is used to remove the flat field effects from the science
             frames and also for the detector level gain correction.
        - \b MASTER_READGAIN (required): A master read/gain table
        - \b MASTER_CONF (required): A master confidence map 
             appropriate for the filter used in the science observations.
             This is used for weighting and to mark bad pixels.
    \par Output Products:
        The following list gives the output data products that are generated
        by this recipe. The word in bold gives the DPR CATG keyword value for
        each product:
        - Master fringe frame (\b MASTER_FRINGE)
        - Master fringe variance map (\b MASTER_FRINGE_VAR)
    \par Output QC Parameters:
        None
    \par Notes
        None.
    \par Fatal Error Conditions:
        - NULL input frameset
        - Input frameset headers incorrect meaning that RAW and CALIB frame
          can't be distinguished
        - No science frames in the input frameset
        - No master bias, dark, flat or confidence map frame in input frameset
        - Unable to save data products
        - Unable to load images
    \par Non-Fatal Error Conditions:
        - None
    \par Conditions Leading To Dummy Products:
        - Master calibration images flagged as dummys
        - Detector for the current image is flagged as dead in all 
          science images
        - Failure of one of the processing steps
    \par Author:
        Jim Lewis, CASU
    \par Code Reference: 
        vimos_ima_fringe.c
*/

/* Function code */

/*---------------------------------------------------------------------------*/
/**
  @brief    Build the list of available plugins, for this module.
  @param    list    the plugin list
  @return   0 if everything is ok

  This function is exported.
 */
/*---------------------------------------------------------------------------*/

int cpl_plugin_get_info(cpl_pluginlist *list) {
    cpl_recipe  *recipe = cpl_calloc(1,sizeof(*recipe));
    cpl_plugin  *plugin = &recipe->interface;
    char alldesc[SZ_ALLDESC];
    (void)snprintf(alldesc,SZ_ALLDESC,vimos_ima_fringe_description,
                   VIMOS_SCI_OBJECT_RAW,VIMOS_CAL_BIAS,VIMOS_CAL_DARK,
                   VIMOS_CAL_TWILIGHT_FLAT,VIMOS_CAL_CONF,VIMOS_CAL_READGAIN);
    cpl_plugin_init(plugin,
                    CPL_PLUGIN_API,
                    VIMOS_BINARY_VERSION,
                    CPL_PLUGIN_TYPE_RECIPE,
                    "vimos_ima_fringe",
                    "Fringe frame creation for imaging",
                    alldesc,
                    "Jim Lewis",
                    "jrl@ast.cam.ac.uk",
                    vimos_get_license(),
                    vimos_ima_fringe_create,
                    vimos_ima_fringe_exec,
                    vimos_ima_fringe_destroy);

    cpl_pluginlist_append(list,plugin);

    return(0);
}


/*---------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options
  @param    plugin  the plugin
  @return   0 if everything is ok

  Create the recipe instance and make it available to the application using the
  interface.
 */
/*---------------------------------------------------------------------------*/

static int vimos_ima_fringe_create(cpl_plugin *plugin) {
    cpl_recipe      *recipe;
    cpl_parameter   *p;

    /* Get the recipe out of the plugin */

    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE)
        recipe = (cpl_recipe *)plugin;
    else 
        return(-1);

    /* Create the parameters list in the cpl_recipe object */

    recipe->parameters = cpl_parameterlist_new();

    /* Fill in flag to decide whether use predictable names or nice
       names based on input file names */

    p = cpl_parameter_new_value("vimos.vimos_ima_fringe.prettynames",
                                CPL_TYPE_BOOL,"Use pretty product names?",
                                "vimos.vimos_ima_fringe",FALSE);
    cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,"prettynames");
    cpl_parameterlist_append(recipe->parameters,p);

    /* ***Special parameters for object detection for pawsky_mask*** */

    /* Fill in the minimum object size */

    p = cpl_parameter_new_range("vimos.vimos_ima_fringe.psm_ipix",
                                CPL_TYPE_INT,
                                "Minimum pixel area for each detected object",
                                "vimos.vimos_ima_fringe",5,1,100000);
    cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,"psm_ipix");
    cpl_parameterlist_append(recipe->parameters,p);

    /* Fill in the detection threshold parameter */

    p = cpl_parameter_new_range("vimos.vimos_ima_fringe.psm_thresh",
                                CPL_TYPE_DOUBLE,
                                "Detection threshold in sigma above sky",
                                "vimos.vimos_ima_fringe",3.0,1.0e-6,1.0e10);
    cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,"psm_thresh");
    cpl_parameterlist_append(recipe->parameters,p);

    /* Fill in background smoothing box size */

    p = cpl_parameter_new_range("vimos.vimos_ima_fringe.psm_nbsize",
                                CPL_TYPE_INT,"Background smoothing box size",
                                "vimos.vimos_ima_fringe",64,1,2048);
    cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,"psm_nbsize");
    cpl_parameterlist_append(recipe->parameters,p);

    /* Get out of here */

    return(0);
}
        
/*---------------------------------------------------------------------------*/
/**
  @brief    Execute the plugin instance given by the interface
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/

static int vimos_ima_fringe_exec(cpl_plugin *plugin) {
    cpl_recipe  *recipe;

    /* Get the recipe out of the plugin */

    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE)
        recipe = (cpl_recipe *)plugin;
    else 
        return(-1);

    return(vimos_ima_fringe(recipe->parameters,recipe->frames));
}
                                
/*---------------------------------------------------------------------------*/
/**
  @brief    Destroy what has been created by the 'create' function
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/

static int vimos_ima_fringe_destroy(cpl_plugin *plugin) {
    cpl_recipe *recipe ;

    /* Get the recipe out of the plugin */

    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE)
        recipe = (cpl_recipe *)plugin;
    else 
        return(-1);

    cpl_parameterlist_delete(recipe->parameters);
    return(0);
}

/*---------------------------------------------------------------------------*/
/**
  @brief    The recipe data reduction part is implemented here 
  @param    parlist     the parameters list
  @param    framelist   the frames list
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/

static int vimos_ima_fringe(cpl_parameterlist *parlist, 
                                 cpl_frameset *framelist) {
    const char *fctid="vimos_ima_fringe";
    memstruct ps;
    configstruct cs;
    cpl_parameter *p;
    cpl_size nlab;
    int nfail,status,i,j,isfirst;
    float gaincor_fac,readnoise,gain,texp;
    cpl_frame *template,*frm;
    cpl_image *im;
    casu_fits *ff,*ffv;
    char bname[BUFSIZ],*vimos_names[VIMOS_NEXTN];
    unsigned char *bconf;

    /* Check validity of input frameset */

    if (framelist == NULL || cpl_frameset_get_size(framelist) <= 0) {
        cpl_msg_error(fctid,"Input framelist NULL or has no input data");
        return(-1);
    }

    /* Initialise some things */

    vimos_ima_fringe_init(&ps);

    /* Get the parameters */

    p = cpl_parameterlist_find(parlist,
                               "vimos.vimos_ima_fringe.prettynames");
    cs.prettynames = (cpl_parameter_get_bool(p) ? 1 : 0);

    /* Object detection for pawsky_mask */

    p = cpl_parameterlist_find(parlist,
                               "vimos.vimos_ima_fringe.psm_ipix");
    cs.psm_ipix = cpl_parameter_get_int(p);
    p = cpl_parameterlist_find(parlist,
                               "vimos.vimos_ima_fringe.psm_thresh");
    cs.psm_thresh = (float)cpl_parameter_get_double(p);
    p = cpl_parameterlist_find(parlist,
                               "vimos.vimos_ima_fringe.psm_nbsize");
    cs.psm_nbsize = cpl_parameter_get_int(p);

    /* Sort out raw from calib frames */

    if (vimos_dfs_set_groups(framelist) != CASU_OK) {
        cpl_msg_error(fctid,"Cannot identify RAW and CALIB frames");
        vimos_ima_fringe_tidy(&ps,0);
        return(-1);
    }

    /* Label the input frames */

    if ((ps.labels = cpl_frameset_labelise(framelist,casu_compare_tags,
                                           &nlab)) == NULL) {
        cpl_msg_error(fctid,"Cannot labelise the input frames");
        vimos_ima_fringe_tidy(&ps,0);
        return(-1);
    }

    /* Get the input science frames */

    nfail = 0;
    if ((ps.science_frames = 
         casu_frameset_subgroup(framelist,ps.labels,nlab,
                                VIMOS_SCI_OBJECT_RAW)) == NULL) {
        cpl_msg_error(fctid,"No science images to process!");
        vimos_ima_fringe_tidy(&ps,0);
        return(-1);
    }
    nfail += vimos_testfrms(ps.science_frames,1,1,1);

    /* See what chip set this is */

    if (vimos_load_names(cpl_frameset_get_position(ps.science_frames,0),
                         vimos_names) != CASU_OK) {
        cpl_msg_error(fctid,"Cannot get the name set");
        vimos_ima_fringe_tidy(&ps,0);
        return(-1);
    }

    /* Now divide the science images up so that we have groups of 4
       images per exposure */

    vimos_get_groupsof4(ps.science_frames,vimos_names,&(ps.gr4),&(ps.ngr4));

    /* Check to see if there is a master bias frame */

    if ((ps.master_bias = casu_frameset_subgroup_1(framelist,ps.labels,nlab,
                                                   VIMOS_CAL_BIAS)) == NULL) {
        cpl_msg_error(fctid,"No master bias found");
        vimos_ima_fringe_tidy(&ps,0);
        return(-1);
    }
    nfail += vimos_testfrm_1(ps.master_dark,VIMOS_NEXTN,1,0);

    /* Check to see if there is a master dark frame */

    if ((ps.master_dark = casu_frameset_subgroup_1(framelist,ps.labels,nlab,
                                                   VIMOS_CAL_DARK)) == NULL) {
        cpl_msg_error(fctid,"No master dark found");
        vimos_ima_fringe_tidy(&ps,0);
        return(-1);
    }
    nfail += vimos_testfrm_1(ps.master_dark,VIMOS_NEXTN,1,0);
                
    /* Check to see if there is a master twilight flat frame */

    if ((ps.master_twilight_flat = 
         casu_frameset_subgroup_1(framelist,ps.labels,nlab,
                                  VIMOS_CAL_TWILIGHT_FLAT)) == NULL) {
        cpl_msg_error(fctid,"No master twilight flat found");
        vimos_ima_fringe_tidy(&ps,0);
        return(-1);
    }
    nfail += vimos_testfrm_1(ps.master_twilight_flat,VIMOS_NEXTN,1,0);

    /* Get the gain corrections */

    status = CASU_OK;
    if (casu_gaincor_calc(ps.master_twilight_flat,&i,&(ps.gaincors),
                          &status) != CASU_OK) {
        cpl_msg_error(fctid,"Error calculating gain corrections");
        vimos_ima_fringe_tidy(&ps,0);
        return(-1);
    }

    /* Check to see if there is a master confidence map. */

    if ((ps.master_conf = 
        casu_frameset_subgroup_1(framelist,ps.labels,nlab,
                                 VIMOS_CAL_CONF)) == NULL) {
        cpl_msg_error(fctid,"No master confidence map file found");
        vimos_ima_fringe_tidy(&ps,0);
        return(-1);
    }
    nfail += vimos_testfrm_1(ps.master_conf,VIMOS_NEXTN,1,0);
    ps.mask = casu_mask_define(framelist,ps.labels,nlab,VIMOS_CAL_CONF,
                               "NONE");

    /* Check to see if there is a master read/gain table */

    if ((ps.readgain = 
        casu_frameset_subgroup_1(framelist,ps.labels,nlab,
                                 VIMOS_CAL_READGAIN)) == NULL) {
        cpl_msg_error(fctid,"No master readgain table file found");
        vimos_ima_fringe_tidy(&ps,0);
        return(-1);
    }
    nfail += vimos_testrdgn(ps.master_conf,ps.readgain);

    /* Ok if any of this failed, then get out of here. This makes it a bit 
       simpler later on since we now know that all of the specified files 
       have the correct number of extensions and will load properly */

    if (nfail > 0) {
        cpl_msg_error(fctid,
                      "There are %" CPL_SIZE_FORMAT " input file errors -- cannot continue",
                      (cpl_size)nfail);
        vimos_ima_fringe_tidy(&ps,0);
        return(-1);
    }

    /* Get some workspace for the product frames */

    ps.sci_fits = cpl_malloc(ps.ngr4*sizeof(casu_fits *));
    ps.sci_vars = cpl_malloc(ps.ngr4*sizeof(casu_fits *));
    for (i = 0; i < ps.ngr4; i++) {
        ps.sci_fits[i] = NULL;
        ps.sci_vars[i] = NULL;
    }
  
    /* Now loop for all detectors... */

    template = NULL;
    for (j = 1; j <= VIMOS_NEXTN; j++) {
        isfirst = (j == 1);

        /* Right, we want to bias, dark and flat correct, so we need to load 
           the mean calibration frames */

        ps.fbias = casu_fits_load(ps.master_bias,CPL_TYPE_FLOAT,j);
        ps.fdark = casu_fits_load(ps.master_dark,CPL_TYPE_FLOAT,j);
        ps.fflat = casu_fits_load(ps.master_twilight_flat,CPL_TYPE_FLOAT,j);
        ps.fconf = casu_fits_load(ps.master_conf,CPL_TYPE_INT,j);
        casu_mask_load(ps.mask,j,
                       (int)cpl_image_get_size_x(casu_fits_get_image(ps.fconf)),
                       (int)cpl_image_get_size_y(casu_fits_get_image(ps.fconf)));

        /* Gain corrections are in the same order as vimos_names so there's
           no trouble making sure we have the right one... */

        gaincor_fac = (ps.gaincors)[j-1];

        /* Get the readnoise and gain estimates */

        vimos_getrdgn(ps.readgain,casu_fits_get_extname(ps.fflat),
                      &readnoise,&gain);

        /* Loop for each image and do the stage 1 corrections. */

        cpl_msg_info(fctid,"Stage 1 corrections for %s",vimos_names[j-1]);
        for (i = 0; i < ps.ngr4; i++) {
            status = CASU_OK;
            frm = ps.gr4[i].inf[j-1];
            if (frm == NULL) {
                im = casu_dummy_image(ps.fconf);
                (ps.sci_fits)[i] = casu_fits_wrap(im,NULL,NULL,NULL);
                casu_fits_set_status((ps.sci_fits)[i],CASU_FATAL);
                im = casu_dummy_image(ps.fconf);
                (ps.sci_vars)[i] = casu_fits_wrap(im,NULL,NULL,NULL);
                casu_fits_set_status((ps.sci_vars)[i],CASU_FATAL);
                cpl_msg_info(fctid,
                             "There is no image for detector %s in group %s",
                             vimos_names[j-1],ps.gr4[i].name);
                continue;
            }
            if (template == NULL)
                template = cpl_frame_duplicate(frm);
            ff = casu_fits_load(frm,CPL_TYPE_FLOAT,
                                cpl_frame_get_nextensions(frm));
            (ps.sci_fits)[i] = ff;
            vimos_biascor(ff,ps.fbias,1,1,&status);
            vimos_pfits_get_exptime(casu_fits_get_ehu(ff),&texp);
            casu_darkcor(ff,ps.fdark,texp,&status);
            ffv = vimos_var_create(ff,ps.mask,readnoise,gain);
            casu_flatcor(ff,ps.fflat,&status);
            vimos_var_div_im(ffv,ps.fflat);
            casu_gaincor(ff,gaincor_fac,&status);
            vimos_var_divk(ffv,gaincor_fac);
            casu_fits_set_error(ff,status);
            cpl_propertylist_update_float(casu_fits_get_ehu(ff),"READNOIS",
                                          readnoise);
            cpl_propertylist_set_comment(casu_fits_get_ehu(ff),"READNOIS",
                                         "[ADU] Read noise used in reduction");
            cpl_propertylist_update_float(casu_fits_get_ehu(ff),"GAIN",
                                          gain);
            cpl_propertylist_set_comment(casu_fits_get_ehu(ff),"GAIN",
                                         "[e-/ADU] gain used in reduction");

            /* Store away the variance map */

            (ps.sci_vars)[i] = ffv;
        }
            
        /* Create the fringe frame */

        status = CASU_OK;
        (void)casu_pawsky_mask(ps.sci_fits,ps.sci_vars,ps.ngr4,ps.fconf,
                               ps.mask,&(ps.ffringe),&(ps.ffringe_var),
                               3,cs.psm_ipix,cs.psm_thresh,cs.psm_nbsize,
                               2.0,&status);

        /* Now save it and the variance map */

        if (j == 1) 
            vimos_ima_fringe_product_name(template,FRINGE_FILE,cs.prettynames,
                                      bname);
        else 
            strcpy(bname,cpl_frame_get_filename(ps.product_frame_fringe));
        if (vimos_ima_fringe_save(ps.ffringe,framelist,parlist,
                              template,isfirst,VIMOS_PRO_FRINGE,bname,
                              vimos_names[j-1],
                              &(ps.product_frame_fringe)) != CASU_OK) {
            cpl_msg_error(fctid,"Unable to save product %s[%d]",bname,j);
            vimos_ima_fringe_tidy(&ps,0);
            return(-1);
        }        
        if (j == 1)
            vimos_ima_fringe_product_name(template,FRINGE_VAR,cs.prettynames,
                                      bname);
        else 
            strcpy(bname,cpl_frame_get_filename(ps.product_frame_fringe_var));
        if (vimos_ima_fringe_save(ps.ffringe_var,framelist,parlist,
                              template,isfirst,VIMOS_PRO_FRINGE_VAR,bname,
                              vimos_names[j-1],
                              &(ps.product_frame_fringe_var)) != CASU_OK) {
            cpl_msg_error(fctid,"Unable to save product %s[%d]",bname,j);
            vimos_ima_fringe_tidy(&ps,0);
            return(-1);
        }        

        /* End of the extension loop */

        vimos_ima_fringe_tidy(&ps,1);
        freeframe(template);
    }

    /* Get out of here */

    vimos_ima_fringe_tidy(&ps,0);
    return(0);
}
    
/*---------------------------------------------------------------------------*/
/**
    \par Name:
        vimos_ima_fringe_save
    \par Purpose:
        Save the fringe image products
    \par Description:
        An extension of an image product is saved here
    \par Language:
        C
    \param obj
        The casu_fits structure for the image to be saved
    \param framelist
        The input recipe frameset
    \param parlist
        The input recipe parameter list
    \param template
        A template to be used in the cpl saving routine
    \param isfirst
        Set of this is the first extension to be written to the file
    \param tag
        The product tag for the saved file
    \param fname
        The output file name
    \param cname
        The detector name
    \param product_frame
        The output product frame
    \retval CASU_OK if everything is ok
    \retval CASU_FATAL if something failed
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

static int vimos_ima_fringe_save(casu_fits *obj, cpl_frameset *framelist,
                             cpl_parameterlist *parlist, cpl_frame *template,
                             int isfirst, const char *tag, char *fname, 
                             char *cname, cpl_frame **product_frame) {
    cpl_propertylist *plist;
    int isdummy;
    const char *fctid = "vimos_ima_fringe_save";

    /* Get some information about this guy */

    isdummy = (casu_fits_get_status(obj) != CASU_OK);

    /* If we need to make a PHU then do that now based on the first frame
       in the input frame list. Get rid of the file if it already exists */

    if (isfirst) {
        if (access(fname,F_OK)) 
            remove(fname);

        /* Create a new product frame object and define some tags */

        *product_frame = cpl_frame_new();
        cpl_frame_set_filename(*product_frame,fname);

        /* Tag the product */

        cpl_frame_set_tag(*product_frame,tag);
        cpl_frame_set_type(*product_frame,CPL_FRAME_TYPE_IMAGE);
        cpl_frame_set_group(*product_frame,CPL_FRAME_GROUP_PRODUCT);
        cpl_frame_set_level(*product_frame,CPL_FRAME_LEVEL_FINAL);

        /* Set up the PHU header */

        plist = casu_fits_get_phu(obj);
        vimos_dfs_set_product_primary_header(plist,*product_frame,framelist,
                                             parlist,VIMOS_RECIPENAME,
                                             "PRO-1.15",template,1);

        /* 'Save' the PHU image */                         

        if (cpl_image_save(NULL,fname,CPL_TYPE_UCHAR,plist,CPL_IO_DEFAULT) != 
            CPL_ERROR_NONE) {
            cpl_msg_error(fctid,"Cannot save product PHU");
            cpl_frame_delete(*product_frame);
            return(CASU_FATAL);
        }
        cpl_frameset_insert(framelist,*product_frame);
    }

    /* Get the extension property list */

    plist = casu_fits_get_ehu(obj);
    if (isdummy)
        casu_dummy_property(plist);

    /* Fiddle with the header now */

    cpl_propertylist_update_string(plist,"EXTNAME",cname);
    vimos_dfs_set_product_exten_header(plist,*product_frame,framelist,parlist,
                                       VIMOS_RECIPENAME,"PRO-1.15",template);
    if (cpl_image_save(casu_fits_get_image(obj),fname,CPL_TYPE_FLOAT,plist,
                       CPL_IO_EXTEND) != CPL_ERROR_NONE) {
        cpl_msg_error(fctid,"Cannot save product image extension -- %s",
                      cpl_error_get_message());
        return(CASU_FATAL);
    }

    /* Get out of here */

    return(CASU_OK);
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        vimos_ima_fringe_product_name
    \par Purpose:
        Set up an output product file name
    \par Description:
        An output file name is defined based on a template name,  a
        'predictable' name or a temporary file name.
    \par Language:
        C
    \param template
        The this will be used as a reference name.
    \param producttype
        The type of product you are writing out. These are enumerated above.
    \param nametype
        This is: 0 -> predictable names, 1 -> pretty names based on input
        file names or 2 -> temporary file names.
    \param outfname
        The output file name
    \return
        Nothing
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

static void vimos_ima_fringe_product_name(cpl_frame *template, int producttype, 
                                      int nametype, char *outfname) {
    const char *esonames[] = {"fringe","fringevar"};
    const char *prefix[] = {"fringe","fringevar"};
    char *fname,*bname,filter[16],dateobs[32];
    cpl_propertylist *p;

    /* If the name type is the ESO predictable sort, then it's easy. Just
       use the esonames defined above */

    switch (nametype) {
    case 0:
        (void)sprintf(outfname,"%s.fits",esonames[producttype]);
        break;

    /* If this is a temporary file, then just append tmp_ to the template
       name and return that */

    case 2:
        fname = cpl_strdup(cpl_frame_get_filename(template));
        bname = basename(fname);
        (void)sprintf(outfname,"tmp_%s",bname);
        freespace(fname);
        break;

    /* Ok, we want a pretty name... */

    case 1:
        p = cpl_propertylist_load(cpl_frame_get_filename(template),
                                  cpl_frame_get_nextensions(template));
        if (vimos_pfits_get_filter(p,filter) != CASU_OK) 
            (void)strncpy(filter,"unknown",16);
        if (vimos_pfits_get_dateobs(p,dateobs) != CASU_OK)
            (void)strncpy(dateobs,"1900-01-01T00:00:00.000",32);
        sprintf(outfname,"%s_%s_%s.fits",filter,prefix[producttype],dateobs);
        cpl_propertylist_delete(p);
        break;

    /* something else ?? */

    default:
        (void)strcpy(outfname,"");
        break;
    }
    return;
}   

/**@}*/

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        vimos_ima_fringe_init
    \par Purpose:
        Initialise pointers in the global memory structure.
    \par Description:
        Initialise pointers in the global memory structure.
    \par Language:
        C
    \param ps
        The memory structure
    \return
        Nothing
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

static void vimos_ima_fringe_init(memstruct *ps) {

    /* Level 0 stuff */

    ps->labels = NULL;
    ps->gr4 = NULL;
    ps->ngr4 = 0;
    ps->master_bias = NULL;
    ps->master_dark = NULL;
    ps->master_twilight_flat = NULL;
    ps->master_conf = NULL;
    ps->readgain = NULL;
    ps->mask = NULL;
    ps->science_frames = NULL;
    ps->product_frame_fringe = NULL;
    ps->product_frame_fringe_var = NULL;
    ps->gaincors = NULL;

    /* Level 1 stuff */

    ps->fbias = NULL;
    ps->fdark = NULL;
    ps->fflat = NULL;
    ps->fconf = NULL;
    ps->ffringe = NULL;
    ps->ffringe_var = NULL;
    ps->sci_fits = NULL;
    ps->sci_vars = NULL;
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        vimos_ima_fringe_tidy
    \par Purpose:
        Free allocated workspace in the global memory structure
    \par Description:
        Free allocated workspace in the global memory structure. The tidy works
        on two levels. Level 1 is for items that are usually cleared up after
        each extension is processed. Level 2 is for cleaning up the whole
        recipe
    \par Language:
        C
    \param ps
        The memory structure
    \param level
        The level of the tidy to be done. 1: Tidy up after finishing an 
        extension, 0: Tidy up after finishing the recipe.
    \return
        Nothing
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

static void vimos_ima_fringe_tidy(memstruct *ps, int level) {
    int i;

    /* Level 1 stuff */

    freefits(ps->fbias); 
    freefits(ps->fdark); 
    freefits(ps->fflat); 
    freefits(ps->fconf);
    freefits(ps->ffringe);
    freefits(ps->ffringe_var);
    for (i = 0; i < ps->ngr4; i++) {
        freefits(ps->sci_fits[i]);
        freefits(ps->sci_vars[i]);
    }

    if (level == 1)
        return;
    
    /* Level 0 stuff */

    freespace(ps->labels);
    vimos_free_groupsof4(&(ps->ngr4),&(ps->gr4));
    freeframe(ps->master_bias);
    freeframe(ps->master_dark);
    freeframe(ps->master_twilight_flat);
    freeframe(ps->master_conf);
    freeframe(ps->readgain);
    freemask(ps->mask);
    freeframeset(ps->science_frames);
    freespace(ps->gaincors);
    freespace(ps->sci_fits);
    freespace(ps->sci_vars);
}

/*

$Log: vimos_ima_fringe.c,v $
Revision 1.5  2015/09/11 09:48:16  jim
changed declaration of some parameters to ranges where appropriate

Revision 1.4  2015/08/07 13:07:15  jim
Fixed copyright to ESO

Revision 1.3  2015/05/13 11:59:06  jim
Fringes are no longer chopped

Revision 1.2  2015/05/01 09:02:22  jim
Modified to pass detector name information differently so that Mac compiler
doesn't gripe

Revision 1.1  2015/04/30 12:22:44  jim
New entry


*/
