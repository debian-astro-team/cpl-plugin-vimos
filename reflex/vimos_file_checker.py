from __future__ import with_statement
from __future__ import absolute_import
from __future__ import print_function
import sys

try:
    import reflex
    from pipeline_product import PipelineProduct
    import os
    import math
    import pdb
    from optparse import OptionParser
except  ImportError:
    print("Error importing one or more modules")

if __name__ == '__main__':

  # Define the I/O for the Actor
  parser = reflex.ReflexIOParser()
  parser.add_option("-i", "--in_sof", dest="in_sof")
  parser.add_option("-e", "--in_enable", dest="in_enable")
  parser.add_output("-s", "--out_sof", dest="out_sof")
  parser.add_output("-w", "--out_warning", dest="out_warning")
  parser.add_output("-n", "--out_enable", dest="out_enable")
  inputs  = parser.get_inputs()
  outputs = parser.get_outputs()

  # If Enable is false do nothing and pass everything through
  if (inputs.in_enable == 'false'):
      outputs.out_sof = inputs.in_sof
      outputs.out_enable = inputs.in_enable
      outputs.out_warning = 'none'

  elif (inputs.in_enable == 'true'):

      valid_files = True
      warn_sep = "\n"
      warn = []

      bias_frames = []
      flat_frames = []

      for file in inputs.in_sof.files:
          if (file.category == "BIAS"):
              bias_frames.append(PipelineProduct(file))
          if (file.category == "FLAT_TWILIGHT"):
              flat_frames.append(PipelineProduct(file))

      # Make sure we have exactly 8 biases and 8 flats
      if ((len(bias_frames) !=8) or (len(flat_frames) != 8)):
          warn.append("Number of bias or flat frames != 8")
          valid_files = False
      else:

          # Sort frames (in place) by ascending MJD-OBS keyword in primary header
          bias_frames.sort(key=lambda x: x.all_hdu[0].header['MJD-OBS'])
          flat_frames.sort(key=lambda x: x.all_hdu[0].header['MJD-OBS'])

          # Store MJD and chip names into convenient lists
          mjd_bias = []
          chip_id_bias = []

          for j in range(0,8):
              mjd_bias.append(bias_frames[j].all_hdu[0].header['MJD-OBS'])
              chip_id_bias.append(bias_frames[j].all_hdu[0].header['HIERARCH ESO DET CHIP1 ID'])

          mjd_flat = []
          chip_id_flat = []

          for j in range(0,8):
              mjd_flat.append(flat_frames[j].all_hdu[0].header['MJD-OBS'])
              chip_id_flat.append(flat_frames[j].all_hdu[0].header['HIERARCH ESO DET CHIP1 ID'])

          for i in range(0,8,4):
              # Check if a subgroup of 4 all have MJD-OBS within 0.5 seconds of each other
              # (Ugly way to identify coherent set of file for a single observation)
              delta_t = 86400 * (max(mjd_bias[i:i+4]) - min(mjd_bias[i:i+4]))
              if (delta_t >= 0.5): 
                  warn.append("MJD-OBS between biases >= 0.5 sec")
                  valid_files = False

              delta_t = 86400 * (max(mjd_flat[i:i+4]) - min(mjd_flat[i:i+4]))
              if (delta_t >= 0.5): 
                  warn.append("MJD-OBS between flats >= 0.5 sec")
                  valid_files = False

              # Check that we have full complement of chips in each group of four

              if (( ('Tom' in chip_id_bias[i:i+4]) and 
                    ('Keith' in chip_id_bias[i:i+4]) and
                    ('DAVID' in chip_id_bias[i:i+4]) and
                    ('BRIAN' in chip_id_bias[i:i+4]) ) or
                  ( ('CCD-59A' in chip_id_bias[i:i+4]) and 
                    ('CCD-60A' in chip_id_bias[i:i+4]) and
                    ('CCD-59B' in chip_id_bias[i:i+4]) and
                    ('CCD-60B' in chip_id_bias[i:i+4]) )):
                  pass
              else:
                  warn.append("Incomplete set of chips for biases")
                  valid_files = False

              if (( ('Tom' in chip_id_flat[i:i+4]) and 
                    ('Keith' in chip_id_flat[i:i+4]) and
                    ('DAVID' in chip_id_flat[i:i+4]) and
                    ('BRIAN' in chip_id_flat[i:i+4]) ) or
                  ( ('CCD-59A' in chip_id_flat[i:i+4]) and 
                    ('CCD-60A' in chip_id_flat[i:i+4]) and
                    ('CCD-59B' in chip_id_flat[i:i+4]) and
                    ('CCD-60B' in chip_id_flat[i:i+4]) )):
                  pass
              else:
                  warn.append("Incomplete set of chips for flats")
                  valid_files = False

      if (valid_files == True):
          outputs.out_warning = ('none')
          outputs.out_enable = inputs.in_enable
          outputs.out_sof = inputs.in_sof
      elif (valid_files == False):
          warn.insert(0,"WARNING: Incomplete/Invalid Set of Files for "
                                 "vimos_ima_det_noise recipe.")
          warn.append("Enable_Master_Readgain_Processing set to false.")
          outputs.out_warning = warn_sep.join(warn)
          print(outputs.out_warning)
          outputs.out_enable = ('false')
          outputs.out_sof = inputs.in_sof

  parser.write_outputs()
  sys.exit() 
